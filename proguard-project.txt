# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
 #  public *;
#}
-ignorewarnings
-libraryjars /libs/alipaysdk.jar
-libraryjars /libs/alipaysecsdk.jar 
-libraryjars /libs/alipayutdid.jar  
-libraryjars /libs/bmobim_v1.1.2beta.jar
-libraryjars /libs/bmobsdk_v3.2.3_0919.jar
-libraryjars /libs/gson-2.2.1.jar
-libraryjars /libs/locSDK_3.3.jar
-libraryjars /libs/mframework.jar
-libraryjars /libs/nineoldandroids-2.4.0.jar
-libraryjars /libs/universal-image-loader-1.9.2_sources.jar
-libraryjars /libs/xUtils-2.6.11.jar



-libraryjars /libs/unity-classes-aa.jar
-dontwarn com.unity3d.player.**
-keep class com.unity3d.player.** { *;}
-keep class org.fmod.** { *;}



-keep class com.alipay.android.app.IAlixPay{*;} 
-keep class com.alipay.android.app.IAlixPay$Stub{*;} 
-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
-keep class com.alipay.sdk.app.PayTask{ public *;}
-keep class com.alipay.sdk.auth.AlipaySDK{ public *;} 
-keep class com.alipay.sdk.auth.APAuthInfo{ public *;} 
-keep class com.alipay.mobilesecuritysdk.* 
-keep class com.ut.*

