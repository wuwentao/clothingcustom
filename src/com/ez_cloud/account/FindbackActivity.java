package com.ez_cloud.account;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class FindbackActivity extends BaseActivity {
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	@ViewInject(R.id.tel)
	EditText tel;
	@ViewInject(R.id.code)
	EditText code;
	@ViewInject(R.id.getcode)
	Button getcode;
	@ViewInject(R.id.psw)
	EditText psw;
	@ViewInject(R.id.psw_again)
	EditText psw_again;
	@ViewInject(R.id.confirm)
	Button confirm;
	WebReturnData webReturnData;
	String url;
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg != null) {
				webReturnData = (WebReturnData) msg.obj;
			} else {
				Toast.makeText(FindbackActivity.this, R.string.server_error,
						Toast.LENGTH_SHORT).show();
				return;
			}
			switch (msg.what) {
			case Constants.REQUEST_TAG_A:// 找回密码是否成功
				if (webReturnData.code.equals("00")) {
					Toast.makeText(FindbackActivity.this,
							webReturnData.message, Toast.LENGTH_SHORT).show();
					((ClothingApplication) FindbackActivity.this
							.getApplication()).setLongin(true);
					startActivity(new Intent(FindbackActivity.this,
							LoginActivity.class));
					FindbackActivity.this.finish();
				} else {
					Toast.makeText(FindbackActivity.this,
							webReturnData.message, Toast.LENGTH_SHORT).show();

				}
				break;
			case Constants.REQUEST_TAG_B:// 获取验证码
				if (webReturnData.code.equals("00")) {
					Toast.makeText(FindbackActivity.this,
							webReturnData.message, Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(FindbackActivity.this,
							webReturnData.message, Toast.LENGTH_SHORT).show();
				}
				break;
			case Constants.REQUEST_TAG_C:// 验证码输入是否正确
				if (webReturnData.code.equals("00")) {
					url = ServerInterface.findbackUrl;
					requestParams = new RequestParams();
					requestParams.addBodyParameter("userId",
							SharedPreferencesUitls.getString(
									getApplicationContext(), "userId", ""));
					if (telS.matches(Constants.EMAIL)) {
						requestParams.addBodyParameter("userEmail", telS);
					} else {
						requestParams.addBodyParameter("telNumber", telS);
					}
					requestParams.addBodyParameter("authCode", authCode);
					requestParams.addBodyParameter("newPassWord", pswS);
					ClothingHttpUtils.httpSend(HttpMethod.POST, url,
							requestParams, WebReturnData.class, handler,
							Constants.REQUEST_TAG_A, Constants.REQUEST_FAILURE);

				} else {
					Toast.makeText(FindbackActivity.this,
							webReturnData.message, Toast.LENGTH_SHORT).show();
				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(FindbackActivity.this, R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};
	private String telS;
	private String authCode;
	private String pswS;
	private String psw_againS;
	private RequestParams requestParams = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_findback);
		ViewUtils.inject(this);// 注解组件

		initData();

	}

	private void initData() {
		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				FindbackActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);
			}
		});
	}

	/**
	 * 获取验证码
	 * 
	 * @param v
	 */
	@OnClick(R.id.getcode)
	public void getcode(View v) {
		String text = tel.getText().toString();

		if (TextUtils.isEmpty(text)) {
			Toast.makeText(getApplicationContext(),
					R.string.register_way_error, Toast.LENGTH_SHORT).show();
			return;
		} else if (text.matches(Constants.EMAIL)) {// 邮箱找回
			url = ServerInterface.EMAIL_CHECK_CODE;
			requestParams = new RequestParams();
			requestParams.addBodyParameter("userEmail", text);
			requestParams.addBodyParameter("model", "RESET");
		} else if (text.matches(Constants.telRegex)) {// 手机找回
			url = ServerInterface.GET_AUTH_CODE_URL;
			requestParams = new RequestParams();
			requestParams.addBodyParameter("telNumber", text);
			requestParams.addBodyParameter("model", "RESET");
		} else {
			Toast.makeText(getApplicationContext(), R.string.way_error,
					Toast.LENGTH_SHORT).show();
			return;
		}
		ClothingHttpUtils.httpSend(HttpMethod.POST, url, requestParams,
				WebReturnData.class, handler, Constants.REQUEST_TAG_B,
				Constants.REQUEST_FAILURE);

	}

	/**
	 * 确认重置密码
	 * 
	 * @param v
	 */
	@OnClick(R.id.confirm)
	public void confirm(View v) {
		telS = tel.getText().toString();
		authCode = code.getText().toString();
		pswS = psw.getText().toString();
		psw_againS = psw_again.getText().toString();

		if (TextUtils.isEmpty(telS)) {
			Toast.makeText(getApplicationContext(),
					R.string.register_way_error, Toast.LENGTH_SHORT).show();
			return;
		} else if (!telS.matches(Constants.EMAIL)
				&& !telS.matches(Constants.telRegex)) {
			Toast.makeText(getApplicationContext(), R.string.way_error,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (TextUtils.isEmpty(pswS)) {
			Toast.makeText(getApplicationContext(), R.string.password_null,
					Toast.LENGTH_SHORT).show();
			return;
		} else if (pswS.matches(Constants.pswRegex)) {
			Toast.makeText(getApplicationContext(), R.string.password_error5,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (!pswS.equals(psw_againS)) {
			Toast.makeText(getApplicationContext(), R.string.password_error,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (!pswS.equals(psw_againS)) {// 验证再次输入密码是否一致
			psw.setText("");
			psw_again.setText("");
			Toast.makeText(FindbackActivity.this, R.string.password_error,
					Toast.LENGTH_SHORT).show();
			return;
		}
		url = ServerInterface.CHECK_CODE_URL;
		requestParams = new RequestParams();
		requestParams.addBodyParameter("checkCode", code.getText().toString());
		if (telS.matches(Constants.EMAIL)) {
			requestParams.addBodyParameter("userEmail", telS);
		} else {
			requestParams.addBodyParameter("telNumber", telS);
		}
		ClothingHttpUtils.httpSend(HttpMethod.POST, url, requestParams,
				WebReturnData.class, handler, Constants.REQUEST_TAG_C,
				Constants.REQUEST_FAILURE);

	}

	/*
	 * http://121.42.29.14:8080/fashion/mobileuser/sendCheckCodeToMobile.do?
	 * telNumber=13066907987&model=RESET
	 * 
	 * 根据telNumber和model来获取验证码，model有RESET和REGISTER两种类型,RESET是找回密码，REGISTER是注册
	 * 
	 * http://121.42.29.14:8080/fashion/mobileuser/checkMobileCheckCode.do?telNumber
	 * =13066907987&checkCode=9145
	 * 
	 * 校验验证码18824297860 7040
	 */

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
