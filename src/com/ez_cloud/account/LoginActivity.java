package com.ez_cloud.account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.bean.UserInfo;
import com.ez_cloud.bean.UserInfo.WebUser;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.home.HomeActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class LoginActivity extends Activity {

	@ViewInject(R.id.forget)
	TextView forget;
	@ViewInject(R.id.register)
	TextView register;
	@ViewInject(R.id.pb_loginProgressBar)
	ProgressBar progressBar;

	/** �ʺ������ */
	@ViewInject(R.id.accout)
	EditText accout;
	/** ��������� */
	@ViewInject(R.id.et_password)
	EditText passWord;
	/** ��½��ť */
	@ViewInject(R.id.btn_login)
	Button login;
	@ViewInject(R.id.login_view)
	View view;

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			progressBar.setVisibility(View.GONE);
			view.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:// ��½�ɹ�
				if (null != msg) {
					UserInfo userInfo = (UserInfo) msg.obj;
					if ("00".equals(userInfo.code)) {
						WebUser webUser = (WebUser) userInfo.WebUser;
						saveUserInformation(webUser);

						if (longin) {
							LoginActivity.this.startActivity(new Intent(
									LoginActivity.this, HomeActivity.class));
							longin = false;
							clothingApplication.setLongin(longin);
						}
						LoginActivity.this.finish();
						overridePendingTransition(R.anim.push_right_in,
								R.anim.push_right_out);

					} else {// ��½ʧ��
						Toast.makeText(LoginActivity.this, R.string.wrong, 0)
								.show();
					}
				}
				break;
			case Constants.REQUEST_FAILURE:// ��������ʧ��
				Toast.makeText(LoginActivity.this, R.string.server_error, 0).show();
				break;
			}
		}

	};
	private boolean longin;
	private ClothingApplication clothingApplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.firstpage);
		ViewUtils.inject(this);
		clothingApplication = (ClothingApplication) getApplication();
		longin = clothingApplication.isLongin();

		String userName = getIntent().getStringExtra("userName");
		accout.setText(userName);
		setListener();
	}

	/**
	 * �����û���Ĭ���ջ���ַ�绰����Ϣ
	 * 
	 * @param webUser
	 */
	protected void saveUserInformation(WebUser webUser) {
		SharedPreferencesUitls.saveString(getApplicationContext(), "address",
				webUser.address);
		SharedPreferencesUitls.saveString(getApplicationContext(), "email",
				webUser.email);
		SharedPreferencesUitls.saveString(getApplicationContext(), "password",
				webUser.password);
		SharedPreferencesUitls.saveString(getApplicationContext(), "realName",
				webUser.realName);
		SharedPreferencesUitls.saveString(getApplicationContext(), "regDate",
				webUser.regDate);
		SharedPreferencesUitls.saveString(getApplicationContext(), "score",
				webUser.score);
		SharedPreferencesUitls.saveString(getApplicationContext(), "telNumber",
				webUser.telNumber);
		SharedPreferencesUitls.saveString(getApplicationContext(), "userId",
				webUser.userId);
		SharedPreferencesUitls.saveString(getApplicationContext(), "userName",
				webUser.userName);
		SharedPreferencesUitls.saveString(getApplicationContext(), "yxbz",
				webUser.yxbz);
		SharedPreferencesUitls.saveString(getApplicationContext(), "zcfs",
				webUser.zcfs);
	}

	private void setListener() {
		forget.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,
						FindbackActivity.class));
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
			}

		});

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,
						RegisterActivity.class));
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
			}
		});
		// ��½
		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				progressBar.setVisibility(View.VISIBLE);
				view.setVisibility(View.VISIBLE);
				String username = accout.getText().toString();
				String password = passWord.getText().toString();
				if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
					Toast.makeText(LoginActivity.this, R.string.login_ts, 0)
							.show();
				}
				sendData(username, password);

			}
		});

	}

	private void sendData(String username, String password) {
		RequestParams params = new RequestParams();
		params.addBodyParameter("userName", username);
		params.addBodyParameter("password", password);
		ClothingHttpUtils.httpSend(HttpMethod.POST, ServerInterface.LOGINURL,
				params, UserInfo.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);
	}

}
