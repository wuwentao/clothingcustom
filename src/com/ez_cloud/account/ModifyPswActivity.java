package com.ez_cloud.account;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class ModifyPswActivity extends BaseActivity {

	/** 返回按钮 */
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	/** EditText 输入原密码 */
	@ViewInject(R.id.original_psw)
	EditText original_psw;
	/** EditText 输入新密码 */
	@ViewInject(R.id.new_psw)
	EditText new_psw;
	/** EditText 再次输入新密码 */
	@ViewInject(R.id.confirm_new_psw)
	EditText confirm_new_psw;
	/** 确认按钮 */
	@ViewInject(R.id.btn_confirm)
	Button btn_confirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_psw);
		ViewUtils.inject(this);// 注解组件

		userName = getIntent().getStringExtra("userName");

		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				ModifyPswActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);
			}
		});
	}

	/**
	 * 确认按钮
	 * 
	 * @param v
	 */
	@OnClick(R.id.btn_confirm)
	public void confirm(View v) {
		String psw = SharedPreferencesUitls.getString(getApplicationContext(),
				"password", "");
		String originalPsw = original_psw.getText().toString();
		String newPsw = new_psw.getText().toString();
		String confirmPsw = confirm_new_psw.getText().toString();

		if (TextUtils.isEmpty(originalPsw)) {
			Toast.makeText(getApplicationContext(),
					R.string.modpw_phone_number, Toast.LENGTH_SHORT).show();
			return;
		}

		if (TextUtils.isEmpty(newPsw)) {
			Toast.makeText(getApplicationContext(),
					R.string.modpw_password_error, Toast.LENGTH_SHORT).show();
			return;
		}

		if (!newPsw.equals(confirmPsw)) {
			Toast.makeText(getApplicationContext(), R.string.password_error,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (!originalPsw.equals(psw)) {
			Toast.makeText(ModifyPswActivity.this,
					R.string.password_error_hint, Toast.LENGTH_SHORT).show();
			original_psw.setText("");
			return;
		}

		if (!newPsw.equals(confirmPsw)) {
			Toast.makeText(ModifyPswActivity.this, R.string.password_error,
					Toast.LENGTH_SHORT).show();
			new_psw.setText("");
			confirm_new_psw.setText("");
			return;
		}
		String userId = SharedPreferencesUitls.getString(
				getApplicationContext(), "userId", "");

		RequestParams params = new RequestParams();
		params.addBodyParameter("userId", userId);
		params.addBodyParameter("newPassWord", newPsw);
		params.addBodyParameter("oldPassWord", originalPsw);

		ClothingHttpUtils.httpSend(HttpMethod.POST,
				ServerInterface.modifyPswUrl, params, WebReturnData.class,
				handler, Constants.REQUEST_SUCCEED, Constants.REQUEST_SUCCEED);
	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			WebReturnData modifyPswInfo = null;
			if (msg != null) {
				modifyPswInfo = (WebReturnData) msg.obj;
			}

			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (modifyPswInfo.code.equals("00")) {
					Toast.makeText(ModifyPswActivity.this,
							R.string.password_succed, Toast.LENGTH_SHORT)
							.show();
					SharedPreferencesUitls.saveString(getApplicationContext(),
							"userId", null);
					Intent intent = new Intent(ModifyPswActivity.this,
							LoginActivity.class);
					intent.putExtra("userName", userName);
					startActivity(intent);
					overridePendingTransition(R.anim.push_left_in,
							R.anim.push_left_out);
					finish();
				} else {
					Toast.makeText(ModifyPswActivity.this,
							modifyPswInfo.message, Toast.LENGTH_SHORT).show();
				}

				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(ModifyPswActivity.this, R.string.system_error,
						Toast.LENGTH_SHORT).show();
				break;

			}

		}
	};

	private String userName;

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
