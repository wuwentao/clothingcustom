package com.ez_cloud.account;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

/**
 * 注册用户
 * 
 * @author SCCS
 * 
 */
public class RegisterActivity extends BaseActivity {

	/** 返回按钮 */
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	/** 用户名输入框 */
	@ViewInject(R.id.username)
	EditText user_name;
	/** 验证码输入框 */
	@ViewInject(R.id.code)
	EditText code;
	/** 获取验证码按钮 */
	@ViewInject(R.id.getcode)
	Button getcode;
	/** 密码输入框 */
	@ViewInject(R.id.psw)
	EditText psw;
	/**
	 * 重新确认密码输入框
	 */
	@ViewInject(R.id.psw_again)
	EditText psw_again;
	/** 确认按钮 */
	@ViewInject(R.id.confirm)
	Button confirm;
	@ViewInject(R.id.progressBar1)
	ProgressBar progressBar1;
	private boolean isGainCode = false;
	private WebReturnData data;
	String url = null;
	RequestParams requestParams = null;

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (msg != null) {
				data = (WebReturnData) msg.obj;
			} else {
				return;
			}
			switch (msg.what) {
			case Constants.REQUEST_TAG_A:
				if ("00".equals(data.code)) {
					url = ServerInterface.CHECK_NUMBER_URL;
					requestParams = new RequestParams();
					requestParams.addBodyParameter("telNumber", userName);
					requestData(url, requestParams, Constants.REQUEST_TAG_B);
				} else {
					progressBar1.setVisibility(View.GONE);
					Toast.makeText(getApplicationContext(),
							R.string.username_error, Toast.LENGTH_SHORT).show();
					confirm.setEnabled(true);
					confirm.setClickable(true);
				}
				break;
			case Constants.REQUEST_TAG_B:
				if ("00".equals(data.code)) {
					if (isGainCode) {
						if (userName.matches(Constants.EMAIL)) {
							url = ServerInterface.EMAIL_CHECK_CODE;
							requestParams = new RequestParams();
							requestParams.addBodyParameter("userEmail",
									userName);
							requestParams.addBodyParameter("model", "REGIST");

						} else {
							url = ServerInterface.GET_AUTH_CODE_URL;
							requestParams = new RequestParams();
							requestParams.addBodyParameter("telNumber",
									userName);
							requestParams.addBodyParameter("model", "REGIST");
						}
						requestData(url, requestParams,
								Constants.REQUEST_SUCCEED);
						isGainCode = false;
					} else {
						String codeUrl = ServerInterface.CHECK_CODE_URL;
						requestParams = new RequestParams();
						if (userName.matches(Constants.EMAIL)) {
							requestParams.addBodyParameter("userEmail",
									userName);
						} else {
							requestParams.addBodyParameter("telNumber",
									userName);
						}
						requestParams.addBodyParameter("checkCode", checkCode);
						requestData(codeUrl, requestParams,
								Constants.REQUEST_TAG_C);
					}
				} else {
					progressBar1.setVisibility(View.GONE);
					Toast.makeText(getApplicationContext(),
							R.string.system_error, Toast.LENGTH_SHORT).show();
					confirm.setEnabled(true);
					confirm.setClickable(true);
				}
				break;
			case Constants.REQUEST_TAG_C:
				if ("00".equals(data.code)) {
					// isCheckCode=true;
					url = ServerInterface.registerUrl;
					requestParams = new RequestParams();
					requestParams.addBodyParameter("userName", userName);
					requestParams.addBodyParameter("password", pswS);
					if (userName.matches(Constants.EMAIL)) {
						requestParams.addBodyParameter("userEmail", userName);
					} else {
						requestParams.addBodyParameter("telNumber", userName);
					}
					requestParams.addBodyParameter("regModel", "android");
					requestData(url, requestParams, Constants.REQUEST_DATA);
				} else {
					progressBar1.setVisibility(View.GONE);
					Toast.makeText(getApplicationContext(),
							R.string.code_error, Toast.LENGTH_SHORT).show();
					confirm.setEnabled(true);
					confirm.setClickable(true);
				}
				break;
			case Constants.REQUEST_DATA:
				progressBar1.setVisibility(View.GONE);
				if ("00".equals(data.code)) {
					Toast.makeText(getApplicationContext(),
							R.string.register_succeed, Toast.LENGTH_SHORT)
							.show();
					Intent intent = new Intent(RegisterActivity.this,
							LoginActivity.class);
					intent.putExtra("userName", userName);
					startActivity(intent);
					RegisterActivity.this.finish();
				} else {
					Toast.makeText(getApplicationContext(),
							R.string.register_fail, Toast.LENGTH_SHORT).show();
					confirm.setEnabled(true);
					confirm.setClickable(true);
				}
				break;
			case Constants.REQUEST_SUCCEED:
				progressBar1.setVisibility(View.GONE);
				if ("00".equals(data.code)) {
					Toast.makeText(getApplicationContext(),
							R.string.code_send_succeed, Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(getApplicationContext(),
							R.string.way_invain, Toast.LENGTH_SHORT).show();
				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;
			}

		}
	};
	private String userName;
	private String checkCode;
	private String pswS;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_register);
		ViewUtils.inject(this);// 注解组件
		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				RegisterActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);
			}
		});

	}

	/** 触发获取验证码事件 */
	@OnClick(R.id.getcode)
	public void getCode(View v) {
		isGainCode = true;
		userName = user_name.getText().toString();
		if (TextUtils.isEmpty(userName)) {
			Toast.makeText(RegisterActivity.this, R.string.register_way_error,
					Toast.LENGTH_SHORT).show();
			return;
		} else if (userName.matches(Constants.telRegex)) {// 手机注册
			url = ServerInterface.CHECK_NUMBER_URL;
			requestParams = new RequestParams();
			requestParams.addBodyParameter("telNumber", userName);
			requestData(url, requestParams, Constants.REQUEST_TAG_B);

		} else if (userName.matches(Constants.EMAIL)) {// 邮箱注册
			url = ServerInterface.CHECK_EMAIL;
			requestParams = new RequestParams();
			requestParams.addBodyParameter("userEmail", userName);
			requestData(url, requestParams, Constants.REQUEST_TAG_B);
		} else {
			Toast.makeText(RegisterActivity.this, R.string.way_error,
					Toast.LENGTH_SHORT).show();
			return;
		}

	}

	/** 触发确认注册事件 */
	@OnClick(R.id.confirm)
	public void confirm(View v) {

		isGainCode = false;
		userName = user_name.getText().toString();
		checkCode = code.getText().toString();
		pswS = psw.getText().toString();
		String psw_againS = psw_again.getText().toString();

		if (TextUtils.isEmpty(userName)) {
			Toast.makeText(RegisterActivity.this, R.string.register_way_error,
					Toast.LENGTH_SHORT).show();
			return;
		} else if (!userName.matches(Constants.telRegex)
				&& !userName.matches(Constants.EMAIL)) {
			Toast.makeText(RegisterActivity.this, R.string.way_error,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (TextUtils.isEmpty(pswS)) {
			Toast.makeText(RegisterActivity.this, R.string.password_null,
					Toast.LENGTH_SHORT).show();
			return;
		} else if (!pswS.equals(psw_againS)) {
			Toast.makeText(getApplicationContext(), R.string.password_error,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (TextUtils.isEmpty(checkCode)) {
			Toast.makeText(RegisterActivity.this, R.string.code_null,
					Toast.LENGTH_SHORT).show();
			return;
		}

		progressBar1.setVisibility(View.VISIBLE);
		url = ServerInterface.CHECK_USERNAME_URL;
		requestParams = new RequestParams();
		requestParams.addBodyParameter("userName", userName);
		requestData(url, requestParams, Constants.REQUEST_TAG_A);

		confirm.setEnabled(false);
		confirm.setClickable(false);
	}

	public void requestData(String url, RequestParams params,
			final int requestTag) {

		ClothingHttpUtils.httpSend(HttpMethod.POST, url, params,
				WebReturnData.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);

	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
