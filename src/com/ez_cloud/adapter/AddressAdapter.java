package com.ez_cloud.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ez_cloud.bean.AddressInfo.Result;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.personalcentre.AddAddressActivity;
import com.ez_cloud.personalcentre.AddressActivity;
import com.ez_cloud.utils.SharedPreferencesUitls;

public class AddressAdapter extends BaseAdapter {
	private Context context;
	private List<Result> lists;
	private LayoutInflater inflater;
	private CheckBox moren;
	private Result resultInfo;

	public AddressAdapter(Context context, List<Result> lists) {
		this.context = context;
		this.lists = lists;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		/* return address!=null?address.size():0; */
		return lists != null ? lists.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lists.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		resultInfo = lists.get(position);
		convertView = inflater.inflate(R.layout.address_list_item, null);
		TextView name = (TextView) convertView
				.findViewById(R.id.myset_address_item_name);
		LinearLayout listViewItem = (LinearLayout) convertView
				.findViewById(R.id.listview_item);
		TextView provinces = (TextView) convertView
				.findViewById(R.id.myset_address_item_provinces);
		TextView street = (TextView) convertView
				.findViewById(R.id.myset_address_item_street);
		TextView phone = (TextView) convertView
				.findViewById(R.id.myset_address_phone);
		moren = (CheckBox) convertView
				.findViewById(R.id.loading_checkbox);
		ImageView amend = (ImageView) convertView
				.findViewById(R.id.itemdes_imageview);
		moren.setClickable(false);
		name.setText(resultInfo.receiver);
		provinces.setText(resultInfo.areaCity);
		street.setText(resultInfo.comAddress);
		phone.setText(resultInfo.telNumber);
		listViewItem.setTag(resultInfo.id);
		if (lists.get(position).xybz.equals("Y")) {
			moren.setChecked(true);
			SharedPreferencesUitls.saveString(context, "address", resultInfo.areaCity+resultInfo.comAddress);
			SharedPreferencesUitls.saveString(context, "takeName", resultInfo.receiver);
			SharedPreferencesUitls.saveString(context, "takeNumber", resultInfo.telNumber);
			SharedPreferencesUitls.saveString(context, "areaCode", resultInfo.areaCode);
		} else {
			moren.setChecked(false);
		}

		amend.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(((AddressActivity) context).getBaseContext(),AddAddressActivity.class);
				i.putExtra("activityTag", "amend");
				Bundle b=new Bundle();
				b.putString("id", lists.get(position).id);
				b.putString("receiver", lists.get(position).receiver);
				b.putString("telNumber", lists.get(position).telNumber);
				b.putString("areaCity", lists.get(position).areaCity);
				b.putString("comAddress", lists.get(position).comAddress);
				b.putString("postCode", lists.get(position).postCode);
				b.putString("xybz", lists.get(position).xybz);
				b.putString("areaCode", lists.get(position).areaCode);
				b.putString("id", lists.get(position).id);
				i.putExtras(b);
				context.startActivity(i);
				//				((AddressActivity)context).finish();
				lists.clear();
			}

		});
		
		notifyDataSetChanged();

		return convertView;
	}



}
