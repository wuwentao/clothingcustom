package com.ez_cloud.adapter;

import java.util.ArrayList;
import java.util.List;

import android.os.Handler;
import android.os.Message;

import com.ez_cloud.adapter.AddressInfo.Result;
import com.ez_cloud.utils.GsonUtils;
import com.ez_cloud.utils.ServerInterface;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class AddressEngine {
	public AddressInfo addressInfo;
	public List<Result> lists;
	public Result result;

	/** 向服务器发送数据，并获取从服务器返回的数据 */
	public void getResult(final Handler handler, String userId) {
		StringBuilder sb = new StringBuilder(ServerInterface.addressUrl);
		sb.append("userId=").append(userId);
		String url = sb.toString();
		HttpUtils http = new HttpUtils();
		http.send(HttpMethod.GET, url, null, new RequestCallBack<String>() {
			@Override
			public void onFailure(HttpException arg0, String arg1) {
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				addressInfo = GsonUtils.json2Bean(arg0.result,
						AddressInfo.class);
				lists = new ArrayList<Result>();
				if(addressInfo.result==null){
					Message message = Message.obtain();
					message.obj = lists;
					handler.sendMessage(message);
				}
				else{
					for (Result r : addressInfo.result) {
						lists.add(r);
					}
					Message message = Message.obtain();
					message.obj = lists;
					handler.sendMessage(message);
				}
				
			}
		});
	}
}
