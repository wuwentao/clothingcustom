package com.ez_cloud.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ez_cloud.bean.OrderInfo.Result;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.MyBitmapUtils;

public class LogisticsAdapter extends BaseAdapter {

	private Context context;
	private List<Result> lists;
	private LayoutInflater inflater;

	public LogisticsAdapter(Context context, List<Result> lists) {
		this.context = context;
		this.lists = lists;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lists != null ? lists.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.order_item, null);
			viewHolder = new ViewHolder();
			viewHolder.listViewItem = (RelativeLayout) convertView
					.findViewById(R.id.rl_orid_item);
			viewHolder.order_picture_iv = (ImageView) convertView
					.findViewById(R.id.orid_picture_iv);
			viewHolder.price_tv = (TextView) convertView
					.findViewById(R.id.tv_orid_price);
			viewHolder.order_number_tv = (TextView) convertView
					.findViewById(R.id.tv_orid_orderid);
			viewHolder.clothName=(TextView) convertView.findViewById(R.id.tv_orid_clothname);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		String path=lists.get(position).path;
		if(path.equals("")){
			viewHolder.order_picture_iv.setBackgroundResource(R.drawable.placehold_pi);
		}
		else{
//			MyBitmapUtils.downImage(path, viewHolder.order_picture_iv, true);
			MyBitmapUtils.display(context, viewHolder.order_picture_iv, path);
		}
		String priceStr = lists.get(position).price;
		if (priceStr.equals("")) {
			viewHolder.price_tv.setText("0.00");
		} else {
			float unit_price = Float.parseFloat(priceStr);
			int number = lists.get(position).preNum;
			float total_price = unit_price * number;
			viewHolder.price_tv.setText("" + total_price);
		}
		viewHolder.order_number_tv.setText(lists.get(position).orderId);
		viewHolder.clothName.setText(lists.get(position).clothName);
		return convertView;
	}

	class ViewHolder {
		RelativeLayout listViewItem;
		ImageView order_picture_iv;
		TextView  clothName, price_tv, order_number_tv;
	}
}
