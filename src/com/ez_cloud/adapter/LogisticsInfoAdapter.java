package com.ez_cloud.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ez_cloud.bean.LogisticsInfo.Data;
import com.ez_cloud.clothingcustom.R;

public class LogisticsInfoAdapter extends BaseAdapter {

	private List<Data> dataLists;
	private LayoutInflater inflater;
	LinearLayout listViewItem;
	TextView logistics_info_tv,time;
	public LogisticsInfoAdapter(Context context, List<Data> dataLists) {
		this.dataLists = dataLists;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataLists != null ? dataLists.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = inflater.inflate(R.layout.logistics_info_item, null);
		listViewItem = (LinearLayout) convertView.findViewById(R.id.listview_item);
		logistics_info_tv = (TextView) convertView.findViewById(R.id.logistics_info_tv);
		time = (TextView) convertView.findViewById(R.id.tv_logistics_info_time);
		
		String ftime=dataLists.get(position).ftime;
		time.setText(ftime);
		String context=dataLists.get(position).context;
		logistics_info_tv.setText(context);
		
		//下面这里需要把第一项的字体设置为红色，但是这里逻辑有问题，暂时未设置
		if(position==0){
			time.setTextColor(Color.RED);
			logistics_info_tv.setTextColor(Color.RED);
		}
//		if(logistics_info_tv.getText().toString().equals(dataLists.get(0).context)){
//			logistics_info_tv.setTextColor(Color.RED);
//		}
		return convertView;
	}

}
