package com.ez_cloud.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ez_cloud.bean.OrderStatusInfo.OrderResultInfo;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.MyBitmapUtils;

public class RefundAdapter extends BaseAdapter {

	private Context context;
	private List<OrderResultInfo> lists;
	private LayoutInflater inflater;

	public RefundAdapter(Context context, List<OrderResultInfo> lists) {
		this.context = context;
		this.lists = lists;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lists != null ? lists.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lists.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder holder=null;
	OrderResultInfo info;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		holder=new ViewHolder();
		if(convertView==null){
			convertView=inflater.inflate(R.layout.refund_item, null);
			holder.clothPic=(ImageView) convertView.findViewById(R.id.refund_picture_iv);
			holder.clothName=(TextView) convertView.findViewById(R.id.tv_refund_clothname);
			holder.clothPrice=(TextView) convertView.findViewById(R.id.tv_refund_price);
			holder.moneySum=(TextView) convertView.findViewById(R.id.tv_refund_moneysum);
			holder.oridNum=(TextView) convertView.findViewById(R.id.tv_refund_orderid);
			holder.oridShare=(ImageView) convertView.findViewById(R.id.iv_refund_state);
			convertView.setTag(holder);
		}else{
			holder=(ViewHolder) convertView.getTag();
		}
		info=lists.get(position);
		holder.oridNum.setText(info.orderId);
		holder.clothName.setText(info.clothName);
		MyBitmapUtils.display(context, holder.clothPic, info.path);
//		MyBitmapUtils.downImage(info.path, holder.clothPic, true);
		if("2".equals(info.orderAreaCode)){
			holder.clothPrice.setText("NTD"+info.price+"*"+info.preNum);
			holder.moneySum.setText("NTD"+info.totalPrice);
		}else{
			holder.clothPrice.setText("�� "+info.price+"*"+info.preNum);
			holder.moneySum.setText("��"+info.totalPrice);
		}
		
		return convertView;
	}
	class ViewHolder {
		ImageView clothPic,oridShare;
		TextView clothName, clothPrice, moneySum, oridNum;
	}
}
