package com.ez_cloud.adapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.bean.OrderStatusInfo.OrderResultInfo;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.GsonUtils;
import com.ez_cloud.utils.MyBitmapUtils;
import com.ez_cloud.utils.ServerInterface;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class UnpaidOrderAdapter extends BaseAdapter {
	public List<OrderResultInfo> mData;  
	public Map<Integer, Boolean> isSelected;
	private Context context;
	private List<OrderResultInfo> lists;
	private LayoutInflater inflater;
	private ProgressBar mProgressBar;
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			mProgressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				//				Toast.makeText(context, R.string.deleorder_succeed, 0).show();
				//				((UnpaidActivity) context).request();
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(context, R.string.server_error, 0).show();
				break;
			default:
				break;
			}
		};
	};




	public UnpaidOrderAdapter(Context context, List<OrderResultInfo> lists, ProgressBar mProgressBar) {
		this.context = context;
		this.lists = lists;
		this.inflater = LayoutInflater.from(context);
		this.mProgressBar=mProgressBar;
		init(); 
	}
	private void init() {
		mData=new ArrayList<OrderResultInfo>();    
		for (int i = 0; i < lists.size(); i++) {    
			mData.add(lists.get(i));    
		} 
		isSelected = new HashMap<Integer, Boolean>();    
		for (int i = 0; i < lists.size(); i++) {    
			isSelected.put(i, false);    
		}    
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lists != null ? lists.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lists.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder holder=null;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final OrderResultInfo info;
		if(convertView==null){
			holder=new ViewHolder();
			convertView=inflater.inflate(R.layout.unpaid_item, null);
			holder.clothPic=(ImageView) convertView.findViewById(R.id.unpaid_picture_iv);
			holder.clothName=(TextView) convertView.findViewById(R.id.tv_unpaid_clothname);
			holder.clothPrice=(TextView) convertView.findViewById(R.id.tv_unpaid_price);
			holder.moneySum=(TextView) convertView.findViewById(R.id.tv_unpaid_moneysum);
			holder.oridNum=(TextView) convertView.findViewById(R.id.tv_unpaid_orderid);
			holder.pay=(ImageView) convertView.findViewById(R.id.iv_unpaid_pay);
			convertView.setTag(holder);
		}else{
			holder=(ViewHolder) convertView.getTag();
		}
		info=mData.get(position);
		holder.oridNum.setText(info.orderId);
		holder.clothName.setText(info.clothName);
				MyBitmapUtils.display(context, holder.clothPic, info.path);
//		MyBitmapUtils.downImage(info.path, holder.clothPic, true);
		if("2".equals(info.orderAreaCode)){
			holder.clothPrice.setText("NTD "+info.price+"*"+info.preNum);
			holder.moneySum.setText("NTD "+info.totalPrice);
		}else{
			holder.clothPrice.setText("�� "+info.price+"*"+info.preNum);
			holder.moneySum.setText("�� "+info.totalPrice);
		}
		
		if (isSelected.get(position)) {
			convertView.setBackgroundResource(R.drawable.list_yellow_single);
		}
		else {
			convertView.setBackgroundResource(R.drawable.select);
		}
		holder.pay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(context, info.orderId, 0).show();
					Toast.makeText(context, R.string.twfkts, 1).show();
			}
		});
		return convertView;
	}
	class ViewHolder {
		ImageView clothPic;
		ImageView pay;
		TextView clothName, clothPrice, moneySum, oridNum;
	}
	public void delItem(Collection<OrderResultInfo> arg){
		mProgressBar.setVisibility(View.VISIBLE);
		for(OrderResultInfo info:arg){
			requestData(ServerInterface.DEIETE_ORDER+info.orderId);
		}
		lists.removeAll(arg);
		init();

	}

	private void requestData(String url) {
		HttpUtils httpUtils = new HttpUtils();
		httpUtils.send(HttpMethod.GET, url, new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				WebReturnData json2Bean = GsonUtils.json2Bean(arg0.result, WebReturnData.class);
				Message msg=Message.obtain();
				if("00".equals(json2Bean.code)){
					msg.what=Constants.REQUEST_SUCCEED;
				}else{
					msg.what=Constants.REQUEST_FAILURE;
				}
				mHandler.sendMessage(msg);
			}
		});
	}

}
