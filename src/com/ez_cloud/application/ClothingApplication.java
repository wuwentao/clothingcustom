package com.ez_cloud.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Application;
import android.widget.ImageView;

import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.OneCustomize.Result;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class ClothingApplication extends Application {
	// 一键定制的数据对象
	private Result oneCustomizeInfo;
	// 标准定制的数据集合
	private Map<String, String> mMap;

	private List<ImageView> checkList;
	// 管理运行的activity集合
	private List<BaseActivity> activityList;
	// 记录版型历史记录的集合
	private Map<String, Integer> historyMaps;// 版型历史记录
	private boolean isCheck;
	private boolean isStart;
	private boolean isLongin;



	public static GoogleAnalytics analytics;
	public static Tracker tracker;
	@Override
	public void onCreate() {
		super.onCreate();
		
		CrashHandler crashHandler = CrashHandler.getInstance();
		crashHandler.init(getApplicationContext());
		setOneCustomizeInfo(oneCustomizeInfo);
		setmMap(new HashMap<String, String>());
		setCheckList(new ArrayList<ImageView>());
		setActivityList(new ArrayList<BaseActivity>());
		setHistoryMaps(new HashMap<String, Integer>());
		setCheck(false);
		setStart(false);
		setLongin(false);
		
		new OpenGoogleAnalytics().start();
	}

	public  GoogleAnalytics analytics() {
        return analytics;
    }

	class OpenGoogleAnalytics extends Thread{
		public void run(){
			analytics = GoogleAnalytics.getInstance(ClothingApplication.this);
			
		    analytics.setLocalDispatchPeriod(1800);

		    tracker = analytics.newTracker("UA-69154352-1"); 
		    tracker.enableExceptionReporting(true);
		    tracker.enableAdvertisingIdCollection(true);
		    tracker.enableAutoActivityTracking(true);
		}
	}
    /**
     * The default app tracker. If this method returns null you forgot to either set
     * android:name="&lt;this.class.name&gt;" attribute on your application element in
     * AndroidManifest.xml or you are not setting this.tracker field in onCreate method override.
     */
    public static Tracker tracker() {
        return tracker;
    }
	
	
	/**
	 * 销毁所有已标记的activity
	 */
	public void destroyActivity() {
		historyMaps.clear();

		for (int i = 0; i < activityList.size(); i++) {
			activityList.get(i).mFinish();
		}
	}

	public Result getOneCustomizeInfo() {
		return oneCustomizeInfo;
	}

	public void setOneCustomizeInfo(Result oneCustomizeInfo) {
		this.oneCustomizeInfo = oneCustomizeInfo;
	}

	public Map<String, String> getmMap() {
		return mMap;
	}

	public void setmMap(Map<String, String> mMap) {
		this.mMap = mMap;
	}

	public List<ImageView> getCheckList() {
		return checkList;
	}

	public void setCheckList(List<ImageView> checkList) {
		this.checkList = checkList;
	}


	public List<BaseActivity> getActivityList() {
		return activityList;
	}

	public void setActivityList(List<BaseActivity> activityList) {
		this.activityList = activityList;
	}

	public boolean isCheck() {
		return isCheck;
	}

	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}

	public boolean isStart() {
		return isStart;
	}

	public void setStart(boolean isStart) {
		this.isStart = isStart;
	}

	public boolean isLongin() {
		return isLongin;
	}

	public void setLongin(boolean isLongin) {
		this.isLongin = isLongin;
	}

	public Map<String, Integer> getHistoryMaps() {
		return historyMaps;
	}

	public void setHistoryMaps(Map<String, Integer> historyMaps) {
		this.historyMaps = historyMaps;
	}

}
