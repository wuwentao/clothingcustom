package com.ez_cloud.appshare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.bean.BuyerShareInfo;
import com.ez_cloud.bean.BuyerShareInfo.ShareResultInfo;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.theme.ThemeActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.GsonUtils;
import com.ez_cloud.utils.Helper;
import com.ez_cloud.utils.ImageFetcher;
import com.ez_cloud.utils.Member;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.Utils;
import com.ez_cloud.view.ScaleImageView;
import com.ez_cloud.view.XListView;
import com.ez_cloud.view.XListView.IXListViewListener;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class BuyerShareActivity extends FragmentActivity implements
		IXListViewListener, OnClickListener {
	private ImageFetcher mImageFetcher;
	private XListView mAdapterView = null;
	private StaggeredAdapter mAdapter = null;
	private int currentPage = 1;
	ContentTask task = new ContentTask(this, 1);
	private ImageView iv_share_back;
	private ImageView share_release;
	private List<ShareResultInfo> shareInfos;

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == Constants.REQUEST_TAG_A) {
				currentPage--;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_buyershare);

		// 获取系统语言环境
		country = Utils.getCountry(this);

		AddItemToContainer(1, 1, "N");

		iv_share_back = (ImageView) findViewById(R.id.iv_share_back);
		share_release = (ImageView) findViewById(R.id.iv_share_release);
		iv_share_back.setOnClickListener(this);
		share_release.setOnClickListener(this);
		mAdapterView = (XListView) findViewById(R.id.list);

		mAdapterView.setPullLoadEnable(true);
		mAdapterView.setXListViewListener(this);
		mAdapter = new StaggeredAdapter(this, mAdapterView);

		mImageFetcher = new ImageFetcher(this, 240);
		mImageFetcher.setLoadingImage(R.drawable.empty_photo);
		mImageFetcher.setExitTasksEarly(false);
		mAdapterView.setAdapter(mAdapter);
	}

	private String newShareId = "11111";// 记录最新分享数据的shareID
	boolean isFirst = true;
	private String country;

	private class ContentTask extends
			AsyncTask<String, Integer, List<ShareResultInfo>> {

		private Context mContext;
		private int type = 1;

		public ContentTask(Context context, int type1) {
			super();
			mContext = context;
			type = type1;
		}

		@Override
		protected List<ShareResultInfo> doInBackground(String... params) {
			try {
				return parseNewsJSON(params[0]);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<ShareResultInfo> result) {
			if(result!=null&&result.size()>0){
				if (type == 1) {
					if (newShareId.equals(result.get(0).shareId)) {
						Toast.makeText(BuyerShareActivity.this, R.string.no_updata,
								Toast.LENGTH_SHORT).show();
					} else {
						newShareId = result.get(0).shareId;
						mAdapter.addItemTop(result);
						mAdapter.notifyDataSetChanged();
					}
					mAdapterView.stopRefresh();
					
				} else if (type == 2) {
					mAdapterView.stopLoadMore();
					mAdapter.addItemLast(result);
					mAdapter.notifyDataSetChanged();
				}
			}else{
				Toast.makeText(BuyerShareActivity.this, R.string.no_share_data,
						Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected void onPreExecute() {
		}

		public List<ShareResultInfo> parseNewsJSON(String url)
				throws IOException {
			shareInfos = new ArrayList<ShareResultInfo>();
			String json = "";
			if (Helper.checkConnection(mContext)) {
				try {
					json = Helper.getStringFromUrl(url);

				} catch (IOException e) {
					Log.e("IOException is : ", e.toString());
					e.printStackTrace();
					return shareInfos;
				}
			}
			Log.d("MainActiivty", "json:" + json);

			BuyerShareInfo json2Bean = GsonUtils.json2Bean(json,
					BuyerShareInfo.class);
			if ("00".equals(json2Bean.code) && json2Bean.result.size() > 0) {
				shareInfos = json2Bean.result;
			} else {
				Message msg = Message.obtain();
				msg.what = Constants.REQUEST_TAG_A;
				handler.sendMessage(msg);
			}

			return shareInfos;
		}
	}

	/**
	 * 添加内容
	 * 
	 * @param pageindex
	 * @param type
	 *            1为下拉刷新 2为加载更多
	 */
	private void AddItemToContainer(int pageindex, int type1, String order) {
		if (task.getStatus() != Status.RUNNING) {
			// String url = ServerInterface.SHARE_ORDER_URL +
			// pageindex+"&country="+country;
			String url = ServerInterface.SHARE_ORDER_URL + pageindex
					+ "&country=" + "CH";
			Log.d("MainActivity", "current url:" + url);
			ContentTask task = new ContentTask(this, type1);
			task.execute(url);
		}
	}

	// private TextView approveNumber;
	public class StaggeredAdapter extends BaseAdapter {
		private Context mContext;
		private LinkedList<ShareResultInfo> mInfos;

		public StaggeredAdapter(Context context, XListView xListView) {
			mContext = context;
			mInfos = new LinkedList<ShareResultInfo>();
		}

		String width;
		String height;
		ViewHolder holder = null;

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			final ShareResultInfo buyerShareInfo = mInfos.get(position);
			final TextView approveNumber;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = View.inflate(mContext, R.layout.infos_list, null);
				holder.imageView = (ScaleImageView) convertView
						.findViewById(R.id.news_pic);
				holder.contentView = (TextView) convertView
						.findViewById(R.id.tv_buyershare_comment);
				holder.userName = (TextView) convertView
						.findViewById(R.id.tv_buyershare_nicheng);
				holder.userHead = (ImageView) convertView
						.findViewById(R.id.iv_buyershare_touxiang);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			final ImageView approve = (ImageView) convertView
					.findViewById(R.id.approve);
			approveNumber = (TextView) convertView
					.findViewById(R.id.iv_buyershare_approve_bumber);

			if (null != buyerShareInfo.picWidth
					&& !"".equals(buyerShareInfo.picWidth))
				width = buyerShareInfo.picWidth;
			if (null != buyerShareInfo.picHeight
					&& !"".equals(buyerShareInfo.picHeight))
				height = buyerShareInfo.picHeight;

			float widthFloat = Float.parseFloat(width);
			float heightFloat = Float.parseFloat(height);
			holder.imageView.setImageWidth((int) widthFloat);
			holder.imageView.setImageHeight((int) heightFloat);
			holder.userName.setText(buyerShareInfo.userName);
			approveNumber.setText(buyerShareInfo.approve);
			if ("".equals(buyerShareInfo.userPicPath)) {
				holder.userHead.setImageResource(R.drawable.heardimage);
			} else {
				new BitmapUtils(getApplicationContext()).display(
						holder.userHead, buyerShareInfo.userPicPath);
			}
			holder.contentView.setText(buyerShareInfo.comment);

			// buyerShareInfo.approve=number+"";
			approve.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					requestData(ServerInterface.SHARE_APPROVE_URL
							+ buyerShareInfo.shareId);
					approve.setImageResource(R.drawable.approve02);
					approve.setClickable(false);
					approveNumber.setText(Integer.parseInt(approveNumber
							.getText().toString()) + 1 + "");

				}
			});

			int picNum = Integer.parseInt(buyerShareInfo.picNum);
			for (int i = 0; i < picNum; i++) {

			}
			// 设置图片
			// mImageFetcher.loadImage(buyerShareInfo.picPath,
			// holder.imageView);
			new BitmapUtils(BuyerShareActivity.this).display(holder.imageView,
					buyerShareInfo.picPath);
			holder.imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(BuyerShareActivity.this,
							ShareParticularActivity.class);
					Bundle bundle = new Bundle();
					bundle.putSerializable("ShareResultInfo", buyerShareInfo);
					intent.putExtras(bundle);
					startActivity(intent);
					overridePendingTransition(R.anim.push_right_in,
							R.anim.push_right_out);
				}
			});

			return convertView;
		}

		class ViewHolder {
			ScaleImageView imageView;
			TextView userName, contentView, timeView, approveNumber;
			ImageView userHead;

		}

		@Override
		public int getCount() {
			return mInfos.size();
		}

		@Override
		public Object getItem(int arg0) {
			return mInfos.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		public void addItemLast(List<ShareResultInfo> datas) {
			mInfos.addAll(datas);
		}

		public void addItemTop(List<ShareResultInfo> datas) {
			mInfos.clear();
			for (ShareResultInfo info : datas) {
				mInfos.addLast(info);
			}
		}
	}

	// private int height;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_share_back:
			startActivity(new Intent(this, ThemeActivity.class));
			this.finish();
			overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			break;
		case R.id.iv_share_release:
			if (Member.isMember(BuyerShareActivity.this)) {

				startActivity(new Intent(BuyerShareActivity.this,
						MyShareActivity.class));
				this.finish();
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);

			}

			break;

		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	/**
	 * 请求服务器
	 * 
	 * @param url
	 * @param x
	 */
	private void requestData(String url) {
		ClothingHttpUtils.httpSend(HttpMethod.GET, url, null,
				WebReturnData.class, handler, Constants.COMMENT_APPROVE,
				Constants.REQUEST_FAILURE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return true;
	}

	@Override
	public void onRefresh() {
		Log.d("下拉更新----", currentPage + "");
		try {
			AddItemToContainer(1, 1, "N");
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	public void onLoadMore() {
		Log.d("上拉加载----", currentPage++ + "");
		try {
			AddItemToContainer(currentPage + 1, 2, "N");
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
