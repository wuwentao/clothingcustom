package com.ez_cloud.appshare;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.MyBitmapUtils;
import com.ez_cloud.utils.PhotoUtil;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class MyShareActivity extends Activity implements OnClickListener {
	@ViewInject(R.id.tv_share_me_share)
	ImageView me_share;// 返回ME_分享
	@ViewInject(R.id.tv_myshare_share)
	ImageView share;// 分享
	@ViewInject(R.id.et_myshare_comment)
	EditText userComment;// 评论
	@ViewInject(R.id.ll_myshare_all)
	RelativeLayout layout_all;
	@ViewInject(R.id.iv_myshare_image1)
	ImageView image1;// 缩略图1
	@ViewInject(R.id.iv_myshare_image2)
	ImageView image2;// 缩略图2
	@ViewInject(R.id.iv_myshare_image3)
	ImageView image3;// 缩略图3
	@ViewInject(R.id.iv_myshare_image4)
	ImageView image4;// 缩略图4
	@ViewInject(R.id.iv_myshare_delete1)
	ImageView delede1;// 撤销1
	@ViewInject(R.id.iv_myshare_delete2)
	ImageView delede2;// 撤销2
	@ViewInject(R.id.iv_myshare_delete3)
	ImageView delede3;// 撤销3
	@ViewInject(R.id.iv_myshare_delete4)
	ImageView delede4;// 撤销4
	@ViewInject(R.id.iv_myshare_add_Image)
	ImageView addImage;// 添加照片
	@ViewInject(R.id.iv_myshare_add1_Image)
	ImageView addImage1;// 添加照片
	@ViewInject(R.id.myshare_pb)
	ProgressBar mProgressBar;//
	// @ViewInject(R.id.tv_myshare_location_yes)TextView locationYse;//定位
	// @ViewInject(R.id.tv_myshare_location_no)TextView locationNo;//不定位
	// @ViewInject(R.id.tv_myshare_loaction_text)TextView locationText;//位置信息

	private boolean flag1 = false;
	private boolean flag2 = false;
	private boolean flag3 = false;
	private boolean flag4 = false;
	private String orderId = "";
	private String style = "";
	private Bitmap cameraBitmap;
	private List<Drawable> thumbnailList;// 存放缩略图的集合
	private List<Bitmap> bitmapList;// 存放小图的集合
	// private float latitude;//维度
	// private float longitude;//经度
	public Button ReLBSButton = null;
	public static String TAG = "msg";
	// private LocationClient mLocationClient;
	// public BDLocationListener myListener;
	private List<ImageView> images = new ArrayList<ImageView>();
	private Handler handler = new Handler() {
		// 当主线程消息栈有消息时执行此方法
		public void handleMessage(android.os.Message msg) {
			mProgressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					WebReturnData mWebReturnData = (WebReturnData) msg.obj;
					if ("00".equals(mWebReturnData.code)) {
						MyShareActivity.this
								.startActivity(new Intent(MyShareActivity.this,
										BuyerShareActivity.class));
						MyShareActivity.this.finish();
					}
				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_my_share);
		ViewUtils.inject(this);
		ShareSDK.initSDK(this);
		bitmapList = new ArrayList<Bitmap>();
		init();
	}

	private void init() {
		thumbnailList = new ArrayList<Drawable>();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		me_share.setOnClickListener(this);
		share.setOnClickListener(this);

		// locationYse.setOnClickListener(this);
		// locationNo.setOnClickListener(this);
		// locationYse.setOnClickListener(this);
		// locationNo.setOnClickListener(this);
		image1.setOnClickListener(this);
		image2.setOnClickListener(this);
		image3.setOnClickListener(this);
		image4.setOnClickListener(this);
		delede1.setOnClickListener(this);
		delede2.setOnClickListener(this);
		delede3.setOnClickListener(this);
		delede4.setOnClickListener(this);
		addImage1.setOnClickListener(this);
		addImage.setOnClickListener(this);
		images.add(image1);
		images.add(image2);
		images.add(image3);
		images.add(image4);
		// mLocationClient = new LocationClient(this.getApplicationContext());
		// myListener = new MyLocationListener();
		// mLocationClient.registerLocationListener(myListener);
		//
		// LocationClientOption locationOption = new LocationClientOption();
		// locationOption.setOpenGps(true);
		// locationOption.setCoorType("bd09ll");
		// locationOption.setAddrType("all");
		// locationOption.setProdName("BaiduLocation");
		// locationOption.setScanSpan(5000);//设置发起定位请求的间隔时间为5000ms
		// locationOption.disableCache(true);//禁止启用缓存定位
		// locationOption.setPoiExtraInfo(true); //是否需要POI的电话和地址等详细信息
		// mLocationClient.setLocOption(locationOption);
		//
		// Log.i("Baidu", "BaiduMapMyLocationActivity 开启定位");
		// mLocationClient.start();
	}

	// /**
	// * 位置定位监听
	// * @author
	// *
	// */
	// public class MyLocationListener implements BDLocationListener {
	// @Override
	// public void onReceiveLocation(BDLocation location) {
	// if (location == null)
	// return ;
	// latitude=(float) location.getLatitude();
	// longitude=(float) location.getLongitude();
	// locationText.setText(location.getAddrStr());
	// }
	// public void onReceivePoi(BDLocation poiLocation) {
	// if (poiLocation == null){
	// return ;
	// }
	// latitude=(float) poiLocation.getLatitude();
	// longitude=(float) poiLocation.getLongitude();
	// locationText.setText(poiLocation.getAddrStr());
	// }
	// }
	//
	// @Override
	// public void onDestroy() {
	// mLocationClient.stop();//停止定位
	// locationText = null;
	// super.onDestroy();
	// }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_share_me_share:
			Intent intent = new Intent(this, BuyerShareActivity.class);
			startActivity(intent);
			this.finish();
			break;
		case R.id.tv_myshare_share:
			if (thumbnailList.size() > 0) {
				merger();// 合并图片
				showShare();
			} else {
				Toast.makeText(getApplicationContext(), R.string.image_commit,
						Toast.LENGTH_SHORT).show();
				return;
			}
			break;
		// case R.id.tv_myshare_location_yes:
		// locationYse.setVisibility(View.INVISIBLE);
		// locationText.setVisibility(View.INVISIBLE);
		// locationNo.setVisibility(View.VISIBLE);
		// mLocationClient.stop();//停止定位
		// break;
		// case R.id.tv_myshare_location_no:
		// mLocationClient.start();//开始定位
		// locationNo.setVisibility(View.INVISIBLE);
		// locationYse.setVisibility(View.VISIBLE);
		// locationText.setVisibility(View.VISIBLE);
		// break;
		case R.id.iv_myshare_add_Image:// 添加图片
		case R.id.iv_myshare_add1_Image:
			showAvatarPop();
			break;
		case R.id.iv_myshare_image1:
			if (flag1) {
				delede1.setVisibility(View.GONE);
				flag1 = !flag1;
			} else {
				delede1.setVisibility(View.VISIBLE);
				flag1 = !flag1;
			}
			break;
		case R.id.iv_myshare_image2:
			if (flag2) {
				delede2.setVisibility(View.GONE);
				flag2 = !flag2;
			} else {
				delede2.setVisibility(View.VISIBLE);
				flag2 = !flag2;
			}
			break;
		case R.id.iv_myshare_image3:

			if (flag3) {
				delede3.setVisibility(View.GONE);
				flag3 = !flag3;
			} else {
				delede3.setVisibility(View.VISIBLE);
				flag3 = !flag3;
			}
			break;
		case R.id.iv_myshare_image4:
			if (flag4) {
				delede4.setVisibility(View.GONE);
				flag4 = !flag4;
			} else {
				delede4.setVisibility(View.VISIBLE);
				flag4 = !flag4;
			}
			break;
		case R.id.iv_myshare_delete1:
			delede1.setVisibility(View.GONE);
			thumbnailList.remove(0);
			bitmapList.remove(0);
			setAddImageView();

			break;
		case R.id.iv_myshare_delete2:
			delede2.setVisibility(View.GONE);
			thumbnailList.remove(1);
			bitmapList.remove(1);
			setAddImageView();

			break;
		case R.id.iv_myshare_delete3:
			delede3.setVisibility(View.GONE);
			thumbnailList.remove(2);
			bitmapList.remove(2);
			setAddImageView();

			break;
		case R.id.iv_myshare_delete4:
			delede4.setVisibility(View.GONE);
			thumbnailList.remove(3);
			bitmapList.remove(3);
			setAddImageView();

			break;
		}

	}

	private void setAddImageView() {
		for (int i = 0; i < images.size(); i++) {
			images.get(i).setVisibility(View.GONE);
		}

		for (int i = 0; i < thumbnailList.size(); i++) {
			images.get(i).setVisibility(View.VISIBLE);
			images.get(i).setBackgroundDrawable(thumbnailList.get(i));
		}
		if (thumbnailList.size() < 2) {
			addImage1.setVisibility(View.GONE);
			addImage.setVisibility(View.VISIBLE);
		} else if (thumbnailList.size() == 2 || thumbnailList.size() == 3) {
			addImage.setVisibility(View.GONE);
			addImage1.setVisibility(View.VISIBLE);
		} else if (thumbnailList.size() == 4) {
			addImage1.setVisibility(View.GONE);
			addImage.setVisibility(View.GONE);
		}
	}

	public void appShare() {
		mProgressBar.setVisibility(View.VISIBLE);
		String url;
		RequestParams requestParams = new RequestParams();
		if (thumbnailList.size() < 1) {
			Toast.makeText(getApplicationContext(), R.string.image_commit,
					Toast.LENGTH_SHORT).show();
			return;
		}
		url = ServerInterface.MYSHARE_COMMIT_URL;
		requestParams.addBodyParameter("userId", SharedPreferencesUitls
				.getString(getApplicationContext(), "userId", ""));
		requestParams.addBodyParameter("orderId", orderId);
		requestParams.addBodyParameter("style", style);
		requestParams
				.addBodyParameter("picWidth", cameraBitmap.getWidth() + "");
		requestParams.addBodyParameter("picHeight", cameraBitmap.getHeight()
				+ "");
		requestParams.addBodyParameter("smPicWidth", cameraBitmap.getWidth()
				+ "");
		requestParams.addBodyParameter("smPicHeight", cameraBitmap.getHeight()
				+ "");
		requestParams.addBodyParameter("picNum", thumbnailList.size() + "");
		requestParams.addBodyParameter("comment", userComment.getText()
				.toString());
		requestParams.addBodyParameter("longitude", "");
		requestParams.addBodyParameter("latitude", "");
		File file = new File(Environment.getExternalStorageDirectory()
				+ "/largeshare.jpg");
		requestParams.addBodyParameter("file", file);
		for (int i = 0; i < thumbnailList.size(); i++) {
			File file2 = new File(Environment.getExternalStorageDirectory()
					+ "/file" + (i + 1) + ".jpg");
			if (file2.exists()) {
				requestParams.addBodyParameter("file" + i + 1, file2);
				Log.d("file" + i + "=", file2.getPath());
			}
		}

		commitShare(url, requestParams);
	}

	/**
	 * 提交分享数据到服务器
	 * 
	 * @param requestParams
	 */
	private void commitShare(String url, RequestParams params) {
		ClothingHttpUtils.httpSend(HttpMethod.POST, url, params,
				WebReturnData.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);

	}

	/**
	 * 处理选择的照片
	 * 
	 * @param i
	 */
	private void setImage() {
		for (int i = 0; i < thumbnailList.size(); i++) {
			switch (i) {
			case 0:
				image1.setVisibility(View.VISIBLE);
				image1.setBackgroundDrawable(thumbnailList.get(i));
				break;
			case 1:
				image2.setVisibility(View.VISIBLE);
				image2.setBackgroundDrawable(thumbnailList.get(i));
				addImage.setVisibility(View.GONE);
				addImage1.setVisibility(View.VISIBLE);
				break;
			case 2:
				image3.setVisibility(View.VISIBLE);
				image3.setBackgroundDrawable(thumbnailList.get(i));
				break;
			case 3:
				image4.setVisibility(View.VISIBLE);
				image4.setBackgroundDrawable(thumbnailList.get(i));
				addImage.setVisibility(View.GONE);
				addImage1.setVisibility(View.GONE);
				break;
			}
		}
		MyBitmapUtils.downloadBitmap(cameraBitmap,
				"file" + thumbnailList.size() + ".jpg");
	}

	/**
	 * 删除手机中保存的图片文件
	 */
	private void deleteImage() {
		if (cameraBitmap != null){
			cameraBitmap.recycle();
		}
		if(newBitmap!=null){
			newBitmap.recycle();
		}
		for (int i = 0; i < bitmapList.size(); i++) {
			bitmapList.get(i).recycle();
			MyBitmapUtils
					.deleteTempFile(Environment.getExternalStorageDirectory()
							+ "/file" + (i + 1) + ".jpg");
		}
		MyBitmapUtils.deleteTempFile(Environment.getExternalStorageDirectory()
				+ "/largeshare.jpg");
	}

	/**
	 * 合成图片
	 */
	private void merger() {
		cameraBitmap = MyBitmapUtils.compoundBitmap(bitmapList);

		MyBitmapUtils.downloadBitmap(cameraBitmap, "largeshare.jpg");
	}

	private void showShare() {
		OnekeyShare oks = new OnekeyShare();
		// 关闭sso授权
		oks.disableSSOWhenAuthorize();
		oks.setSilent(false);
		// oks.setDialogMode();
		// 分享时Notification的图标和文字
		// oks.setNotification(R.drawable.ic_share,
		// getString(R.string.app_name));
		// title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
		oks.setTitle(getString(R.string.share));
		// titleUrl是标题的网络链接，仅在人人网和QQ空间使用
		oks.setTitleUrl("http://www.me-shirt.net/");
		// text是分享文本，所有平台都需要这个字段
		oks.setText(getString(R.string.my_share_text));
		// imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
		oks.setImagePath(Environment.getExternalStorageDirectory() + "/"
				+ "largeshare.jpg");// 确保SDcard下面存在此张图片
		// url仅在微信（包括好友和朋友圈）中使用
		oks.setUrl("http://www.me-shirt.net/");
		// comment是我对这条分享的评论，仅在人人网和QQ空间使用
		oks.setComment(userComment.getText().toString());
		// site是分享此内容的网站名称，仅在QQ空间使用
		oks.setSite(getString(R.string.app_name));
		// siteUrl是分享此内容的网站地址，仅在QQ空间使用
		oks.setSiteUrl("http://www.me-shirt.net/");
		// 启动分享GUI
		// oks.setLatitude(latitude);
		// oks.setLongitude(longitude);
		oks.show(this);
	}

	public String filePath = "";
	RelativeLayout layout_choose;
	RelativeLayout layout_photo;
	protected int mScreenWidth;
	protected int mScreenHeight;
	PopupWindow avatorPop;

	/**
	 * 显示图片选择
	 */
	private void showAvatarPop() {
		View view = LayoutInflater.from(this).inflate(R.layout.pop_showavator,
				null);
		layout_choose = (RelativeLayout) view.findViewById(R.id.layout_choose);
		layout_photo = (RelativeLayout) view.findViewById(R.id.layout_photo);
		layout_photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				layout_choose.setBackgroundColor(getResources().getColor(
						R.color.base_color_text_white));
				layout_photo.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.pop_bg_press));
				File dir = new File(Constants.MyAvatarDir);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				// 原图
				File file = new File(dir, new SimpleDateFormat("yyMMddHHmmss")
						.format(new Date()));
				filePath = file.getAbsolutePath();// 获取相片的保存路径
				Uri imageUri = Uri.fromFile(file);
				Uri externalContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				ContentResolver cr = getContentResolver();
				cr.query(externalContentUri, null, null, null, null);
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				startActivityForResult(intent,
						Constants.REQUESTCODE_UPLOADAVATAR_CAMERA);
				if (avatorPop != null) {
					avatorPop.dismiss();
				}
			}
		});
		layout_choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				layout_photo.setBackgroundColor(getResources().getColor(
						R.color.base_color_text_white));
				layout_choose.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.pop_bg_press));
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent,
						Constants.REQUESTCODE_UPLOADAVATAR_LOCATION);
			}
		});

		avatorPop = new PopupWindow(view, mScreenWidth, 600);
		avatorPop.setTouchInterceptor(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					avatorPop.dismiss();
					return true;
				}
				return false;
			}
		});

		avatorPop.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		avatorPop.setHeight(550);
		avatorPop.setTouchable(true);
		avatorPop.setFocusable(true);
		avatorPop.setOutsideTouchable(true);
		avatorPop.setBackgroundDrawable(new BitmapDrawable());
		// 动画效果 从底部弹起
		avatorPop.setAnimationStyle(R.style.Animations_GrowFromBottom);
		avatorPop.showAtLocation(layout_all, Gravity.BOTTOM, 0, 0);
	}

	Bitmap newBitmap;
	boolean isFromCamera = false;// 区分拍照旋转
	int degree = 0;

	/**
	 * 回调结果处理
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Bitmap thumbnailBitmap = null;
		switch (requestCode) {
		case Constants.REQUESTCODE_UPLOADAVATAR_CAMERA:// 拍照
			if (resultCode == RESULT_OK) {
				if (!Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					Toast.makeText(getApplicationContext(), R.string.no_sd,
							Toast.LENGTH_SHORT).show();
					return;
				}
				isFromCamera = true;
				File file = new File(filePath);
				degree = PhotoUtil.readPictureDegree(file.getAbsolutePath());
				cameraBitmap = MyBitmapUtils.getSmallBitmap(filePath);
				if (cameraBitmap != null) {
					thumbnailBitmap = ThumbnailUtils.extractThumbnail(
							cameraBitmap, 800, 800); // 获取缩略图
					thumbnailList.add(MyBitmapUtils
							.bitmapToDrawable(thumbnailBitmap));
					bitmapList.add(thumbnailBitmap);
				}
			}
			// 初始化文件路径
			filePath = "";
			break;
		case Constants.REQUESTCODE_UPLOADAVATAR_LOCATION:// 相册
			if (avatorPop != null) {
				avatorPop.dismiss();
			}
			Uri uri = null;
			if (data == null) {
				return;
			}
			if (resultCode == RESULT_OK) {
				if (!Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					Toast.makeText(getApplicationContext(), R.string.no_sd,
							Toast.LENGTH_SHORT).show();
					return;
				}
				isFromCamera = false;
				uri = data.getData();

				String[] proj = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(uri, proj, null, null, null);
				int column_index = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String path = cursor.getString(column_index);
				cameraBitmap = MyBitmapUtils.getSmallBitmap(path);

				if (cameraBitmap != null) {
					thumbnailBitmap = ThumbnailUtils.extractThumbnail(
							cameraBitmap, 800, 800); // 获取缩略图
					thumbnailList.add(MyBitmapUtils
							.bitmapToDrawable(thumbnailBitmap));
					bitmapList.add(thumbnailBitmap);
				} else {
					Toast.makeText(getApplicationContext(),
							R.string.getiamge_fail, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(getApplicationContext(),
						R.string.photograph_fail, Toast.LENGTH_SHORT).show();
			}

			break;

		}
		setImage();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(avatorPop!=null){
			avatorPop.dismiss();
		}
		deleteImage();
	}

	public void openProgressBar() {
		mProgressBar.setVisibility(View.VISIBLE);
	}

}
