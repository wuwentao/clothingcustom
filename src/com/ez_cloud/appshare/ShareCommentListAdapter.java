package com.ez_cloud.appshare;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ez_cloud.bean.ShareCommentInfo.CommentInfo;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.view.CircularImage;
import com.lidroid.xutils.BitmapUtils;

public class ShareCommentListAdapter extends BaseAdapter {
	private Context context;
	private List<CommentInfo> list;
	

	public ShareCommentListAdapter(Context context, List<CommentInfo> list) {
		super();
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		CommentInfo info=list.get(position);
		if(convertView==null){
			holder=new ViewHolder();
			convertView=View.inflate(context, R.layout.share_comment_list_item,null);
			holder.userHead=(CircularImage) convertView.findViewById(R.id.iv_share_comment_touxiang);
			holder.userName=(TextView) convertView.findViewById(R.id.tv_share_comment_nicheng);
			holder.shareTime=(TextView) convertView.findViewById(R.id.tv_share_comment_time);
			holder.shareComment=(TextView) convertView.findViewById(R.id.tv_share_comment_comment);
			convertView.setTag(holder);
		}
		holder=(ViewHolder) convertView.getTag();
//		MyBitmapUtils.display(context, holder.userHead,info.userPicPath);
//		MyBitmapUtils.downImage(info.userPicPath, holder.userHead, false);
		if("".equals(info.userPicPath)){
			holder.userHead.setImageResource(R.drawable.heardimage);
		}else{
			
			new BitmapUtils(context).display(holder.userHead, info.userPicPath);
		}
		holder.userName.setText(info.userName);
		holder.shareTime.setText(info.shareDate);
		holder.shareComment.setText(info.comment);
		return convertView;
	}
	public class ViewHolder{
		com.ez_cloud.view.CircularImage userHead;
		TextView userName;
		TextView shareTime;
		TextView shareComment;
	}

}
