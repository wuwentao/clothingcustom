package com.ez_cloud.appshare;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.bean.BuyerShareInfo.ShareResultInfo;
import com.ez_cloud.bean.ShareCommentInfo;
import com.ez_cloud.bean.ShareCommentInfo.CommentInfo;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.GsonUtils;
import com.ez_cloud.utils.Member;
import com.ez_cloud.utils.MyBitmapUtils;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.ez_cloud.utils.Utility;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ShareParticularActivity extends Activity implements
		OnClickListener {
	@ViewInject(R.id.iv_shareparticular_meshare)
	ImageView me_share;// 返回分享
	@ViewInject(R.id.tv_shareparticular_who)
	TextView whoShare;// 谁的分享
	@ViewInject(R.id.lv_shareparticular_commentlist)
	ListView commentList;// 评论列表
	@ViewInject(R.id.et_comment)
	EditText comment;// 评论内容
	@ViewInject(R.id.bt_publish)
	Button publish;// 发表评论
	@ViewInject(R.id.iv_iamge1)
	ImageView iv_image1;// 图片1
	@ViewInject(R.id.iv_iamge2)
	ImageView iv_image2;// 图片2
	@ViewInject(R.id.iv_iamge3)
	ImageView iv_image3;// 图片3
	@ViewInject(R.id.iv_iamge4)
	ImageView iv_image4;// 图片4
	private List<String> mImageUrlLists;

	private ShareResultInfo buyerShareInfo;// 当前分享的数据对象
	private List<CommentInfo> comments;// 评论数据集合

	private ShareCommentListAdapter shareCommentListAdapter;
	private Handler handler = new Handler() {

		@SuppressLint("HandlerLeak")
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					ShareCommentInfo commentInfo = (ShareCommentInfo) msg.obj;
					comments = commentInfo.result;
					shareCommentListAdapter = new ShareCommentListAdapter(
							getApplicationContext(), comments);
					commentList.setAdapter(shareCommentListAdapter);
					Utility.setListViewHeightBasedOnChildren(commentList);// 重绘listview的高度
					shareCommentListAdapter.notifyDataSetChanged();
				}
				break;
			case Constants.COMMENT_APPROVE:
				// 重新请求评论列表
				requestCommentList();
				comment.setText("");
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;

			}

		};
	};
	private int width;
	private List<ImageView> imageViewLists;
	private ImageView view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_share_particular);
		ViewUtils.inject(this);

		init();

		initData();

	}

	private void init() {
		imageViewLists = new ArrayList<ImageView>();
		imageViewLists.add(iv_image1);
		imageViewLists.add(iv_image2);
		imageViewLists.add(iv_image3);
		imageViewLists.add(iv_image4);
		Display defaultDisplay = getWindowManager().getDefaultDisplay();
		width = defaultDisplay.getWidth();
		comments = new ArrayList<CommentInfo>();
		buyerShareInfo = (ShareResultInfo) getIntent().getSerializableExtra(
				"ShareResultInfo");
		requestCommentList();
		me_share.setOnClickListener(this);
		publish.setOnClickListener(this);
	}

	private void requestCommentList() {
		RequestParams requestParams = new RequestParams();
		requestParams.addBodyParameter("shareId", buyerShareInfo.shareId);
		requestData(ServerInterface.USER_COMMENT_URL, requestParams,
				Constants.REQUEST_DATA);
	}

	/**
	 * 初始化数据
	 */
	private void initData() {
		whoShare.setTextSize(16);
		whoShare.setText(buyerShareInfo.userName);
		int picNum = Integer.parseInt(buyerShareInfo.picNum);
		mImageUrlLists = new ArrayList<String>(picNum);
		for (int i = 1; i <= picNum; i++) {
			StringBuffer buffer = new StringBuffer(buyerShareInfo.picPath);
			int len = buffer.length();
			StringBuffer uri = buffer.insert(len - 4, "_file" + i);
			mImageUrlLists.add(uri.toString());
		}
		processData(mImageUrlLists);
	}

	// 处理数据
	private void processData(List<String> mImageUrlLists) {
		for (int i = 0; i < mImageUrlLists.size(); i++) {
			view = imageViewLists.get(i);
			view.setVisibility(View.VISIBLE);
			BitmapUtils bitmapUtils = new BitmapUtils(
					ShareParticularActivity.this);
			bitmapUtils.display(view, mImageUrlLists.get(i),
					new BitmapLoadCallBack<ImageView>() {

						@Override
						public void onLoadCompleted(ImageView view,
								String arg1, Bitmap bitmap,
								BitmapDisplayConfig config, BitmapLoadFrom arg4) {
							// 根据屏幕的宽度重新绘制图片
							Bitmap stretchBitmap = MyBitmapUtils
									.stretchScreenWidth(bitmap, width);
							// 设置viewpager宽高
							LayoutParams layoutParams = view.getLayoutParams();
							layoutParams.width = stretchBitmap.getWidth();
							layoutParams.height = stretchBitmap.getHeight();
							view.setLayoutParams(layoutParams);

							view.setScaleType(ImageView.ScaleType.CENTER_CROP);
							view.setImageBitmap(stretchBitmap);
						}

						@Override
						public void onLoadFailed(ImageView arg0, String arg1,
								Drawable arg2) {
							// TODO Auto-generated method stub

						}
					});

		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.iv_shareparticular_meshare:// 返回ME分享
			// startActivity(new Intent(this,BuyerShareActivity.class));
			this.finish();
			break;
		case R.id.bt_publish:
			publishComment();
			break;
		}
	}

	/**
	 * 提交评论
	 */
	private void publishComment() {
		if (Member.isMember(ShareParticularActivity.this)) {
			String commentString = comment.getText().toString();
			if (TextUtils.isEmpty(commentString)) {
				Toast.makeText(getApplicationContext(), R.string.comment_ts,
						Toast.LENGTH_SHORT).show();
				return;
			}
			RequestParams requestParams = new RequestParams();
			requestParams.addBodyParameter("userId", SharedPreferencesUitls
					.getString(getApplicationContext(), "userId", ""));
			requestParams.addBodyParameter("shareId", buyerShareInfo.shareId);
			requestParams.addBodyParameter("remark", commentString);
			requestData(ServerInterface.SHARE_COMMITOTHERS_URL, requestParams,
					Constants.COMMENT_APPROVE);
		}
	}

	/**
	 * 向服务器请求数据
	 * 
	 * @param url
	 * @param requestParams
	 * @param x
	 *            用来区分请求数据和请求点赞的
	 */
	private void requestData(String url, RequestParams requestParams,
			final int x) {
		HttpUtils httpUtils = new HttpUtils();
		httpUtils.configTimeout(5000);
		httpUtils.configSoTimeout(5000);
		httpUtils.configRequestRetryCount(0);
		httpUtils.send(HttpMethod.POST, url, requestParams,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						Message msg = Message.obtain();
						msg.what = Constants.REQUEST_FAILURE;
						handler.sendMessage(msg);
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						Message msg = Message.obtain();
						switch (x) {
						case Constants.REQUEST_DATA:
							ShareCommentInfo json2Bean = GsonUtils.json2Bean(
									arg0.result, ShareCommentInfo.class);
							if ("00".equals(json2Bean.code)) {
								msg.what = Constants.REQUEST_SUCCEED;
								msg.obj = json2Bean;

							} else {
								msg.what = Constants.REQUEST_FAILURE;
							}
							break;
						case Constants.COMMENT_APPROVE:// 评论
							WebReturnData json2Bean2 = GsonUtils.json2Bean(
									arg0.result, WebReturnData.class);
							if (null != json2Bean2
									&& "00".equals(json2Bean2.code)) {
								msg.what = Constants.COMMENT_APPROVE;
							} else {
								msg.what = Constants.REQUEST_FAILURE;
							}
							break;
						}
						handler.sendMessage(msg);

					}
				});

	}
}
