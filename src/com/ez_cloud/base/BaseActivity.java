package com.ez_cloud.base;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.clothingcustom.R;
import com.google.android.gms.analytics.HitBuilders;

import android.app.Activity;
import android.view.MenuItem;

public abstract class BaseActivity extends Activity {

	public abstract void mFinish();
	
	  @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        int id = item.getItemId();
	        if (id == R.id.menu_settings) {
	            // Log setting open event with category="ui", action="open", and label="settings"
	            ClothingApplication.tracker().send(new HitBuilders.EventBuilder("ui", "open")
	                    .setLabel("settings")
	                    .build());
	            return true;
	        }
	        return super.onOptionsItemSelected(item);
	    }
}
