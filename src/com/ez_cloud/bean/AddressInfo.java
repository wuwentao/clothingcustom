package com.ez_cloud.bean;

import java.util.List;

public class AddressInfo {
	/** 从服务器获取的状态码 */
	public String code;
	/** 从服务器获取的状态信息 */
	public String message;
	/** 从服务器获取的首页数据 */
	public List<Result> result;

	@Override
	public String toString() {
		return "HomeInfo [code=" + code + ", message=" + message + ", result="
				+ result + "]";
	}

	public class Result {
		/** 省市 */
		public String areaCity;
		/** 街道 */
		public String comAddress;
		/** id */
		public String id;
		/** 区域 */
		public String areaCode;
		/** 邮编 */
		public String postCode;
		/** 收货人 */
		public String receiver;
		/** 注册日期 */
		public String regDate;
		/** 电话号码 */
		public String telNumber;
		/** 用户id */
		public String userId;
		/** 选用标志，是否是默认地址 */
		public String xybz;
		/** 标志 */
		public String yxbz;
		@Override
		public String toString() {
			return "Result [areaCity=" + areaCity + ", comAddress="
					+ comAddress + ", id=" + id + ", postCode=" + postCode
					+ ", receiver=" + receiver + ", regDate=" + regDate
					+ ", telNumber=" + telNumber + ", userId=" + userId
					+ ", xybz=" + xybz + ", yxbz=" + yxbz + "]";
		}
		
		

	}
}
