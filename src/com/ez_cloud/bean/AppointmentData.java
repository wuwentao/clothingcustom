package com.ez_cloud.bean;

import java.util.List;

public class AppointmentData {
public String code;
public String message;
public List<Result> result;
public class Result{
	public String arrangeDate;
	public String arrangeStatus;
	public String arrangeTime;
	public String id;
	public String serviceArea;
	public String submitDate;
	public String userId;
	public String userName;
	public String userSex;
	public String webUserAddress;
	public String webUserName;
	public String webUserTel;
	public String cityCode;
}
}
