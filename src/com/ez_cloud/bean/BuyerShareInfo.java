package com.ez_cloud.bean;

import java.io.Serializable;
import java.util.List;

public class BuyerShareInfo implements Serializable{
	public String code;
	public String message;
	public List<ShareResultInfo> result;
	
	
	public class ShareResultInfo  implements Serializable{
		public String approve;//点赞
		public String clothComment;//布料介绍
		public String clothFeature;//布料特点
		public String clothInLine;//
		public String clothIsStock;//
		public String clothName;//布料名称
		public String clothNo;//布料编号
		public String clothPicPath;//布料图片路径
		public String clothPrice;//布料价格
		public String clothRemark;//布料备注
		public String clothSpins;//线数
		public String clothStoreNum;//库存
		public String comment;//评论
		public String latitude;//维度
		public String longitude;//经度
		public String orderId;//订单ID
		public String picHeight;//图片高度
		public String picNum;//小图个数
		public String picPath;//分享大图路径
		public String picWidth;//图片宽度
		public String realName;//
		public String shareDate;//分享时间
		public String shareId;//分享ID
		public String shareNum;//分享数量
		public String style;//样式
		public String userId;//用户ID
		public String userName;//用户昵称
		public String userPicPath;//用户头像
		public String userScore;//
		public String voteNum;//
		public String yxbz;//
		public String smPicWidth;//
		public String smPicHeight ;//
		@Override
		public String toString() {
			return "ShareResultInfo [approve=" + approve + ", clothComment="
					+ clothComment + ", clothFeature=" + clothFeature
					+ ", clothInLine=" + clothInLine + ", clothIsStock="
					+ clothIsStock + ", clothName=" + clothName + ", clothNo="
					+ clothNo + ", clothPicPath=" + clothPicPath
					+ ", clothPrice=" + clothPrice + ", clothRemark="
					+ clothRemark + ", clothSpins=" + clothSpins
					+ ", clothStoreNum=" + clothStoreNum + ", comment="
					+ comment + ", latitude=" + latitude + ", longitude="
					+ longitude + ", orderId=" + orderId + ", picHeight="
					+ picHeight + ", picNum=" + picNum + ", picPath=" + picPath
					+ ", picWidth=" + picWidth + ", realName=" + realName
					+ ", shareDate=" + shareDate + ", shareId=" + shareId
					+ ", shareNum=" + shareNum + ", style=" + style
					+ ", userId=" + userId + ", userName=" + userName
					+ ", userPicPath=" + userPicPath + ", userScore="
					+ userScore + ", voteNum=" + voteNum + ", yxbz=" + yxbz
					+ "]";
		}
		
		
		
	}


}
