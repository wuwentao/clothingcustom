package com.ez_cloud.bean;

import java.io.Serializable;
import java.util.List;

public class ClothAllInfo {
	public String code;
	public String message;
	public List<ClothInfos> result;
	
	public class ClothInfos implements Serializable{
		public int approve;//
		public String clothName;//布料名称
		public String clothNo;//布料编号
		public String component;//详细信息
		public String feature;//成分
		public String inLine;//
		public String isStock;
		public String num;
		public String orderId;
		public String orderStatus;
		public String path;//图片路径
		public String payDate;
		public String payStatus;
		public String preNum;//库存
		public String price;//价格
		public String pubDate;
		public String remark;
		public String spins;
		public int storeNum;
		public String style;
		public String submitDate;
		public String updateDate;
		public String userId;
		public String yxbz;
		public boolean flag=false;
		

		
	}
}
