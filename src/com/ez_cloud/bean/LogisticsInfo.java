package com.ez_cloud.bean;

import java.util.List;

public class LogisticsInfo {
	/** 快递公司名称 */
	public String com;
	/** 快递条件 */
	public String condition;
	/** 物流信息详情 */
	public List<Data> data;
	/** 是否查询 */
	public String ischeck;
	/** 查询结果状态信息 */
	public String message;
	/** 快递运单号码 */
	public String nu;
	/** 查询结果状态码 */
	public String state;
	/** 服务器返回数据状态码 */
	public String status;

	@Override
	public String toString() {
		return "LogisticsInfo [com=" + com + ", condition=" + condition
				+ ", data=" + data + ", ischeck=" + ischeck + ", message="
				+ message + ", nu=" + nu + ", state=" + state + ", status="
				+ status + "]";
	}

	public class Data {
		/** 快递运单号码 */
		public String context;
		/** 该物流点发货时间 */
		public String ftime;
		/** 该物流点收货时间 */
		public String time;

		@Override
		public String toString() {
			return "Data [context=" + context + ", ftime=" + ftime + ", time="
					+ time + "]";
		}

	}
}
