package com.ez_cloud.bean;


public class MeasureHistoryInfo {
	public String code;
	public String message;
	public UserData userData;
	
	public class UserData{
		public String userId;
		public String bodyHeight;
		public String bodyWeight;
		public String frontPicPath;
		public String backPicPath;
		public String orderDate;
		
	}

}
