package com.ez_cloud.bean;

import java.io.Serializable;
import java.util.List;

public class OneCustomize {
	public String code;
	@Override
	public String toString() {
		return "ThemeInfo [code=" + code + ", message=" + message
				+ ", resultInfos=" + result + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	public String message;
	public List<Result> result;
	
	public class Result implements Serializable{
		public String backSnap;
		public String bxStyle;
		public String imagePath;
		public String kdStyle;
		public String lkStyle;
		public String mjStyle;
		public String modelId;
		public String modelType;
		public String price;
		public String remark;
		public String xkStyle;
		public String ycRemark;
		public String clothNo;
		
	}
}
