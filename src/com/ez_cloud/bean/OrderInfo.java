package com.ez_cloud.bean;

import java.util.List;

public class OrderInfo {
	/** 从服务器获取数据的状态码 */
	public String code;
	/** 从服务器获取数据的状态信息 */
	public String message;

	@Override
	public String toString() {
		return "LogisticsInfo [code=" + code + ", message=" + message
				+ ", result=" + result + "]";
	}

	/** 从服务器获取的数据 */
	public List<Result> result;

	public class Result {
		/** 从服务器获取数据的状态码 */
		public String areaCity;
		/** 从服务器获取数据的状态码 */
		public String armpitLength;
		/** 从服务器获取数据的状态码 */
		public String backSnap;
		/** 从服务器获取数据的状态码 */
		public String billComp;
		/** 从服务器获取数据的状态码 */
		public String billNo;
		/** 从服务器获取数据的状态码 */
		public String chestLength;
		/** 从服务器获取数据的状态码 */
		public String closing;
		/** 从服务器获取数据的状态码 */
		public String clothLength;
		/** 从服务器获取数据的状态码 */
		public String clothName;
		/** 从服务器获取数据的状态码 */
		public String clothNo;
		/** 从服务器获取数据的状态码 */
		public String collarLength;
		/** 从服务器获取数据的状态码 */
		public String collarModel;
		/** 从服务器获取数据的状态码 */
		public String comAddress;
		/** 从服务器获取数据的状态码 */
		public String component;
		/** 从服务器获取数据的状态码 */
		public String downDate;
		/** 从服务器获取数据的状态码 */
		public String embContent;
		/** 从服务器获取数据的状态码 */
		public String feature;
		/** 从服务器获取数据的状态码 */
		public String height;
		/** 从服务器获取数据的状态码 */
		public String isEmb;
		/** 从服务器获取数据的状态码 */
		public String measurModel;
		/** 从服务器获取数据的状态码 */
		public String orderId;
		/** 从服务器获取数据的状态码 */
		public String orderStatus;
		/** 从服务器获取数据的状态码 */
		public String path;
		/** 从服务器获取数据的状态码 */
		public String payDate;
		/** 从服务器获取数据的状态码 */
		public String payStatus;
		/** 从服务器获取数据的状态码 */
		public String pocket;
		/** 从服务器获取数据的状态码 */
		public String postCode;
		/** 从服务器获取数据的状态码 */
		public int preNum;
		/** 从服务器获取数据的状态码 */
		public String price;
		/** 从服务器获取数据的状态码 */
		public String receiver;
		/** 从服务器获取数据的状态码 */
		public String regDate;
		/** 从服务器获取数据的状态码 */
		public String sendDate;
		/** 从服务器获取数据的状态码 */
		public String shoulder;
		/** 从服务器获取数据的状态码 */
		public String sleeveLength;
		/** 从服务器获取数据的状态码 */
		public String sleeveLongShort;
		/** 从服务器获取数据的状态码 */
		public String sleeveStyle;
		/** 从服务器获取数据的状态码 */
		public String spins;
		/** 从服务器获取数据的状态码 */
		public String style;
		/** 从服务器获取数据的状态码 */
		public String submitDate;
		/** 从服务器获取数据的状态码 */
		public String telNumber;
		/** 从服务器获取数据的状态码 */
		public String tradeNo;
		/** 从服务器获取数据的状态码 */
		public String updateDate;
		/** 从服务器获取数据的状态码 */
		public String userId;
		/** 从服务器获取数据的状态码 */
		public String userName;
		/** 从服务器获取数据的状态码 */
		public String waistLength;
		/** 从服务器获取数据的状态码 */
		public String weight;
		/** 从服务器获取数据的状态码 */
		public String wristLength;
		/** 从服务器获取数据的状态码 */
		public String xybz;
		/** 从服务器获取数据的状态码 */
		public String yxbz;

		@Override
		public String toString() {
			return "Result [areaCity=" + areaCity + ", armpitLength="
					+ armpitLength + ", backSnap=" + backSnap + ", billComp="
					+ billComp + ", billNo=" + billNo + ", chestLength="
					+ chestLength + ", closing=" + closing + ", clothLength="
					+ clothLength + ", clothName=" + clothName + ", clothNo="
					+ clothNo + ", collarLength=" + collarLength
					+ ", collarModel=" + collarModel + ", comAddress="
					+ comAddress + ", component=" + component + ", downDate="
					+ downDate + ", embContent=" + embContent + ", feature="
					+ feature + ", height=" + height + ", isEmb=" + isEmb
					+ ", measurModel=" + measurModel + ", orderId=" + orderId
					+ ", orderStatus=" + orderStatus + ", path=" + path
					+ ", payDate=" + payDate + ", payStatus=" + payStatus
					+ ", pocket=" + pocket + ", postCode=" + postCode
					+ ", preNum=" + preNum + ", price=" + price + ", receiver="
					+ receiver + ", regDate=" + regDate + ", sendDate="
					+ sendDate + ", shoulder=" + shoulder + ", sleeveLength="
					+ sleeveLength + ", sleeveLongShort=" + sleeveLongShort
					+ ", sleeveStyle=" + sleeveStyle + ", spins=" + spins
					+ ", style=" + style + ", submitDate=" + submitDate
					+ ", telNumber=" + telNumber + ", tradeNo=" + tradeNo
					+ ", updateDate=" + updateDate + ", userId=" + userId
					+ ", userName=" + userName + ", waistLength=" + waistLength
					+ ", weight=" + weight + ", wristLength=" + wristLength
					+ ", xybz=" + xybz + ", yxbz=" + yxbz + "]";
		}

	}

}
