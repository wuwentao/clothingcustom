package com.ez_cloud.bean;

import java.util.List;


public class OrderStatusInfo {
	/** 从服务器获取数据的状态码 */
	public String code;
	/** 从服务器获取数据的状态信息 */
	public String message;
	
	@Override
	public String toString() {
		return "OrderStatusInfo [code=" + code + ", message=" + message
				+ ", result=" + result + "]";
	}

	/** 从服务器获取的数据 */
	public List<OrderResultInfo> result;
	
	public class OrderResultInfo{
		/** 点赞人数*/
		public int approve;
		/** 服装名称*/
		public String clothName;
		/** 服装编号*/
		public String clothNo;
		/** 布料*/
		public String component;
		/** 布料特点*/
		public String feature;
		/** 是否在售*/
		public String inLine;
		/** 是否有库存*/
		public String isStock;
		/** 数量*/
		public String num;
		/** 订单编号*/
		public String orderId;
		/** 订单状态*/
		public String orderStatus;
		/** 订单图片地址*/
		public String path;
		/** 付款日期*/
		public String payDate;
		/** 付款状态*/
		public String payStatus;
		/** 购买数量*/
		public String preNum;
		/** 单价*/
		public String price;
		/** 发布日期*/
		public String pubDate;
		/** 标志*/
		public String remark;
		/** 大小*/
		public String spins;
		/** 店铺存量*/
		public int storeNum;
		/** 款式*/
		public String style;
		/** 提交订单时间*/
		public String submitDate;
		/** 更新时间*/
		public String updateDate;
		/** 用户id*/
		public String userId;
		
		public String totalPrice;
		/** 标记*/
		public String yxbz;
		public String orderAreaCode;
		@Override
		public String toString() {
			return "Result [approve=" + approve + ", clothName=" + clothName
					+ ", clothNo=" + clothNo + ", component=" + component
					+ ", feature=" + feature + ", inLine=" + inLine
					+ ", isStock=" + isStock + ", num=" + num + ", orderId="
					+ orderId + ", orderStatus=" + orderStatus + ", path="
					+ path + ", payDate=" + payDate + ", payStatus="
					+ payStatus + ", preNum=" + preNum + ", price=" + price
					+ ", pubDate=" + pubDate + ", remark=" + remark
					+ ", spins=" + spins + ", storeNum=" + storeNum
					+ ", style=" + style + ", submitDate=" + submitDate
					+ ", updateDate=" + updateDate + ", userId=" + userId
					+ ", yxbz=" + yxbz + "]";
		}
		
		
	}
}
