package com.ez_cloud.bean;



public class PersonalCenterInfo {
	public String code;
	public String message;
	public WebUserInfo WebUser;
	
	@Override
	public String toString() {
		return "PersonalCenterInfo [code=" + code + ", message=" + message
				+ ", webUser=" + WebUser + "]";
	}

	public class WebUserInfo {
		public String address;
		public String email;
		public String password;
		public String realName;
		public String regDate;
		public String score;
		public String telNumber;
		public String userId;
		public String userName;
		public String userPicPath;
		public String yxbz;
		public String zcfs;
		@Override
		public String toString() {
			return "WebUser [address=" + address + ", email=" + email
					+ ", password=" + password + ", realName=" + realName
					+ ", regDate=" + regDate + ", score=" + score
					+ ", telNumber=" + telNumber + ", userId=" + userId
					+ ", userName=" + userName + ", userPicPath=" + userPicPath
					+ ", yxbz=" + yxbz + ", zcfs=" + zcfs + "]";
		}
	}
}
