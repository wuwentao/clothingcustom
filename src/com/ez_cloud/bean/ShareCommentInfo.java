package com.ez_cloud.bean;

import java.util.List;

public class ShareCommentInfo {

	public String code;
	public String message;
	public List<CommentInfo> result;
	
	
	public class CommentInfo{
		public String comment;//评论
		public String id;//评论
		public String realName;//真是姓名
		public String shareDate;//分享时间
		public String shareId;//分享id
		public String userId;//用户id
		public String userName;//用户名
		public String userPicPath;//头像
	}
}
