package com.ez_cloud.bean;

public class TodayDataInfo {
	public String code;
	public String message;
	public Result result;
	
	public class Result{
		@Override
		public String toString() {
			return "Result [backSnap=" + backSnap + ", bxStyle=" + bxStyle
					+ ", cdxStyle=" + cdxStyle + ", ckStyle=" + kzStyle
					+ ", dkStyle="  + ", xzStyle=" + xzStyle
					+ ", clothNo=" + clothNo + ", isShow=" + isShow
					+ ", kdStyle=" + kdStyle + ", lkStyle=" + lkStyle
					+ ", mjStyle=" + mjStyle + ", nkStyle=" + nkStyle
					+ ", nkLine=" + nkLine + ", rId=" + rId + ", xkStyle="
					+ xkStyle + ", yxbz=" + yxbz + ", xybz=" + xybz + "]";
		}
		public String backSnap = "";  //0后无打折 1背侧打折 2腰侧打折
		public String bxStyle = "";   //版型 1英伦标准版 2日韩短版 3发式修身版
		public String cdxStyle = "";  //长袖短袖 L/S
		public String kzStyle = "";  //长裤
		public String xzStyle = "";  //鞋子
		public String clothNo = "";
		public String isShow = "";    
		public String kdStyle = "";   //0无 1圆角口袋 2五角口袋
		public String lkStyle = "";   //1标准领 2钉扣领 3温莎领 4立领
		public String mjStyle = "";   //1传统门襟 2传统暗门襟 3法式门襟 4法式暗门襟
		public String nkStyle = "";   //纽扣1白色2黑色3棕色
		public String nkLine = "";    //纽扣线1反色2白色
		public String rId = "";
		public String xkStyle = "";   //袖口样式 1两用袖口2截角袖口3圆角袖口4法式翻袖
		
		
		public String yxbz = "";
		public String xybz = "";
	}
	

}
