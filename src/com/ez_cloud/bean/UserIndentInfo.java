package com.ez_cloud.bean;

import java.io.Serializable;
import java.util.List;

public class UserIndentInfo {
	public String code;
	public String message;
	public List<ResultInfo> result;

	public class ResultInfo implements Serializable{
		public String areaCity;
		public String armpitLength;
		public String backSnap;//背部打褶  （无：@“0”；后中：@“1”；后旁：@“2”）
		public String billComp;
		public String billNo;
		public String chestLength;//胸围 
		public String closing;//门襟  （法式：@“1” ；传统：@“2”）
		public String clothLength;//衣长
		public String clothName;//布料名字
		public String clothNo;//布料编号
		public String collarLength;//领围 
		public String collarModel;//领子：（   标准领：@“1”；纽扣领：@“2”；温莎领：@“3”；立领：@“4”）
		public String comAddress;
		public String component;//布料成份
		public String downDate;
		public String embContent;//绣字内容 
		public String feature;//布料特性
		public String height;//身高
		public String isEmb;//是否绣字
		public String measurModel;//测量方式  （标准版：@“1-1”；修身版：@“1-2”；标准尺寸：@“2”；拍照量身：@“3”；前次记录：@“4”）
		public String orderId;//订单编号
		public String orderStatus;//
		public String path;//布料图片
		public String payDate;
		public String payStatus;
		public String pocket;//口袋： （无：@“0”；圆角：@“1”；五角：@“2”）
		public String postCode;
		public String preNum;//购买数量:  
		public String price;//布料价格
		public String receiver;
		public String regDate;
		public String sendDate;
		public String shoulder;//肩宽 
		public String sleeveLength;//袖长
		public String sleeveLongShort;//长袖L、短袖S
		public String sleeveStyle;//袖口样式（直角袖口：@“1” ；截角：@“2”  ；圆角；@“3”  ；法式：@“4”  ） 
		public String spins;//布料织数
		public String style;//布料类型(//全部:“”// 素色：“S”//条纹:”T”//格纹:”G”)
		public String submitDate;//提交时间
		public String telNumber;
		public String tradeNo;
		public String updateDate;//更新时间
		public String userId;//用户id
		public String userName;//用户名字
		public String waistLength;//腰围 
		public String weight;//体重
		public String wristLength;//手腕 
		public String xybz;
		public String yxbz;


	}



}
