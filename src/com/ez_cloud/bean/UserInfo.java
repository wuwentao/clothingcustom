package com.ez_cloud.bean;


public class UserInfo {
	@Override
	public String toString() {
		return "UserInfo [code=" + code + ", message=" + message
				+ ", WebUser=" + WebUser + "]";
	}

	public String code;
	public String message;
	public WebUser WebUser;

	public class WebUser {
		public String address;
		public String email;
		public String password;
		public String realName;
		public String regDate;
		public String score;
		public String telNumber;
		public String userId;
		public String userName;
		public String yxbz;
		public String zcfs;

		@Override
		public String toString() {
			return "WebUser [address=" + address + ", email=" + email
					+ ", password=" + password + ", realName=" + realName
					+ ", regDate=" + regDate + ", score=" + score
					+ ", telNumber=" + telNumber + ", userId=" + userId
					+ ", userName=" + userName + ", yxbz=" + yxbz + ", zcfs="
					+ zcfs + "]";
		}

	}

}
