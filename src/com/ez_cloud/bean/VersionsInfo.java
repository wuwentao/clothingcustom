package com.ez_cloud.bean;


public class VersionsInfo {
	public String code;
	public String message;
	public Request VersionMessage;
	public class Request{
		public String appName;
		public String appSys;
		public String appUrl;
		public String id;
		public String updateBz;
		public String versionCode;
		public String xybz;
		public String yxbz;
		@Override
		public String toString() {
			return "Request [appName=" + appName + ", appSys=" + appSys
					+ ", appUrl=" + appUrl + ", id=" + id + ", updateBz="
					+ updateBz + ", versionCode=" + versionCode + ", xybz="
					+ xybz + ", yxbz=" + yxbz + "]";
		}

	}
	@Override
	public String toString() {
		return "VersionsInfo [code=" + code + ", message=" + message
				+ ", request=" + VersionMessage + "]";
	}
	






}
