package com.ez_cloud.bean;

public class WebReturnData {

	public String code;
	public String message;

	@Override
	public String toString() {
		return "RegisterInfo [code=" + code + ", message=" + message + "]";
	}

}
