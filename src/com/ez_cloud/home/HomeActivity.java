package com.ez_cloud.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.theme.ThemeActivity;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.ez_cloud.welcome.MainActivity;
import com.ez_cloud.welcome.WesternStyle;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class HomeActivity extends Activity implements OnClickListener {
	RadioButton share;
	@ViewInject(R.id.tv_home_shirt) TextView home_shirt;
	@ViewInject(R.id.tv_home_suit) TextView home_suit;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home);
		ViewUtils.inject(this);

		home_shirt.setOnClickListener(this);
		home_suit.setOnClickListener(this);
 
	}



	@Override
	public void onClick(View v) {

		switch(v.getId()){
		case R.id.tv_home_shirt:
			SharedPreferencesUitls.saveBoolean(getApplicationContext(), "is_detail", false);
			SharedPreferencesUitls.saveString(getApplicationContext(), "ActivityTag", "home");
			startActivity(new Intent(HomeActivity.this,ThemeActivity.class));
			overridePendingTransition( R.anim.push_up_in,R.anim.push_up_out);
			break;
		case R.id.tv_home_suit:
			startActivity(new Intent(HomeActivity.this,WesternStyle.class));
			overridePendingTransition( R.anim.push_up_in,R.anim.push_up_out);
			break;

		}
	}
	boolean isFirstClick=true;
	//监听物理返回键，按两次退出程序，由于项目中存在unity3D引擎退出程序会闪退，所以在这里做个假退出操作，切换到手机主菜单
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getRepeatCount() == 0) {
			if(isFirstClick){
				Toast.makeText(getApplicationContext(), R.string.quit_ts, 0).show();
				isFirstClick=false;
			}else{
				startActivity(new Intent(HomeActivity.this,MainActivity.class));
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);           
				startActivity(intent);
//				System.exit(0);
				isFirstClick=true;
			}

			return true;
		}
		return super.onKeyDown(keyCode, event);
	}



}
