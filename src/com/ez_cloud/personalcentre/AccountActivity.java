package com.ez_cloud.personalcentre;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ez_cloud.account.ModifyPswActivity;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class AccountActivity extends BaseActivity {

	/** 返回按钮 */
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	/** 显示用户名的TextView */
	@ViewInject(R.id.username_tv)
	TextView username_tv;
	/** 显示积分的TextView */
	// @ViewInject(R.id.score_tv)TextView score_tv;
	/** 修改密码的LinearLayout布局 */
	@ViewInject(R.id.password_ll)
	LinearLayout password_ll;
	/** 显示手机号码的TextView */
	@ViewInject(R.id.tel_tv)
	TextView tel_tv;
	/** 收货地址的LinearLayout布局 */
	@ViewInject(R.id.address_ll)
	LinearLayout address_ll;
	private String userName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_account);
		ViewUtils.inject(this);// 注解组件
		init();
	}

	// 初始化组件
	private void init() {
		userName = SharedPreferencesUitls.getString(getApplicationContext(),
				"userName", "易动网云");
		username_tv.setText(userName);
		tel_tv.setText(userName);

		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(AccountActivity.this,
						PersonalCenterActivity.class));
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);
			}
		});
	}

	/**
	 * 跳转到修改密码页面，对密码进行重置
	 * 
	 * @param v
	 */
	@OnClick(R.id.password_ll)
	public void toFindbackActivity(View v) {
		Intent intent = new Intent(AccountActivity.this,
				ModifyPswActivity.class);
		intent.putExtra("userName", userName);
		startActivity(intent);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
	}

	/**
	 * 跳转到收货地址页面，查看收货地址并对收货地址进行一些操作
	 * 
	 * @param v
	 */
	@OnClick(R.id.address_ll)
	public void toAddressActivity(View v) {
		SharedPreferencesUitls.saveBoolean(this, "spikPreview", false);
		startActivity(new Intent(AccountActivity.this, AddressActivity.class));
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
