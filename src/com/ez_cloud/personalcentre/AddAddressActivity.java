package com.ez_cloud.personalcentre;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class AddAddressActivity extends BaseActivity {

	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	/** EditText 姓名输入框 */
	@ViewInject(R.id.name)
	EditText name;
	/** EditText 电话输入框 */
	@ViewInject(R.id.tel)
	EditText tel;
	/** LinearLayout省市区选择 */
	/*
	 * @ViewInject(R.id.area_ll) LinearLayout area_ll;
	 */
	/** EditText 省市输入框 */
	@ViewInject(R.id.city)
	EditText city;
	// TextView city;
	/** EditText 详细地址输入框 */
	@ViewInject(R.id.address)
	EditText address;
	/** EditText 邮编输入框 */
	@ViewInject(R.id.code)
	EditText code;
	/** CheckBox 默认地址选择框 */
	@ViewInject(R.id.cb)
	CheckBox cb;
	/** 保存按钮 */
	@ViewInject(R.id.btn_save)
	Button save;

	/**
	 * 省的WheelView控件
	 */
	// private WheelView mProvince;
	/**
	 * 市的WheelView控件
	 */
	// private WheelView mCity;
	/**
	 * 区的WheelView控件
	 */
	// private WheelView mArea;
	/**
	 * 把全国的省市区的信息以json的格式保存，解析完成后赋值为null
	 */
	// private JSONObject mJsonObj;

	/**
	 * 所有省
	 */
	// private String[] mProvinceDatas;
	/**
	 * key - 省 value - 市s
	 */
	// private Map<String, String[]> mCitisDatasMap = new HashMap<String,
	// String[]>();
	/**
	 * key - 市 values - 区s
	 */
	// private Map<String, String[]> mAreaDatasMap = new HashMap<String,
	// String[]>();

	/**
	 * 当前省的名称
	 */
	// private String mCurrentProviceName;
	// /**
	// * 当前市的名称
	// */
	// private String mCurrentCityName;
	// /**
	// * 当前区的名称
	// */
	// private String mCurrentAreaName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_address);
		ViewUtils.inject(this);// 注解组件
		activityTag = getIntent().getStringExtra("activityTag");
		if ("add".equals(activityTag)) {

		} else if ("amend".equals(activityTag)) {
			tbl_title.setTitle(getResources().getString(R.string.alter_address));
			init();
		}
		areaCode = "2";
	}

	// 初始化
	private void init() {
		Intent i = this.getIntent();
		Bundle b = i.getExtras();
		if (null != b) {
			String receiver = b.getString("receiver");
			String telNumber = b.getString("telNumber");
			String areaCity = b.getString("areaCity");
			String comAddress = b.getString("comAddress");
			String postCode = b.getString("postCode");
			String xybz = b.getString("xybz");
			// String areaCode = b.getString("areaCode");
			id = b.getString("id");

			name.setText(receiver);
			tel.setText(telNumber);
			city.setText(areaCity);
			address.setText(comAddress);
			code.setText(postCode);
			if (xybz.equals("Y")) {
				cb.setChecked(true);
			} else {
				cb.setChecked(false);
			}

		}

		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(AddAddressActivity.this,
						AddressActivity.class));
				AddAddressActivity.this.finish();
			}
		});
	}

	/**
	 * 保存按钮
	 * 
	 * @param v
	 */
	@OnClick(R.id.btn_save)
	public void save(View v) {
		String nameS = name.getText().toString();
		String telS = tel.getText().toString();
		String cityS = city.getText().toString();
		String addressS = address.getText().toString();
		String codeS = code.getText().toString();
		String xybz = "";

		if (TextUtils.isEmpty(nameS)) {
			Toast.makeText(getApplicationContext(), R.string.add_address_name,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (TextUtils.isEmpty(telS)) {
			Toast.makeText(getApplicationContext(), R.string.add_address_phone,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (TextUtils.isEmpty(cityS)) {
			Toast.makeText(getApplicationContext(), R.string.add_address_city,
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (TextUtils.isEmpty(addressS)) {
			Toast.makeText(getApplicationContext(),
					R.string.add_address_detailed, Toast.LENGTH_SHORT).show();
			return;
		}

		if (TextUtils.isEmpty(codeS)) {
			Toast.makeText(getApplicationContext(),
					R.string.add_address_postcode, Toast.LENGTH_SHORT).show();
			return;
		}

		if (cb.isChecked()) {
			xybz = "Y";
		} else {
			xybz = "N";
		}

		RequestParams params = new RequestParams();

		params.addBodyParameter("receiver", nameS);
		params.addBodyParameter("telNumber", telS);
		params.addBodyParameter("areaCity", cityS);
		params.addBodyParameter("comAddress", addressS);
		params.addBodyParameter("postCode", codeS);
		params.addBodyParameter("xybz", xybz);
		params.addBodyParameter("areaCode", areaCode);

		if ("amend".equals(activityTag)) {
			params.addBodyParameter("id", id);
			commitData(ServerInterface.modifyAddressUrl, params);
		} else {
			params.addBodyParameter("userId", SharedPreferencesUitls.getString(
					getApplicationContext(), "userId", ""));
			commitData(ServerInterface.addAddressUrl, params);
		}
		params = null;
	}

	private void commitData(String url, RequestParams params) {
		ClothingHttpUtils.httpSend(HttpMethod.POST, url, params,
				WebReturnData.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);

	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		@SuppressLint("HandlerLeak")
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					WebReturnData addAddressInfo = (WebReturnData) msg.obj;
					if ("00".equals(addAddressInfo.code)) {
						Intent i = new Intent(AddAddressActivity.this,
								AddressActivity.class);
						startActivity(i);
						Toast.makeText(AddAddressActivity.this,
								R.string.add_address_succeed,
								Toast.LENGTH_SHORT).show();
						finish();
					} else {
						Toast.makeText(AddAddressActivity.this,
								R.string.add_address_fail, Toast.LENGTH_SHORT)
								.show();
					}

				}
				break;

			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;

			}
		}
	};
	private String areaCode;
	private String activityTag;
	private String id;

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
