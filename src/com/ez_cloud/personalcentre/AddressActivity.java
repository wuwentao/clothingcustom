package com.ez_cloud.personalcentre;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ez_cloud.adapter.AddressAdapter;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.AddressInfo;
import com.ez_cloud.bean.AddressInfo.Result;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.traditioncustom.PreviewActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

@SuppressLint("ShowToast")
public class AddressActivity extends BaseActivity implements
		OnItemLongClickListener, OnItemClickListener {

	// 删除收货地址及编辑收货地址，这里只用跳转到一个新的编辑页面来修改或者删除

	/** 返回按钮 */
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	/** 添加收货地址 */
	@ViewInject(R.id.add)
	Button add;
	/** 显示收货地址的ListView */
	@ViewInject(R.id.address_listView)
	ListView address_listView;
	/** 没有收货地址时显示的收货地址界面 */
	@ViewInject(R.id.address_null)
	RelativeLayout address_null;
	@ViewInject(R.id.rl_address_progressBar)
	RelativeLayout progressBar;

	AlertDialog.Builder builder;
	ProgressBar mProgressBar;
	private AddressAdapter addressAdapter;
	private List<Result> lists = null;

	private Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			progressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null!=msg) {
					WebReturnData webData = (WebReturnData) msg.obj;
					if ("00".equals(webData.code)) {
						requestData();
					} 
				}
				break;
			case Constants.REQUEST_TAG_A:
				if (null != msg) {
					AddressInfo addIfo = (AddressInfo) msg.obj;
					if ("00".equals(addIfo.code)) {
						lists = addIfo.result;
						if (lists != null && lists.size() > 0) {
							addressAdapter = new AddressAdapter(
									AddressActivity.this, lists);
							addressAdapter.notifyDataSetChanged();
							address_listView.setAdapter(addressAdapter);
							addressAdapter.notifyDataSetChanged();
						} else {
							address_null.setVisibility(View.VISIBLE);
						}
					} else {
						Toast.makeText(getApplicationContext(),
								R.string.server_error, 0).show();
					}
					break;
				}
			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						0).show();
				break;
			}
		}
	};
	public AddressInfo addressInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_address);
		ViewUtils.inject(this);// 注解组件

		init();

	}

	private void init() {
		lists = new ArrayList<Result>();
		requestData();

		address_listView.setOnItemLongClickListener(this);
		address_listView.setOnItemClickListener(this);
		
		tbl_title.getBack().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (SharedPreferencesUitls.getBoolean(AddressActivity.this, "spikPreview", false)) {
					startActivity(new Intent(AddressActivity.this,
							PreviewActivity.class));
				} else {
					startActivity(new Intent(AddressActivity.this,
							AccountActivity.class));
				}
				AddressActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			}
		});
	}

	private void requestData() {
		String userId = SharedPreferencesUitls.getString(
				getApplicationContext(), "userId", "");
		RequestParams params = new RequestParams();
		params.addBodyParameter("userId", userId);

		ClothingHttpUtils.httpSend(HttpMethod.POST, ServerInterface.addressUrl,
				params, AddressInfo.class, handler, Constants.REQUEST_TAG_A,
				Constants.REQUEST_FAILURE);

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		addressAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}


	/**
	 * 添加收货地址按钮
	 * 
	 * @param v
	 */
	@OnClick(R.id.add)
	public void toAddAddressActivity(View v) {
		Intent intent = new Intent(getBaseContext(), AddAddressActivity.class);
		intent.putExtra("activityTag", "add");
		startActivity(intent);
		this.finish();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		requestData();
	}

	/**
	 * 请求服务器删除定单
	 * 
	 * @param url
	 */
	private void deleteAddress(String url) {
		// TODO Auto-generated method stub
		ClothingHttpUtils.httpSend(HttpMethod.GET, url, null,
				WebReturnData.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
			final int arg2, long arg3) {
		new AlertDialog.Builder(AddressActivity.this)
				.setTitle(R.string.prompt)
				.setMessage(R.string.deleteorder)
				.setPositiveButton(R.string.deleteorder_yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								progressBar.setVisibility(View.GONE);
								deleteAddress(ServerInterface.deleteAddressUrl
										+ lists.get(arg2).id);
								if (dialog != null
										&& ((Dialog) dialog).isShowing()) {
									dialog.dismiss();
								}
							}
						})
				.setNegativeButton(R.string.soft_update_cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// 取消按钮事件
							}
						}).show();

		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lists.size(); i++) {
			lists.get(i).xybz = "N";
		}
		lists.get(arg2).xybz = "Y";
		SharedPreferencesUitls.saveString(getApplicationContext(), "takeName",
				lists.get(arg2).receiver);
		SharedPreferencesUitls.saveString(getApplicationContext(),
				"takeNumber", lists.get(arg2).telNumber);
		SharedPreferencesUitls.saveString(getApplicationContext(), "address",
				lists.get(arg2).areaCity + "  " + lists.get(arg2).comAddress);
		SharedPreferencesUitls.saveString(getApplicationContext(), "areaCode",
				lists.get(arg2).areaCode);

		addressAdapter.notifyDataSetChanged();
		// CheckBox findViewById = (CheckBox)
		// arg1.findViewById(R.id.loading_checkbox);
		// findViewById.setChecked(true);
	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub
		
	}

}
