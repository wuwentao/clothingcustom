package com.ez_cloud.personalcentre;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.clothingcustom.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class FeedbackActivity extends BaseActivity {

	/** 返回按钮 */
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	/** 意见输入框 */
	@ViewInject(R.id.et)
	EditText et;
	/** TextView显示还可以输入的字数 */
	@ViewInject(R.id.tv)
	TextView tv;
	/** 发送按钮 */
	@ViewInject(R.id.btn_send)
	Button btn_send;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		ViewUtils.inject(this);// 注解组件
		// 给意见输入框添加一个字数监控器
		et.addTextChangedListener(tw);

		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(FeedbackActivity.this,
						PersonalCenterActivity.class));
				FeedbackActivity.this.finish();
			}
		});
	}

	TextWatcher tw = new TextWatcher() {

		private CharSequence temp;
		private int etStart, etEnd;

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			temp = s;
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			etStart = et.getSelectionStart();
			etEnd = et.getSelectionEnd();
			int a = 500 - s.length();
			tv.setText(a + "");

			if (a == 0) {
				Toast.makeText(FeedbackActivity.this, R.string.exceed,
						Toast.LENGTH_LONG).show();
				s.delete(etStart - 1, etEnd);
				int tempSelection = etStart;
				et.setText(s);
				et.setSelection(tempSelection);
			}

		}

	};

	/**
	 * 发送按钮
	 * 
	 * @param v
	 */
	@OnClick(R.id.btn_send)
	public void send(View v) {
		Toast.makeText(FeedbackActivity.this, R.string.feedback_hint,
				Toast.LENGTH_SHORT).show();
		Intent i = new Intent(FeedbackActivity.this,
				PersonalCenterActivity.class);
		startActivity(i);
		finish();
	}

	// /**
	// * 联系我们
	// *
	// * @param v
	// */
	// @OnClick(R.id.contact_tv)
	// public void contact(View v) {
	// new AlertDialog.Builder(FeedbackActivity.this).setTitle(R.string.contact)
	// .setMessage("400-009-4885")
	// .setNegativeButton(R.string.soft_update_cancel, new
	// DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// dialog.dismiss();
	// }
	// })
	// .setPositiveButton(R.string.call, new DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// Intent phoneIntent = new Intent(
	// "android.intent.action.CALL", Uri.parse("tel:"
	// + "400-009-4885"));
	// // 启动
	// startActivity(phoneIntent);
	// dialog.dismiss();
	// }
	// }).show();
	// }

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
