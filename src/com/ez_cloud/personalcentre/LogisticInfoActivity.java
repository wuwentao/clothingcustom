package com.ez_cloud.personalcentre;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.adapter.LogisticsInfoAdapter;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.LogisticsInfo;
import com.ez_cloud.bean.LogisticsInfo.Data;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class LogisticInfoActivity extends BaseActivity {
	/** 返回按钮 */
	@ViewInject(R.id.tbl_log_title)
	com.ez_cloud.view.TitleBarLayout tbl_log_title;
	/** 快递100注明TextView */
	// @ViewInject(R.id.kuaidi100_tv) TextView kuaidi100_tv;
	/** 快递公司显示TextView */
	@ViewInject(R.id.billComp_tv)
	TextView billComp_tv;
	/** 快递运单号显示TextView */
	@ViewInject(R.id.billNo_tv)
	TextView billNo_tv;
	@ViewInject(R.id.tv_logistics_commodity)
	TextView commodity;
	/** 物流详细信息显示ListView */
	@ViewInject(R.id.logistics_info_listView)
	ListView logistics_info_listView;
	@ViewInject(R.id.rl_logistics_progressBar)
	RelativeLayout progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logistics_info);
		ViewUtils.inject(this);// 注解组件
		init();
	}

	// private List<Data> dataLists;
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {

		@SuppressLint("HandlerLeak")
		public void handleMessage(Message msg) {
			progressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					LogisticsInfo info = (LogisticsInfo) msg.obj;
					if ("ok".equalsIgnoreCase(info.message)) {
						List<Data> dataLists = info.data;
						if (null != dataLists && dataLists.size() > 0) {
							LogisticsInfoAdapter logisticsInfoAdapter = new LogisticsInfoAdapter(
									LogisticInfoActivity.this, dataLists);
							logistics_info_listView
									.setAdapter(logisticsInfoAdapter);
						} else {
							// Toast.makeText(LogisticInfoActivity.this,
							// R.string.logistics_no,
							// 0).show();
						}
					}
				}

				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(LogisticInfoActivity.this,
						R.string.server_error, Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	private void init() {
		// kuaidi100_tv.setText("@数据由“快递100”提供    (http://www.kuaidi100.com)");
		Intent i = this.getIntent();
		Bundle b = i.getExtras();
		String billComp = b.getString("billComp");
		String billNo = b.getString("billNo");
		// String orderId = b.getString("orderId");//
		// 到这里了，下一步是显示物流公司到TextView当中，这里需要写两个TextView组件在xml文件当中，用来显示物流公司和运单号。同时需要把物流信息显示在ListView当中
		if ("shunfeng".equalsIgnoreCase(billComp)) {
			billComp_tv.setText(R.string.shunfeng);
		}

		billNo_tv.setText(billNo);
		commodity.setText(b.getString("clothName"));
		requestData(billComp, billNo);
		
		tbl_log_title.getBack().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(LogisticInfoActivity.this,
						LogisticsActivity.class));
				LogisticInfoActivity.this.finish();
			}
		});
	}

	/**
	 * 请求服务器
	 * 
	 * @param billComp
	 * @param billNo
	 */
	private void requestData(String billComp, String billNo) {

		RequestParams params = new RequestParams();
		params.addBodyParameter("type", billComp);
		params.addBodyParameter("postid", billNo);

		ClothingHttpUtils.httpSend(HttpMethod.POST,
				ServerInterface.logisticsInfoUrl, params, LogisticsInfo.class,
				handler, Constants.REQUEST_SUCCEED, Constants.REQUEST_FAILURE);

	}


	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
