package com.ez_cloud.personalcentre;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.ez_cloud.adapter.LogisticsAdapter;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.OrderInfo;
import com.ez_cloud.bean.OrderInfo.Result;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class LogisticsActivity extends BaseActivity {

	/* 这里需要用到美观的ListView来展示物流信息，需要参考GitHub当中的一些ListView,所以先不写这个，先写个人中心中的其他模块 */

	/** 返回按钮 */
	@ViewInject(R.id.tbl_log_title)
	com.ez_cloud.view.TitleBarLayout tbl_log_title;
	/** 显示已经发货的订单 */
	@ViewInject(R.id.shipped_order_listView)
	ListView shipped_order_listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logistics);
		ViewUtils.inject(this);// 注解组件

		requestData();
		
		tbl_log_title.getBack().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				LogisticsActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			}
		});
	}

	private void requestData() {
		String userId = SharedPreferencesUitls.getString(
				getApplicationContext(), "userId", "");
		RequestParams params = new RequestParams();
		params.addBodyParameter("userId", userId);

		ClothingHttpUtils.httpSend(HttpMethod.POST,
				ServerInterface.shippedOrderUrl, params, OrderInfo.class,
				handler, Constants.REQUEST_SUCCEED, Constants.REQUEST_FAILURE);

	}

	private List<Result> lists;
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {

		@SuppressLint("HandlerLeak")
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					OrderInfo info = (OrderInfo) msg.obj;
					if ("00".equals(info.code)) {
						lists = info.result;
						if (lists!=null&&lists.size() > 0) {
							shipped_order_listView
									.setAdapter(new LogisticsAdapter(
											LogisticsActivity.this, lists));
							shipped_order_listView
									.setOnItemClickListener(new OnItemClickListener() {
										@Override
										public void onItemClick(
												AdapterView<?> parent,
												View view, int position, long id) {
											String billComp = lists
													.get(position).billComp;
											String billNo = lists.get(position).billNo;
											String orderId = lists
													.get(position).orderId;
											Intent i = new Intent(
													LogisticsActivity.this,
													LogisticInfoActivity.class);
											Bundle b = new Bundle();
											b.putString("orderId", orderId);
											b.putString("billComp", billComp);
											b.putString("billNo", billNo);
											b.putString(
													"clothName",
													lists.get(position).clothName);
											i.putExtras(b);
											startActivity(i);
										}

									});
						}else{
							Toast.makeText(LogisticsActivity.this, R.string.no_order_data,
									Toast.LENGTH_SHORT).show();
						}

					}
				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(LogisticsActivity.this, R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;
			}

		}
	};


	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
