package com.ez_cloud.personalcentre;

import java.util.ArrayList;
import java.util.List;

import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.base.EmptyActivity;
import com.ez_cloud.clothingcustom.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

@SuppressWarnings("deprecation")
public class MyOrderActivity extends BaseActivity {
	/** 返回按钮 */
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	List<View> list;
	Context context;
	LocalActivityManager manager = null;

	/* @ViewInject(R.id.tabhost) */TabHost tabHost;

	@ViewInject(R.id.viewpager)
	private ViewPager pager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_order);
		ViewUtils.inject(this);// 注解组件
		context = MyOrderActivity.this;
		pager = (ViewPager) findViewById(R.id.viewpager);
		// 定放一个放view的list，用于存放viewPager用到的view
		list = new ArrayList<View>();

		manager = new LocalActivityManager(this, true);
		manager.dispatchCreate(savedInstanceState);

		Intent i1 = new Intent(context, UnpaidActivity.class);
		list.add(getView("A", i1));
		Intent i2 = new Intent(context, PaidActivity.class);
		list.add(getView("B", i2));
		Intent i3 = new Intent(context, RefundActivity.class);
		list.add(getView("C", i3));
		Intent i4 = new Intent(context, RefundedActivity.class);
		list.add(getView("D", i4));
		tabHost = (TabHost) findViewById(R.id.tabhost);
		tabHost.setup();
		tabHost.setup(manager);

		/*
		 * //设置默认tab字体 TextView tv=(TextView)
		 * tabHost.getTabWidget().getChildAt(0).findViewById(R.id.tv_title);
		 * tv.setTextColor(Color.BLACK);
		 */
		// 这儿主要是自定义一下tabhost中的tab的样式
		RelativeLayout tabIndicator1 = (RelativeLayout) LayoutInflater.from(
				this).inflate(R.layout.tabwidget, null);
		TextView tvTab1 = (TextView) tabIndicator1.findViewById(R.id.tv_title);
		tvTab1.setText(R.string.unpaid);
		tvTab1.setTextColor(Color.BLACK);

		RelativeLayout tabIndicator2 = (RelativeLayout) LayoutInflater.from(
				this).inflate(R.layout.tabwidget, null);
		TextView tvTab2 = (TextView) tabIndicator2.findViewById(R.id.tv_title);
		tvTab2.setText(R.string.paid);

		RelativeLayout tabIndicator3 = (RelativeLayout) LayoutInflater.from(
				this).inflate(R.layout.tabwidget, null);
		TextView tvTab3 = (TextView) tabIndicator3.findViewById(R.id.tv_title);
		tvTab3.setText(R.string.refund);

		RelativeLayout tabIndicator4 = (RelativeLayout) LayoutInflater.from(
				this).inflate(R.layout.tabwidget, null);
		TextView tvTab4 = (TextView) tabIndicator4.findViewById(R.id.tv_title);
		tvTab4.setText(R.string.refunded);

		Intent intent = new Intent(context, EmptyActivity.class);
		// 注意这儿Intent中的activity不能是自身
		// 比如“A”对应的是T1Activity，后面intent就new的T3Activity的。
		tabHost.addTab(tabHost.newTabSpec("A").setIndicator(tabIndicator1)
				.setContent(intent));
		tabHost.addTab(tabHost.newTabSpec("B").setIndicator(tabIndicator2)
				.setContent(intent));
		tabHost.addTab(tabHost.newTabSpec("C").setIndicator(tabIndicator3)
				.setContent(intent));
		tabHost.addTab(tabHost.newTabSpec("D").setIndicator(tabIndicator4)
				.setContent(intent));

		pager.setAdapter(new MyPageAdapter(list));
		pager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				// 当viewPager发生改变时，同时改变tabhost上面的currentTab
				tabHost.setCurrentTab(position);
				for (int i = 0; i < 4; i++) {
					TextView tv = (TextView) tabHost.getTabWidget()
							.getChildAt(i).findViewById(R.id.tv_title);
					tv.setTextColor(Color.GRAY);
				}
				TextView tv = (TextView) tabHost.getTabWidget()
						.getChildAt(position).findViewById(R.id.tv_title);
				tv.setTextColor(Color.BLACK);
			}

			@Override
			public void onPageScrolled(int position, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int position) {

			}
		});

		// 点击tabhost中的tab时，要切换下面的viewPager
		tabHost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {

				if ("A".equals(tabId)) {
					pager.setCurrentItem(0);
				}
				if ("B".equals(tabId)) {
					pager.setCurrentItem(1);
				}
				if ("C".equals(tabId)) {
					pager.setCurrentItem(2);
				}
				if ("D".equals(tabId)) {
					pager.setCurrentItem(3);
				}
			}
		});
		
		tbl_title.getBack().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(MyOrderActivity.this,
						PersonalCenterActivity.class));
				MyOrderActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			}
		});

	}

	private View getView(String id, Intent intent) {
		return manager.startActivity(id, intent).getDecorView();
	}

	private class MyPageAdapter extends PagerAdapter {

		private List<View> list;

		private MyPageAdapter(List<View> list) {
			this.list = list;
		}

		@Override
		public void destroyItem(View view, int position, Object arg2) {
			ViewPager pViewPager = ((ViewPager) view);
			pViewPager.removeView(list.get(position));
		}

		@Override
		public void finishUpdate(View arg0) {
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object instantiateItem(View view, int position) {
			ViewPager pViewPager = ((ViewPager) view);
			pViewPager.addView(list.get(position));
			return list.get(position);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
		}

	}


	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
