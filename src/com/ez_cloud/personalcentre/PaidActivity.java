package com.ez_cloud.personalcentre;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.adapter.PaidAdapter;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.OrderStatusInfo;
import com.ez_cloud.bean.OrderStatusInfo.OrderResultInfo;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.theme.ThemeActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PaidActivity extends BaseActivity {

	/** 显示已经付款的订单 */
	@ViewInject(R.id.paid_order_listView)
	ListView paid_order_listView;
	@ViewInject(R.id.loadProgressBar)
	ProgressBar mProgressBar;
	@ViewInject(R.id.rl_paid_layout)
	RelativeLayout mRelativeLayout;
	@ViewInject(R.id.iv_order_go)
	ImageView orderGo;
	private LinearLayout menu;
	private PaidAdapter paidAdapter;
	private Vibrator vibrator;
	private ImageView delImageView, countImageView;
	private TextView countTextView;
	private Animation showAnim, hideAnim;
	private View dragItemView;
	private boolean isChoiceMode, isAll;
	private int count;
	private boolean[] isDel;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_paid);
		ViewUtils.inject(this);// 注解组件
		request();
		// 震动器
		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		menu = (LinearLayout) findViewById(R.id.ll_paid_menu);
		menu.setVisibility(View.GONE);
		delImageView = (ImageView) menu.findViewById(R.id.delimg);
		countImageView = (ImageView) menu.findViewById(R.id.countimg);
		countTextView = (TextView) menu.findViewById(R.id.count);
		// 菜单进入动画
		showAnim = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
				1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
		showAnim.setDuration(300);
		// 菜单退出动画
		hideAnim = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
				0.0f, Animation.RELATIVE_TO_SELF, 1.0f);
		hideAnim.setDuration(200);
		paid_order_listView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						// 获取当前所长按的View
						dragItemView = view;
						dragItemView
								.setBackgroundResource(R.drawable.list_yellow_single);
						vibrator.vibrate(20);
						// 防止多次长按时menu多次出现
						if (!isChoiceMode) {
							menu.setVisibility(View.VISIBLE);
							menu.startAnimation(showAnim);
						}
						isChoiceMode = true;
						if (!isDel[position]) {
							count++;
							countTextView.setText(String.valueOf(count));
						}
						// 标记已选择
						paidAdapter.isSelected.put(position, true);
						isDel[position] = true;

						return true;
					}
				});

		paid_order_listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if (isDel[arg2]) {
					arg1.setBackgroundResource(R.drawable.select);
					isDel[arg2] = false;
					paidAdapter.isSelected.put(arg2, false);
					count--;
					countTextView.setText(String.valueOf(count));
				} else if (isChoiceMode) {
					arg1.setBackgroundResource(R.drawable.list_yellow_single);
					isDel[arg2] = true;
					paidAdapter.isSelected.put(arg2, true);
					count++;
					countTextView.setText(String.valueOf(count));
				}
				for (int i = 0; i < isDel.length; i++) {
					if (isDel[i]) {
						isChoiceMode = true;
						return;
					} else {
						isChoiceMode = false;
					}
				}
				if (!isChoiceMode && menu.getVisibility() == View.VISIBLE) {
					menu.startAnimation(hideAnim);
					menu.setVisibility(View.GONE);
				}

			}
		});
		countImageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isAll) {
					for (int i = 0; i < isDel.length; i++) {
						isDel[i] = false;
						paidAdapter.isSelected.put(i, false);
						if (paid_order_listView.getChildAt(i) != null) {
							paid_order_listView.getChildAt(i)
									.setBackgroundResource(R.drawable.select);
						}
						countTextView.setText("0");
						count = 0;
						isAll = false;
						isChoiceMode = false;
						menu.startAnimation(hideAnim);
						menu.setVisibility(View.GONE);
					}
				} else {
					for (int i = 0; i < isDel.length; i++) {
						isDel[i] = true;
						paidAdapter.isSelected.put(i, true);
						if (paid_order_listView.getChildAt(i) != null) {
							paid_order_listView.getChildAt(i)
									.setBackgroundResource(
											R.drawable.list_yellow_single);
						}
						countTextView.setText("ALL");
						count = isDel.length;
						isAll = true;
					}
				}

			}
		});
		delImageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				delItem();
			}
		});
		orderGo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PaidActivity.this.startActivity(new Intent(PaidActivity.this,
						ThemeActivity.class));
				PaidActivity.this.finish();
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
			}
		});
	}

	public void request() {
		String userId = SharedPreferencesUitls.getString(
				getApplicationContext(), "userId", "");

		RequestParams params = new RequestParams();
		params.addBodyParameter("userId", userId);
		params.addBodyParameter("payStatus", "1");

		ClothingHttpUtils.httpSend(HttpMethod.POST,
				ServerInterface.orderStatusUrl, params, OrderStatusInfo.class,
				handler, Constants.REQUEST_SUCCEED, Constants.REQUEST_FAILURE);

	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {

		@SuppressLint("HandlerLeak")
		public void handleMessage(Message msg) {
			mProgressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					OrderStatusInfo orderInfo = (OrderStatusInfo) msg.obj;
					if ("00".equals(orderInfo.code)) {
						List<OrderResultInfo> lists = orderInfo.result;
						if (lists.size() > 0) {
							mRelativeLayout.setVisibility(View.GONE);
							paidAdapter = new PaidAdapter(PaidActivity.this,
									lists, mProgressBar);
							init();
						} else {
							mRelativeLayout.setVisibility(View.VISIBLE);
						}
					}
				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.order_ts,
						Toast.LENGTH_SHORT).show();
				break;
			case Constants.CONFIGTIMEOUT:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;

			}
		}
	};

	private void delItem() {
		Collection<OrderResultInfo> delWhich = new ArrayList<OrderResultInfo>();
		for (int i = 0; i < isDel.length; i++) {
			if (isDel[i]) {
				delWhich.add(paidAdapter.mData.get(i));
				paidAdapter.isSelected.put(i, false);
			}
		}
		paidAdapter.delItem(delWhich);
		init();
		menu.startAnimation(hideAnim);
		menu.setVisibility(View.GONE);
	}

	private void init() {
		isChoiceMode = false;
		isAll = false;
		count = 0;
		paid_order_listView.setAdapter(paidAdapter);
		isDel = new boolean[paidAdapter.getCount()];
		for (int i = 0; i < isDel.length; i++) {
			isDel[i] = false;
		}
	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}
}
