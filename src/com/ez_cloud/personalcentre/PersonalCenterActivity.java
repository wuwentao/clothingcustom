package com.ez_cloud.personalcentre;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.bmob.im.BmobUserManager;

import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.PersonalCenterInfo;
import com.ez_cloud.bean.PersonalCenterInfo.WebUserInfo;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.home.HomeActivity;
import com.ez_cloud.theme.ThemeActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.PhotoUtil;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class PersonalCenterActivity extends BaseActivity {
	/** 个人帐户信息模块的布局 */
	@ViewInject(R.id.account)
	LinearLayout account;
	/** 积分详情模块的布局 */
	// @ViewInject(R.id.scoreinfo)
	LinearLayout scoreinfo;
	/** 个人帐户信息当中显示用户名的TextView */
	/*
	 * @ViewInject(R.id.username_tv) TextView username;
	 *//** 个人帐户信息当中显示用户积分的TextView */
	/*
	 * @ViewInject(R.id.score_tv) TextView score;
	 */
	/** 我的订单模块的布局 */
	@ViewInject(R.id.myorder)
	LinearLayout myorder;
	/** 查看物流模块的布局 */
	@ViewInject(R.id.logistics)
	LinearLayout logistics;
	/** 常见问题模块的布局 */
	@ViewInject(R.id.vision_info_ll)
	LinearLayout commonproblem;
	/** 意见反馈模块的布局 */
	@ViewInject(R.id.feedback_ll)
	LinearLayout feedback;

	/** 返回按钮 */
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	@ViewInject(R.id.tv_version)
	TextView version;
	/** 退出按钮 */
	@ViewInject(R.id.drop_out)
	Button drop_out;

	@ViewInject(R.id.img_logo)
	ImageView img_logo;

	// 设置头像

	public String filePath = "";
	RelativeLayout layout_choose;
	RelativeLayout layout_photo;
	protected int mScreenWidth;
	protected int mScreenHeight;
	RelativeLayout layout_all;

	PopupWindow avatorPop;

	private WebUserInfo webUser;
	@ViewInject(R.id.username_tv)
	TextView username_tv;

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Constants.REQUEST_TAG_A:
				if (null != msg) {
					PersonalCenterInfo info = (PersonalCenterInfo) msg.obj;
					Log.d("PersonalCenterInfo", info.toString());
					if ("00".equals(info.code)) {
						webUser = info.WebUser;

						BitmapUtils bUtils = new BitmapUtils(
								PersonalCenterActivity.this
										.getApplicationContext());
						bUtils.clearMemoryCache();
						bUtils.clearDiskCache();
						bUtils.clearCache();
						bUtils.closeCache();
						bUtils.configDefaultLoadFailedImage(R.drawable.default_head);// 加载失败默认图片
						bUtils.display(img_logo, webUser.userPicPath);
						// bUtils.cancel();

						username_tv.setText(webUser.userName);
					}
				}
				break;
			case Constants.REQUEST_TAG_B:
				if (null != msg) {
					WebReturnData mWebReturnData = (WebReturnData) msg.obj;
					if ("00".equals(mWebReturnData.code)) {
						Toast.makeText(getApplicationContext(),
								R.string.head_image_succeed, Toast.LENGTH_SHORT)
								.show();
					} else {
						Toast.makeText(
								PersonalCenterActivity.this
										.getApplicationContext(),
								R.string.head_fail, Toast.LENGTH_SHORT).show();
					}
				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personal_center);
		ViewUtils.inject(this);// 注解组件

		initData();
	}

	private void initData() {
		userManager = BmobUserManager.getInstance(this);
		layout_all = (RelativeLayout) findViewById(R.id.layout_all);
		setMouseClick(100, 100);
		if (getVersion() != null) {
			version.setText("v " + getVersion());
		} else {
			version.setText("v 1.0");
		}

		String userId = SharedPreferencesUitls.getString(
				getApplicationContext(), "userId", null);
		RequestParams params = new RequestParams();
		params.addBodyParameter("userId", userId);

		ClothingHttpUtils.httpSend(HttpMethod.POST,
				ServerInterface.getWebUserMessageUrl, params,
				PersonalCenterInfo.class, handler, Constants.REQUEST_TAG_A,
				Constants.REQUEST_FAILURE);

		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(PersonalCenterActivity.this,
						ThemeActivity.class));
				PersonalCenterActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);
			}
		});
	}

	public void setMouseClick(int x, int y) {
		MotionEvent evenDownt = MotionEvent.obtain(System.currentTimeMillis(),
				System.currentTimeMillis() + 100, MotionEvent.ACTION_DOWN, x,
				y, 0);
		dispatchTouchEvent(evenDownt);
		MotionEvent eventUp = MotionEvent.obtain(System.currentTimeMillis(),
				System.currentTimeMillis() + 100, MotionEvent.ACTION_UP, x, y,
				0);
		dispatchTouchEvent(eventUp);
		evenDownt.recycle();
		eventUp.recycle();
	}

	/**
	 * 上传头像
	 * 
	 * @param v
	 */
	@OnClick(R.id.img_logo)
	public void changeLogo(View v) {
		showAvatarPop();
	}

	/**
	 * 显示图片选择
	 */
	private void showAvatarPop() {
		View view = LayoutInflater.from(this).inflate(R.layout.pop_head, null);
		layout_choose = (RelativeLayout) view.findViewById(R.id.layout_choose);
		layout_photo = (RelativeLayout) view.findViewById(R.id.layout_photo);
		layout_photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// ShowLog("点击拍照");
				layout_choose.setBackgroundColor(getResources().getColor(
						R.color.base_color_text_white));
				layout_photo.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.pop_bg_press));
				File dir = new File(Constants.MyAvatarDir);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				// 原图
				File file = new File(dir, new SimpleDateFormat("yyMMddHHmmss")
						.format(new Date()));
				filePath = file.getAbsolutePath();// 获取相片的保存路径
				Uri imageUri = Uri.fromFile(file);

				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				startActivityForResult(intent,
						Constants.REQUESTCODE_UPLOADAVATAR_CAMERA);
			}
		});
		layout_choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// ShowLog("点击相册");
				layout_photo.setBackgroundColor(getResources().getColor(
						R.color.base_color_text_white));
				layout_choose.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.pop_bg_press));
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent,
						Constants.REQUESTCODE_UPLOADAVATAR_LOCATION);
			}
		});

		avatorPop = new PopupWindow(view, mScreenWidth, 600);
		avatorPop.setTouchInterceptor(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					avatorPop.dismiss();
					return true;
				}
				return false;
			}
		});

		avatorPop.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		avatorPop.setHeight(550);
		avatorPop.setTouchable(true);
		avatorPop.setFocusable(true);
		avatorPop.setOutsideTouchable(true);
		avatorPop.setBackgroundDrawable(new BitmapDrawable());
		// 动画效果 从底部弹起
		avatorPop.setAnimationStyle(R.style.Animations_GrowFromBottom);
		avatorPop.showAtLocation(layout_all, Gravity.BOTTOM, 0, 0);
	}

	Bitmap newBitmap;
	boolean isFromCamera = false;// 区分拍照旋转
	int degree = 0;

	/**
	 * 回调结果处理
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case Constants.REQUESTCODE_UPLOADAVATAR_CAMERA:// 拍照修改头像
			if (resultCode == RESULT_OK) {
				if (!Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					// ShowToast("SD不可用");
					Toast.makeText(getApplicationContext(), R.string.no_sd,
							Toast.LENGTH_SHORT).show();
					return;
				}
				isFromCamera = true;
				File file = new File(filePath);
				degree = PhotoUtil.readPictureDegree(file.getAbsolutePath());
				Log.i("life", "拍照后的角度：" + degree);
				startImageAction(Uri.fromFile(file), 200, 200,
						Constants.REQUESTCODE_UPLOADAVATAR_CROP, true);
			}
			break;
		case Constants.REQUESTCODE_UPLOADAVATAR_LOCATION:// 本地修改头像
			if (avatorPop != null) {
				avatorPop.dismiss();
			}
			Uri uri = null;
			if (data == null) {
				return;
			}
			if (resultCode == RESULT_OK) {
				if (!Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					// ShowToast("SD不可用");
					Toast.makeText(getApplicationContext(), R.string.no_sd,
							Toast.LENGTH_SHORT).show();
					return;
				}
				isFromCamera = false;
				uri = data.getData();
				startImageAction(uri, 200, 200,
						Constants.REQUESTCODE_UPLOADAVATAR_CROP, true);
			} else {
				// ShowToast("照片获取失败");
				Toast.makeText(getApplicationContext(),
						R.string.photograph_fail, Toast.LENGTH_SHORT).show();
			}

			break;
		case Constants.REQUESTCODE_UPLOADAVATAR_CROP:// 裁剪头像返回
			// TODO sent to crop
			if (avatorPop != null) {
				avatorPop.dismiss();
			}
			if (data == null) {
				// Toast.makeText(this, "取消选择", Toast.LENGTH_SHORT).show();
				return;
			} else {
				saveCropAvator(data);
			}
			// 初始化文件路径
			filePath = "";
			// 上传头像
			upload();
			break;
		default:
			break;

		}
	}

	/**
	 * 裁剪图片
	 * 
	 * @param uri
	 * @param outputX
	 * @param outputY
	 * @param requestCode
	 * @param isCrop
	 */
	private void startImageAction(Uri uri, int outputX, int outputY,
			int requestCode, boolean isCrop) {
		Intent intent = null;
		if (isCrop) {
			intent = new Intent("com.android.camera.action.CROP");
		} else {
			intent = new Intent(Intent.ACTION_GET_CONTENT, null);
		}
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", outputX);
		intent.putExtra("outputY", outputY);
		intent.putExtra("scale", true);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		intent.putExtra("return-data", true);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
		intent.putExtra("noFaceDetection", true); // no face detection
		startActivityForResult(intent, requestCode);
	}

	String path;

	/**
	 * 保存裁剪的头像
	 * 
	 * @param data
	 */
	private void saveCropAvator(Intent data) {
		Bundle extras = data.getExtras();
		if (extras != null) {
			Bitmap bitmap = extras.getParcelable("data");
			Log.i("life", "avatar - bitmap = " + bitmap);
			if (bitmap != null) {
				bitmap = PhotoUtil.toRoundCorner(bitmap, 10);
				if (isFromCamera && degree != 0) {
					bitmap = PhotoUtil.rotaingImageView(degree, bitmap);
				}
				img_logo.setImageBitmap(bitmap);
				// 保存图片
				// String filename = new SimpleDateFormat("yyMMddHHmmss")
				// .format(new Date());
				path = Constants.MyAvatarDir + "head.png";
				PhotoUtil.saveBitmap(Constants.MyAvatarDir, "head.png", bitmap,
						true);
				// 上传头像
				if (bitmap != null && bitmap.isRecycled()) {
					bitmap.recycle();
				}
			}
		}
	}

	/**
	 * 传送图片文件到服务器的方法
	 * 
	 * @param path
	 *            图片路径
	 */
	private void upload() {
		String userId = SharedPreferencesUitls.getString(
				getApplicationContext(), "userId", null);
		RequestParams params = new RequestParams();
		File file = new File(Constants.MyAvatarDir + "/head.png");
		File file1 = new File(Constants.MyAvatarDir + "/head.png");
		if (!file.exists()) {
			params.addBodyParameter("picData", file);
		}
		params.addBodyParameter("picData", file1);
		params.addBodyParameter("userId", userId);

		ClothingHttpUtils.httpSend(HttpMethod.POST,
				ServerInterface.uploadUserImageUrl, params,
				WebReturnData.class, handler, Constants.REQUEST_TAG_B,
				Constants.REQUEST_FAILURE);
	}

	/**
	 * 跳转到个人帐户页面
	 * 
	 * @param v
	 */
	@OnClick(R.id.account)
	public void toAccount(View v) {
		Intent i = new Intent(PersonalCenterActivity.this,
				AccountActivity.class);
		startActivity(i);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		// finish();
	}

	/**
	 * 跳转到我的订单页面
	 * 
	 * @param v
	 */
	@OnClick(R.id.myorder)
	public void toMyOrderActivity(View v) {
		Intent i = new Intent(PersonalCenterActivity.this,
				MyOrderActivity.class);
		startActivity(i);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		// finish();
	}

	/**
	 * 跳转到查看物流页面
	 * 
	 * @param v
	 */
	@OnClick(R.id.logistics)
	public void toLogisticsActivity(View v) {
		Intent i = new Intent(PersonalCenterActivity.this,
				LogisticsActivity.class);
		startActivity(i);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		// finish();
	}

	/**
	 * 跳转到版本信息
	 * 
	 * @param v
	 */
	@OnClick(R.id.vision_info_ll)
	public void toCommonProblemActivity(View v) {
		// 点击这里可以直接进行版本更新操作，不作页面跳转。如果发布有新版本的话直接更新，如果没有发布新版本的话，就提示版本已经处于最新状态

		Intent i = new Intent(PersonalCenterActivity.this,
				VersionsUpdateActivity.class);
		startActivity(i);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		// finish();

		// Toast.makeText(PersonalCenterActivity.this,R.string.versions_hint,
		// 0).show();
	}

	/**
	 * 跳转到意见反馈页面
	 * 
	 * @param v
	 */
	@OnClick(R.id.feedback_ll)
	public void toFeedbackActivity(View v) {
		Intent i = new Intent(PersonalCenterActivity.this,
				FeedbackActivity.class);
		startActivity(i);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		// finish();
	}

	/**
	 * 触发退出登陆事件
	 * 
	 * @param v
	 */
	@OnClick(R.id.drop_out)
	public void dropOut(View v) {
		SharedPreferencesUitls.saveString(getApplicationContext(), "userId",
				null);
		Intent i = new Intent(PersonalCenterActivity.this, HomeActivity.class);
		startActivity(i);
		overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
		finish();
	}

	Toast mToast;
	private BmobUserManager userManager;

	/**
	 * 获取版本号
	 * 
	 * @return 当前应用的版本号
	 */
	public String getVersion() {
		try {
			PackageManager manager = this.getPackageManager();
			PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
			String version = info.versionName;
			return version;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(avatorPop!=null){
			avatorPop.dismiss();
		}
	}
}
