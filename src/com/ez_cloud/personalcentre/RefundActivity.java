package com.ez_cloud.personalcentre;

import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ez_cloud.adapter.RefundedAdapter;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.OrderStatusInfo;
import com.ez_cloud.bean.OrderStatusInfo.OrderResultInfo;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class RefundActivity extends BaseActivity {

	/** 显示已经付款的订单 */
	@ViewInject(R.id.refund_order_listView)
	ListView refund_order_listView;
	@ViewInject(R.id.rl_refund_progressBar)
	RelativeLayout mProgressBar;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_refund);

		ViewUtils.inject(this);

		requestData();
	}

	private void requestData() {
		String userId = SharedPreferencesUitls.getString(
				getApplicationContext(), "userId", "");

		RequestParams params = new RequestParams();
		params.addBodyParameter("userId", userId);
		params.addBodyParameter("payStatus", "2");

		ClothingHttpUtils.httpSend(HttpMethod.POST,
				ServerInterface.orderStatusUrl, params, OrderStatusInfo.class,
				handler, Constants.REQUEST_SUCCEED, Constants.REQUEST_FAILURE);
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {

		@SuppressLint("HandlerLeak")
		public void handleMessage(Message msg) {
			mProgressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					OrderStatusInfo info = (OrderStatusInfo) msg.obj;
					if ("00".equals(info.code)) {
						List<OrderResultInfo> lists = info.result;
						if (lists.size() > 0) {
							refund_order_listView
									.setAdapter(new RefundedAdapter(
											RefundActivity.this, lists));
						} else {

						}
					}

				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.order_ts,
						Toast.LENGTH_SHORT).show();
				break;
			case Constants.CONFIGTIMEOUT:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;

			}
		}
	};

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}

}
