package com.ez_cloud.personalcentre;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.Utils;

public class VersionsUpdateActivity extends Activity  {
	private TextView versionText;
	private com.ez_cloud.view.TitleBarLayout tbl_title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_versionsupdata);
		versionText = (TextView) findViewById(R.id.tv_version_text);
		tbl_title = (com.ez_cloud.view.TitleBarLayout) findViewById(R.id.tbl_title);
		
		init();
	}

	private void init() {
		String version = Utils.getVersion(getApplicationContext());
		versionText.setText("ME-TAILOR V"+version);
		
		tbl_title.getBack().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				VersionsUpdateActivity.this.finish();
				overridePendingTransition( R.anim.push_left_in,R.anim.push_left_out);
			}
		});

	}

}
