package com.ez_cloud.theme;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.appshare.BuyerShareActivity;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.OneCustomize;
import com.ez_cloud.bean.OneCustomize.Result;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.home.HomeActivity;
import com.ez_cloud.personalcentre.PersonalCenterActivity;
import com.ez_cloud.traditioncustom.CustomizationActivity;
import com.ez_cloud.traditioncustom.PhotoMeasureActivity;
import com.ez_cloud.unlty.UnityPlayerNativeActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.CommonUtil;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.Member;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.ez_cloud.utils.Utils;
import com.ez_cloud.view.RollViewPager;
import com.ez_cloud.view.RollViewPager.MyOnPagerClickListener;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ThemeActivity extends BaseActivity implements OnClickListener {
	private List<View> dotLists;
	@ViewInject(R.id.dots_ll)
	private LinearLayout dots_ll;
	private List<String> mImageUrlLists;
	private List<String> mTitleLists;
	@ViewInject(R.id.top_news_title)
	private TextView top_news_title;
	@ViewInject(R.id.top_news_viewpager)
	private LinearLayout top_news_viewpager;
	@ViewInject(R.id.frame_viewpager_father)
	private FrameLayout frame_viewpager_father;
	@ViewInject(R.id.tbl_title)
	private com.ez_cloud.view.TitleBarLayout  tbl_title;
	@ViewInject(R.id.iv_tradition)
	private ImageView tradition;
	@ViewInject(R.id.iv_share)
	private ImageView share;
	@ViewInject(R.id.iv_show)
	private ImageView show;
	@ViewInject(R.id.rl_theme_ts)
	private RelativeLayout fristHint;
	@ViewInject(R.id.theme_progressbar)
	private ProgressBar mProgressBar;

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			mProgressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg.obj) {
					OneCustomize mThemeInfos = (OneCustomize) msg.obj;
					if ("00".equals(mThemeInfos.code)) {
						// 处理获取的数据
						processData(mThemeInfos);
					} else {
						Toast.makeText(getApplicationContext(),
								R.string.data_failure_hint, Toast.LENGTH_SHORT)
								.show();
					}
				}
				break;

			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;
			}
		};
	};
	private int width;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_theme);
		ViewUtils.inject(this);
		
		Display defaultDisplay = getWindowManager().getDefaultDisplay();
		width = defaultDisplay.getWidth();
		// 初始化控件点击监听
		initClick();
		// 请求一键定制轮播数据
		getThemeData(ServerInterface.THEME_SHOW_IMAGE+Utils.getCountry(this));
		// 判断用户是不是第一次进入程序，是否显示引导
		isFristLogin();

		fristHint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fristHint.setVisibility(View.GONE);
			}
		});
	}

	/**
	 * 初始化控件点击监听
	 */
	private void initClick() {
		//返回上一级
		tbl_title.getBack().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ThemeActivity.this, HomeActivity.class));
				ThemeActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			}
		});
		//跳转到个人中心
		tbl_title.getNext().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (Member.isMember(ThemeActivity.this)) {
					startActivity(new Intent(ThemeActivity.this, PersonalCenterActivity.class));
				}
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
			}
		});
		share.setOnClickListener(this);
		show.setOnClickListener(this);
		tradition.setOnClickListener(this);
	}

	/**
	 * 判断是否是第一次进入程序
	 */
	private void isFristLogin() {
		if ("".equals(SharedPreferencesUitls.getString(ThemeActivity.this,
				"themefirst", ""))) {
			SharedPreferencesUitls.saveString(ThemeActivity.this, "themefirst",
					"NO");
			fristHint.setVisibility(View.VISIBLE);
		} else {
			fristHint.setVisibility(View.GONE);
		}
	}

	/**
	 * 处理服务器返回的数据
	 * 
	 * @param mThemeInfos
	 */
	private void processData(final OneCustomize mThemeInfos) {
		// TODO Auto-generated method stub
		if (mThemeInfos.result != null && mThemeInfos.result.size() > 0) {
			// 初始化界面上的点的数字
			initDot(mThemeInfos.result.size());
			// 初始化图片的url地址的集合
			mImageUrlLists = new ArrayList<String>();
			// 初始化描述信息的集合
			mImageUrlLists = new ArrayList<String>();
			// 初始化描述信息的集合
			mTitleLists = new ArrayList<String>();
			for (Result info : mThemeInfos.result) {
				mImageUrlLists.add(info.imagePath);
				mTitleLists.add(info.remark);
			}
			RollViewPager mViewPager = new RollViewPager(ThemeActivity.this,
					dotLists, frame_viewpager_father, width,
					new MyOnPagerClickListener() {

						@Override
						public void OnPagerClickListener(int position) {
							SharedPreferencesUitls.saveBoolean(
									ThemeActivity.this, "isonecustomize", true);
							Result result = mThemeInfos.result.get(position);
							((ClothingApplication) getApplication())
									.setOneCustomizeInfo(result);
							Intent intent = new Intent(ThemeActivity.this,
									PhotoMeasureActivity.class);
							startActivity(intent);
							overridePendingTransition(R.anim.push_right_in,
									R.anim.push_right_out);
						}
					});

			// 设置图片的url地址
			mViewPager.setImageUrlLists(mImageUrlLists);
			// 传如textview和描述信息
			mViewPager.setTitleLists(top_news_title, mTitleLists);
			// 让轮播图滚动起来
			mViewPager.startRoll();
			// 把viewPager设置到布局里面
			top_news_viewpager.removeAllViews();
			top_news_viewpager.addView(mViewPager);
		}
	}

	// 初始化点
	private void initDot(int size) {
		dotLists = new ArrayList<View>();
		dots_ll.removeAllViews();
		for (int i = 0; i < size; i++) {
			// 设置点的宽高
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					CommonUtil.dip2px(getApplicationContext(), 20),
					CommonUtil.dip2px(getApplicationContext(), 5));
			// 设置点的间距
			params.setMargins(5, 0, 5, 0);
			// 初始化点的对象
			View m = new View(getApplicationContext());
			// 把点的宽高设置到view里面
			m.setLayoutParams(params);
			if (i == 0) {
				// 默认情况下，首先会调用第一个点。就必须展示选中的点
				m.setBackgroundResource(R.drawable.dot_focus);
			} else {
				// 其他的点都是默认的。
				m.setBackgroundResource(R.drawable.dot_normal);
			}
			// 把所有的点装载进集合
			dotLists.add(m);
			// 现在的点进入到了布局里面
			dots_ll.addView(m);
		}

	}

	// 从服务器获取数据
	private void getThemeData(String url) {
		ClothingHttpUtils.httpSend(HttpMethod.GET, url, null,
				OneCustomize.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);

	}

	protected void mToast() {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), R.string.server_error,
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.iv_tradition:
			SharedPreferencesUitls.saveBoolean(ThemeActivity.this,
					"isonecustomize", false);
			startActivity(new Intent(this, CustomizationActivity.class));
			overridePendingTransition(R.anim.push_right_in,
					R.anim.push_right_out);
			break;
		case R.id.iv_share:
			startActivity(new Intent(this, BuyerShareActivity.class));
			overridePendingTransition(R.anim.push_right_in,
					R.anim.push_right_out);
			break;
		case R.id.iv_show:
			SharedPreferencesUitls.saveString(getApplicationContext(),
					"ActivityTag", "play");
			SharedPreferencesUitls.saveBoolean(getApplicationContext(),
					"is_detail", true);
			startActivity(new Intent(this, UnityPlayerNativeActivity.class));
			overridePendingTransition(R.anim.push_right_in,
					R.anim.push_right_out);
			break;
		}
	}

	// 重写back键
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			startActivity(new Intent(ThemeActivity.this, HomeActivity.class));
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub

	}
	
	
}
