package com.ez_cloud.traditioncustom;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.base.MyBaseAdapter;
import com.ez_cloud.bean.ClothAllInfo;
import com.ez_cloud.bean.ClothAllInfo.ClothInfos;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.theme.ThemeActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.MyBitmapUtils;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.Utils;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class CustomizationActivity extends BaseActivity implements
		OnClickListener {
	@ViewInject(R.id.gridview_1499)
	GridView mGridView_1499;
	@ViewInject(R.id.gridview_1800)
	GridView mGridView_1800;
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	@ViewInject(R.id.rl_progressBar)
	ProgressBar rl_progressBar;
	@ViewInject(R.id.tv_cloth_top_name)
	TextView clothTopName;
	@ViewInject(R.id.iv_cloth_bitmap)
	ImageView clothBitmap;
	@ViewInject(R.id.tv_cloth_num)
	TextView clothNo;
	@ViewInject(R.id.tv_cloth_feature)
	TextView clothFeature;
	@ViewInject(R.id.tv_cloth_price)
	TextView clothPrice;
	@ViewInject(R.id.tv_cloth_characteristic)
	TextView clothColor;
	@ViewInject(R.id.tv_cloth_repertory)
	TextView clothRepertory;
	@ViewInject(R.id.tv_gewen)
	TextView gewen;
	@ViewInject(R.id.tv_tiaowen)
	TextView tiaowen;
	@ViewInject(R.id.tv_suse)
	TextView suse;
	@ViewInject(R.id.tv_yitang)
	TextView yitang;
	@ViewInject(R.id.tv_miantang)
	TextView miantang;
	@ViewInject(R.id.tv_shushi)
	TextView shushi;
	@ViewInject(R.id.tv_all)
	TextView all;
	@ViewInject(R.id.tv_purchase)
	TextView purchase;
	@ViewInject(R.id.custom_view)
	View mView;

	private ClothInfos electCloth;// 用户选择的布料
	private List<ClothInfos> cloths;
	private ImageView select;
	private List<ClothInfos> cloth_1499 = new ArrayList<ClothInfos>();
	private List<ClothInfos> cloth_1800 = new ArrayList<ClothInfos>();
	// 遍历数据，分出1499/1800布料
	private Cloth_1499Adapter mGridViewAdapter_1499;
	private cloth_1800Adapter mGridViewAdapter_1800;

	private String country;
	private boolean isCloth1499 = false;
	private Map<String, String> commitData;
	private ClothingApplication clothingApplication;

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			rl_progressBar.setVisibility(View.GONE);
			mView.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					ClothAllInfo allInfo = (ClothAllInfo) msg.obj;
					if ("00".equals(allInfo.code)) {
						if (null != allInfo.result && allInfo.result.size() > 0) {
							cloths.clear();
							cloths = allInfo.result;
						} else {
							// 清空集合数据
							emptyListData();
						}
						setClothList(cloths);
					} else {
						Toast.makeText(CustomizationActivity.this,
								R.string.sold_out, Toast.LENGTH_SHORT).show();
						// 清空集合数据
						emptyListData();
					}
				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(CustomizationActivity.this,
						R.string.server_error, Toast.LENGTH_SHORT).show();
				break;
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customization);
		ViewUtils.inject(this);

		init();

		requestData(ServerInterface.CLOTH_All_URL + country);

		mGridView_1499.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				electCloth = cloth_1499.get(position);
				for (int i = 0; i < cloth_1499.size(); i++) {
					if (i == position) {
						mGridViewAdapter_1499.setFlag(i, true);

					} else {
						mGridViewAdapter_1499.setFlag(i, false);
					}
				}
				if (cloth_1800 != null && cloth_1800.size() > 0) {
					for (int i = 0; i < cloth_1800.size(); i++) {
						mGridViewAdapter_1800.setFlag(i, false);
					}
				}
				setTextData(mGridViewAdapter_1499, electCloth);
			}

		});
		mGridView_1800.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				electCloth = cloth_1800.get(position);
				for (int i = 0; i < cloth_1800.size(); i++) {
					if (i == position) {
						mGridViewAdapter_1800.setFlag(i, true);

					} else {
						mGridViewAdapter_1800.setFlag(i, false);
					}
				}
				if (cloth_1499 != null && cloth_1499.size() > 0) {
					for (int i = 0; i < cloth_1499.size(); i++) {
						mGridViewAdapter_1499.setFlag(i, false);
					}
				}
				setTextData(mGridViewAdapter_1800, electCloth);
			}

		});

	}

	/**
	 * 初始化
	 */
	private void init() {
		clothingApplication = (ClothingApplication) getApplication();
		commitData = clothingApplication.getmMap();

		List<BaseActivity> activityList = clothingApplication.getActivityList();
		activityList.add(CustomizationActivity.this);// 把打开的activity装到集合中
		clothingApplication.setActivityList(activityList);

		cloths = new ArrayList<ClothInfos>();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);

		mGridView_1499.setSelector(new ColorDrawable(Color.TRANSPARENT));
		mGridView_1800.setSelector(new ColorDrawable(Color.TRANSPARENT));
		country = Utils.getCountry(this);
		
		initClick();
	}

	private void initClick() {
		tbl_title.getBack().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(CustomizationActivity.this,
						ThemeActivity.class));
				CustomizationActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			}
		});
		tbl_title.getNext().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				nextStep();
			}
		});
		all.setOnClickListener(this);
		gewen.setOnClickListener(this);
		tiaowen.setOnClickListener(this);
		suse.setOnClickListener(this);
		yitang.setOnClickListener(this);
		miantang.setOnClickListener(this);
		shushi.setOnClickListener(this);
		purchase.setOnClickListener(this);
	}

	/**
	 * 遍历出1499/1800价格的布料数据
	 * 
	 * @param cloths
	 */
	protected void setClothList(List<ClothInfos> cloths) {
		cloth_1499.clear();
		cloth_1800.clear();
		for (ClothInfos info : cloths) {
			if ("1499".equals(info.price) || "1499".equals(info.price)) {
				cloth_1499.add(info);
			} else if ("1800".equals(info.price) || "1800".equals(info.price)) {
				cloth_1800.add(info);
			}
		}

		setGridView(mGridView_1499, cloth_1499);
		mGridViewAdapter_1499 = new Cloth_1499Adapter(
				CustomizationActivity.this, cloth_1499);
		mGridView_1499.setAdapter(mGridViewAdapter_1499);
		if (cloth_1499.size() > 0) {
			defaultCloth(mGridViewAdapter_1499, cloth_1499);// 设置默认选中的布料
			isCloth1499 = true;
		}

		setGridView(mGridView_1800, cloth_1800);
		mGridViewAdapter_1800 = new cloth_1800Adapter(
				CustomizationActivity.this, cloth_1800);
		mGridView_1800.setAdapter(mGridViewAdapter_1800);
		if (!isCloth1499) {
			defaultCloth(mGridViewAdapter_1800, cloth_1800);// 设置默认选中的布料
		}
		isCloth1499 = false;

	}

	/**
	 * 设置单列显示的GridView宽高参数
	 * 
	 * @param mGridView
	 * @param cloths
	 */
	private void setGridView(GridView mGridView, List<ClothInfos> cloths) {

		int size = cloths.size();

		int length = 100;

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		float density = dm.density;
		int gridviewWidth = (int) (size * (length + 4) * density);
		int itemWidth = (int) (length * density);

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				gridviewWidth, LinearLayout.LayoutParams.FILL_PARENT);
		mGridView.setLayoutParams(params); // 重点
		mGridView.setColumnWidth(itemWidth); // 重点
		mGridView.setHorizontalSpacing(5); // 间距
		mGridView.setStretchMode(GridView.NO_STRETCH);
		mGridView.setNumColumns(size); // 重点

	}

	/**
	 * 填充1499布料数据
	 * 
	 * @author pc
	 * 
	 */
	public class Cloth_1499Adapter extends MyBaseAdapter {
		private List<ClothInfos> cloths;
		private Context context;

		public Cloth_1499Adapter(Context context, List<ClothInfos> cloths) {
			this.cloths = cloths;
			this.context = context;
		}

		@Override
		public int getCount() {
			return cloths == null ? 0 : cloths.size();
		}

		@Override
		public Object getItem(int position) {
			return cloths == null ? 0 : cloths.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private ClothInfos clothInfo;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(context, R.layout.bl_gridview_item, null);
			ImageView iconiView = (ImageView) view
					.findViewById(R.id.gridview_item_ico);
			select = (ImageView) view.findViewById(R.id.gridview_select);
			clothInfo = cloths.get(position);

			MyBitmapUtils.display(
					CustomizationActivity.this.getApplicationContext(),
					iconiView, clothInfo.path);
			if (clothInfo.flag == true) {
				select.setVisibility(View.VISIBLE);
			} else {
				select.setVisibility(View.INVISIBLE);
			}

			return view;
		}

		public void setFlag(int position, Boolean isChecked) {
			cloths.get(position).flag = isChecked;
		}

	}

	/**
	 * 填充1800布料数据
	 * 
	 * @author pc
	 * 
	 */
	public class cloth_1800Adapter extends MyBaseAdapter {
		private List<ClothInfos> cloths;
		private Context context;

		public cloth_1800Adapter(Context context, List<ClothInfos> cloths) {
			this.cloths = cloths;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return cloths == null ? 0 : cloths.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return cloths == null ? 0 : cloths.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		private ClothInfos clothInfo;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(context, R.layout.bl_gridview_item, null);
			ImageView iconiView = (ImageView) view
					.findViewById(R.id.gridview_item_ico);
			select = (ImageView) view.findViewById(R.id.gridview_select);
			clothInfo = cloths.get(position);

			// MyBitmapUtils.downImage(clothInfo.path, iconiView, true);
			BitmapUtils bitmapUtils = new BitmapUtils(
					CustomizationActivity.this);
			bitmapUtils.display(iconiView, clothInfo.path);
			if (clothInfo.flag == true) {
				select.setVisibility(View.VISIBLE);
			} else {
				select.setVisibility(View.INVISIBLE);
			}

			return view;
		}

		public void setFlag(int position, Boolean isChecked) {
			cloths.get(position).flag = isChecked;
		}

	}

	/**
	 * 请求服务器
	 * 
	 * @param url
	 */
	public void requestData(String url) {
		ClothingHttpUtils.httpSend(HttpMethod.GET, url, null,
				ClothAllInfo.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);
	}

	/**
	 * 处理点击事件
	 * 
	 * @author pc
	 * 
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_purchase:
			//下一步
			nextStep();
			break;
		case R.id.tv_all:
			rl_progressBar.setVisibility(View.VISIBLE);
			mView.setVisibility(View.VISIBLE);
			requestData(ServerInterface.CLOTH_All_URL + country);// 请求服务器获取数据
			break;
		case R.id.tv_suse:// 素色
			rl_progressBar.setVisibility(View.VISIBLE);
			mView.setVisibility(View.VISIBLE);
			requestData(ServerInterface.CLOTH_S_URL + country);// 请求服务器获取数据
			break;
		case R.id.tv_tiaowen:// 条纹
			rl_progressBar.setVisibility(View.VISIBLE);
			mView.setVisibility(View.VISIBLE);
			requestData(ServerInterface.CLOTH_T_URL + country);// 请求服务器获取数据
			break;
		case R.id.tv_gewen:// 格纹
			rl_progressBar.setVisibility(View.VISIBLE);
			mView.setVisibility(View.VISIBLE);
			requestData(ServerInterface.CLOTH_G_URL + country);// 请求服务器获取数据
			break;
		case R.id.tv_yitang:// Y
			rl_progressBar.setVisibility(View.VISIBLE);
			mView.setVisibility(View.VISIBLE);
			requestData(ServerInterface.CLOTH_Y_URL + country);// 请求服务器获取数据
			break;
		case R.id.tv_miantang:// M
			rl_progressBar.setVisibility(View.VISIBLE);
			mView.setVisibility(View.VISIBLE);
			requestData(ServerInterface.CLOTH_M_URL + country);// 请求服务器获取数据
			break;
		case R.id.tv_shushi:// 格纹
			rl_progressBar.setVisibility(View.VISIBLE);
			mView.setVisibility(View.VISIBLE);
			requestData(ServerInterface.CLOTH_C_URL + country);// 请求服务器获取数据
			break;
		}

	}
	/**
	 * 下一步
	 */
	private void nextStep() {
		if (null == electCloth) {
			Toast.makeText(getApplicationContext(), R.string.clothnull,
					Toast.LENGTH_SHORT).show();
			return;
		}
		commitData.put("clothNo", electCloth.clothNo);
		commitData.put("clothName", electCloth.clothName);
		commitData.put("price", electCloth.price);
		commitData.put("path", electCloth.path);
		clothingApplication.setmMap(commitData);
		Intent intent = new Intent(new Intent(getApplicationContext(),
				ModelActivity.class));
		startActivity(intent);
		overridePendingTransition(R.anim.push_right_in,
				R.anim.push_right_out);
	}

	/**
	 * 选择默认布料
	 * 
	 * @param clothList
	 * @param cloth_18002
	 * @param mAdapter
	 * @param mGridViewAdapter_18002
	 */
	protected void defaultCloth(MyBaseAdapter mGridViewAdapter,
			List<ClothInfos> cloths) {
		if (cloths.size() > 0) {
			electCloth = cloths.get(0);// 默认选中第一个
			mGridViewAdapter.setFlag(0, true);
			setTextData(mGridViewAdapter, electCloth);// 设置数据
		} else if (electCloth != null) {
			clothTopName.setText(electCloth.clothName);
			clothBitmap.setImageResource(R.drawable.placehold_pi);
			clothNo.setText(R.string.none);
			clothFeature.setText(R.string.none);
			clothPrice.setText(R.string.none);
			clothRepertory.setText(R.string.none);
			clothColor.setText(R.string.none);
		}
		mGridViewAdapter.notifyDataSetChanged();
	}

	/**
	 * 设置详细数据
	 * 
	 * @param userCloth
	 */
	private void setTextData(MyBaseAdapter mGridViewAdapter,
			ClothInfos userCloth) {
		clothTopName.setText(userCloth.clothName);
		StringBuffer buffer = new StringBuffer(userCloth.path);
		int len = buffer.length();
		StringBuffer uri = buffer.insert(len - 4, "_" + 1);
		BitmapUtils bitmapUtils = new BitmapUtils(getApplicationContext());
		bitmapUtils.display(clothBitmap, uri.toString());
		clothNo.setText(userCloth.clothNo);
		clothFeature.setText(userCloth.component);
		clothPrice.setText("" + userCloth.price);
		if ("".equals(userCloth.storeNum)) {
			clothRepertory.setText(R.string.none);
		} else {
			clothRepertory.setText(userCloth.storeNum + "");
		}
		clothColor.setText(userCloth.feature);
		if (mGridViewAdapter_1499 != null)
			mGridViewAdapter_1499.notifyDataSetChanged();
		if (mGridViewAdapter_1800 != null)
			mGridViewAdapter_1800.notifyDataSetChanged();
	}

	private void emptyListData() {
		electCloth = null;
		cloth_1499.clear();
		cloth_1800.clear();
		if (null != mGridViewAdapter_1800 && null != mGridViewAdapter_1499) {
			mGridViewAdapter_1800.notifyDataSetChanged();
			mGridViewAdapter_1499.notifyDataSetChanged();
		}
	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub
		this.finish();
	};

}
