package com.ez_cloud.traditioncustom;

import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.unlty.UnityPlayerNativeActivity;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ModelActivity extends BaseActivity {
	@ViewInject(R.id.cb_cuff_text)
	Switch cuffSwitch;
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	@ViewInject(R.id.et_remark)
	EditText remark;
	@ViewInject(R.id.sv_scrollview)
	ScrollView mScrollView;
	@ViewInject(R.id.mrg_model)
	com.ez_cloud.view.ModelRadioGroup mrg_model;// 版型
	@ViewInject(R.id.mrg_pleat)
	com.ez_cloud.view.ModelRadioGroup mrg_pleat;// 打褶
	@ViewInject(R.id.mrg_sleeve)
	com.ez_cloud.view.ModelRadioGroup mrg_sleeve;// 袖子
	@ViewInject(R.id.mrg_cuff)
	com.ez_cloud.view.ModelRadioGroup mrg_cuff;// 袖口
	@ViewInject(R.id.mrg_collar)
	com.ez_cloud.view.ModelRadioGroup mrg_collar;// 衣领
	@ViewInject(R.id.mrg_menjin)
	com.ez_cloud.view.ModelRadioGroup mrg_menjin;// 门襟
	@ViewInject(R.id.mrg_pocket)
	com.ez_cloud.view.ModelRadioGroup mrg_pocket;// 口袋
	@ViewInject(R.id.mrg_fastener)
	com.ez_cloud.view.ModelRadioGroup mrg_fastener;// 纽扣
	@ViewInject(R.id.mrg_deduction_line)
	com.ez_cloud.view.ModelRadioGroup mrg_deduction_line;// 扣线

	private Map<String, String> commitData;// 提交给服务器的参数集合
	private PopupWindow popupWindow;
	private ClothingApplication clothingApplication;
	private String cuffString;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_model);
		ViewUtils.inject(this);

		init();

	}

	/**
	 * 初始化
	 */
	private void init() {
		clothingApplication = (ClothingApplication) getApplication();
		commitData = clothingApplication.getmMap();
		List<BaseActivity> activityList = clothingApplication.getActivityList();
		activityList.add(ModelActivity.this);
		clothingApplication.setActivityList(activityList);

		commitData.put("isEmb", "N");// 默认不锈字

		processorClick();// 处理点击事件

		initView();// 初始化ui

		setDefaultCheckData();
	}

	private void initView() {
		setHideView(2, mrg_sleeve.getRadioButtonList().size(),
				mrg_sleeve.getRadioButtonList());

		setHideView(4, mrg_cuff.getRadioButtonList().size(),
				mrg_cuff.getRadioButtonList());

		setHideView(3, mrg_fastener.getRadioButtonList().size(),
				mrg_fastener.getRadioButtonList());

		setHideView(2, mrg_deduction_line.getRadioButtonList().size(),
				mrg_deduction_line.getRadioButtonList());

	}

	/**
	 * 设置控件监听
	 */
	private void processorClick() {

		tbl_title.getBack().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				back();
			}
		});

		tbl_title.getNext().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				next();
			}
		});
		// 绣字
		cuffSwitch
				.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						embroidery(buttonView, isChecked);
					}
				});
		// 版型
		mrg_model.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:
							commitData.put("bx_Model", "1");
							break;
						case R.id.rg_but_two:
							commitData.put("bx_Model", "2");
							break;
						case R.id.rg_but_three:
							commitData.put("bx_Model", "3");
							break;
						case R.id.rg_but_four:
							commitData.put("bx_Model", "4");
							break;

						}
					}
				});

		// 打褶
		mrg_pleat.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:
							commitData.put("backSnap", "0");
							break;
						case R.id.rg_but_two:
							commitData.put("backSnap", "1");
							break;
						case R.id.rg_but_three:
							commitData.put("backSnap", "2");
							break;
						case R.id.rg_but_four:
							commitData.put("backSnap", "3");
							break;

						}
					}
				});

		// 袖子
		mrg_sleeve.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {
					List<RadioButton> rbLists = mrg_cuff.getRadioButtonList();

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:// 长袖
							commitData.put("sleeveLongShort", "L");
							// 显示前四个袖口
							setVisibleView(0, rbLists.size() - 1, rbLists);
							// 隐藏最后一个袖口
							setHideView(4, rbLists.size(), rbLists);
							// 选中袖口
							rbLists.get(0).setChecked(true);
							break;
						case R.id.rg_but_two:// 短袖
							commitData.put("sleeveLongShort", "S");
							commitData.put("sleeveStyle", "X002");
							// 隐藏前四个袖口
							setHideView(0, rbLists.size() - 1, rbLists);
							// 显示最后一个袖口
							setVisibleView(4, rbLists.size(), rbLists);
							// 选中袖口
							rbLists.get(rbLists.size() - 1).setChecked(true);
							break;

						}
					}
				});

		// 袖口
		mrg_cuff.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:
							commitData.put("sleeveStyle", "X004");
							break;
						case R.id.rg_but_two:
							commitData.put("sleeveStyle", "X005");
							break;
						case R.id.rg_but_three:
							commitData.put("sleeveStyle", "X007");
							break;
						case R.id.rg_but_four:
							commitData.put("sleeveStyle", "X009");
							break;
						case R.id.rg_but_five:
							commitData.put("sleeveStyle", "X002");
							break;

						}
					}
				});
		// 门襟
		mrg_menjin.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:
							commitData.put("collarModel", "L001");
							break;
						case R.id.rg_but_two:
							commitData.put("collarModel", "L003");
							break;
						case R.id.rg_but_three:
							commitData.put("collarModel", "L005");
							break;
						case R.id.rg_but_four:
							commitData.put("collarModel", "L007");
							break;

						}
					}
				});
		// 衣领
		mrg_collar.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:
							commitData.put("closing", "1");
							break;
						case R.id.rg_but_two:
							commitData.put("closing", "2");
							break;
						case R.id.rg_but_three:
							commitData.put("closing", "3");
							break;
						case R.id.rg_but_four:
							commitData.put("closing", "4");
							break;

						}
					}
				});
		// 口袋
		mrg_pocket.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:
							commitData.put("pocket", "0");
							break;
						case R.id.rg_but_two:
							commitData.put("pocket", "1");
							break;
						case R.id.rg_but_three:
							commitData.put("pocket", "2");
							break;
						case R.id.rg_but_four:
							commitData.put("pocket", "3");
							break;

						}
					}
				});
		// 纽扣
		mrg_fastener.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:
							commitData.put("fastener", "1");
							break;
						case R.id.rg_but_two:
							commitData.put("fastener", "2");
							break;
						case R.id.rg_but_three:
							commitData.put("fastener", "3");
							break;
						}
					}
				});
		// 扣线
		mrg_deduction_line.getRadioGroup().setOnCheckedChangeListener(
				new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checked) {
						switch (checked) {
						case R.id.rg_but_one:
							commitData.put("buttonLine", "1");
							break;
						case R.id.rg_but_two:
							commitData.put("buttonLine", "2");
							break;
						}
					}
				});
	}

	/**
	 * 绣字处理
	 * 
	 * @param buttonView
	 * @param isChecked
	 */
	private void embroidery(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			View cuffTextView = View.inflate(ModelActivity.this,
					R.layout.popupwindowview, null);
			final EditText cuffText = (EditText) cuffTextView
					.findViewById(R.id.needlework_text);
			TextView cancel = (TextView) cuffTextView.findViewById(R.id.cancel);
			TextView ok = (TextView) cuffTextView.findViewById(R.id.ok);
			showPopupWindow(ModelActivity.this.getWindow().getDecorView(),
					cuffTextView, buttonView);
			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					cuffSwitch.setChecked(false);
					commitData.put("isEmb", "N");
					if (popupWindow != null) {
						popupWindow.dismiss();
						popupWindow = null;
					}
				}
			});
			ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					cuffString = cuffText.getText().toString();
					if (TextUtils.isEmpty(cuffString)) {
						Toast.makeText(ModelActivity.this, R.string.input_null,
								Toast.LENGTH_SHORT).show();
						return;
					}
					cuffSwitch.setChecked(true);
					commitData.put("isEmb", "Y");
					commitData.put("embContent", cuffString);
					if (popupWindow != null) {
						popupWindow.dismiss();
						popupWindow = null;
					}
				}
			});
		} else {
			commitData.put("isEmb", "N");
		}
	}

	/**
	 * 设置默认选中数据
	 */
	private void setDefaultCheckData() {
		commitData.put("bx_Model", "1");// 版型 1标准版 2宽松 3贴身 4修身

		commitData.put("backSnap", "0");// 0后无打折 1背侧打折 2腰侧打折 3背中打折

		commitData.put("sleeveLongShort", "L");// 长袖短袖 L/S

		commitData.put("sleeveStyle", "X004");// X004两用 X005圆角X007截角X009法式X002短袖

		commitData.put("collarModel", "L001");// L001标准领 L003温莎领L005钉扣领 L007立领

		commitData.put("closing", "1");// 1传统门襟 2传统暗门襟 3法式门襟 4法式暗门襟

		commitData.put("pocket", "0");// 0无口袋 1圆角口袋 2五角口袋3六角口袋

		commitData.put("fastener", "1");// 1白色 2黑色 3木色

		commitData.put("buttonLine", "1");// 1正色 2反色

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	/**
	 * 隐藏指定的RadioButton
	 * 
	 * @param first
	 *            开始角标
	 * @param last
	 *            结尾角标
	 * @param radioButtonList
	 *            数据集合
	 */
	private void setHideView(int first, int last,
			List<RadioButton> radioButtonList) {
		for (int i = first; i < last; i++) {
			radioButtonList.get(i).setVisibility(View.GONE);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (popupWindow != null) {
			popupWindow.dismiss();
		}
	}

	/**
	 * 显示指定的RadioButton
	 * 
	 * @param first
	 * @param last
	 * @param radioButtonList
	 */
	private void setVisibleView(int first, int last,
			List<RadioButton> radioButtonList) {
		for (int i = first; i < last; i++) {
			radioButtonList.get(i).setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 返回上一级
	 */
	private void back() {
		startActivity(new Intent(ModelActivity.this,
				CustomizationActivity.class));
		ModelActivity.this.finish();
		overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 下一步
	 */
	private void next() {
		commitData.put("remark", remark.getText().toString());
		SharedPreferencesUitls.saveBoolean(getApplicationContext(),
				"is_detail", false);
		SharedPreferencesUitls.saveString(getApplicationContext(),
				"ActivityTag", "model");
		clothingApplication.setmMap(commitData);
		startActivity(new Intent(ModelActivity.this,
				UnityPlayerNativeActivity.class));
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
	}

	/**
	 * 显示PopupWindow
	 * 
	 * @param v
	 *            View
	 * @param view
	 *            布局
	 * @param iv
	 *            点击的控件
	 */
	private void showPopupWindow(View v, View view, View iv) {
		popupWindow = new PopupWindow(view, -2, -2);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		int[] location = new int[2];
		iv.getLocationOnScreen(location);

		// popupWindow.showAtLocation(v, Gravity.BOTTOM, location[0],
		// location[1] + iv.getHeight()+10);
		popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub
		this.finish();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		init();
	}

}
