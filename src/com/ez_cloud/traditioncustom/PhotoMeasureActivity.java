package com.ez_cloud.traditioncustom;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.AppointmentData;
import com.ez_cloud.bean.AppointmentData.Result;
import com.ez_cloud.bean.MeasureHistoryInfo;
import com.ez_cloud.bean.MeasureHistoryInfo.UserData;
import com.ez_cloud.bean.WebReturnData;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.theme.ThemeActivity;
import com.ez_cloud.unlty.UnityPlayerNativeActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.Member;
import com.ez_cloud.utils.MyBitmapUtils;
import com.ez_cloud.utils.PhotoUtil;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PhotoMeasureActivity extends BaseActivity implements
		OnClickListener, OnItemSelectedListener {
	@ViewInject(R.id.et_photo_height)
	EditText et_height;
	@ViewInject(R.id.et_photo_weight)
	EditText et_weight;
	@ViewInject(R.id.et_photo_address)
	EditText et_address;
	@ViewInject(R.id.et_photo)
	EditText et_phone;
	@ViewInject(R.id.et_photo_name)
	EditText et_name;
	@ViewInject(R.id.iv_photo_front)
	View photo_front;
	@ViewInject(R.id.iv_photo_side)
	View photo_side;
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	@ViewInject(R.id.bt_front)
	TextView bt_front;
	@ViewInject(R.id.bt_side)
	TextView bt_side;
	@ViewInject(R.id.ll_photo_measures)
	FrameLayout layoutFather;
	@ViewInject(R.id.spinner)
	Spinner mSpinner;
	@ViewInject(R.id.appointment_time)
	Spinner appointment_time;
	@ViewInject(R.id.bt_commit)
	Button commit;
	@ViewInject(R.id.photo_pb)
	ProgressBar mProgressBar;
	@ViewInject(R.id.iv_history_data)
	ImageView history;

	private Map<String, String> commitData;// 数据集合
	private PopupWindow popupWindow;
	private List<String> city_list;
	private List<String> szTimeList;
	private List<String> twTimeList;
	private boolean frontPhotoTag = false;
	private boolean sidePhotoTag = false;
	private boolean isFrontUploading = false;
	private boolean isSideUploading = false;
	private boolean subscribe = false;
	private List<Result> appointmentDatas;
	public String filePath = "";
	RelativeLayout layout_choose;
	RelativeLayout layout_photo;
	protected int mScreenWidth;
	protected int mScreenHeight;
	PopupWindow avatorPop;
	private Bitmap cameraBitmap;
	Bitmap newBitmap;
	boolean isFromCamera = false;// 区分拍照旋转
	int degree = 0;
	private Bitmap frontBitmap;
	private Bitmap sideBitmap;
	private View photoView;
	private LinearLayout ll;
	private ClothingApplication clothingApplication;
	private boolean isMeasureHistory;

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			mProgressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg) {
					AppointmentData appointmentData = (AppointmentData) msg.obj;
					if ("00".equals(appointmentData.code)) {
						appointmentDatas = appointmentData.result;
						for (Result infos : appointmentDatas) {
							if ("01".equals(infos.cityCode)) {
								twTimeList.add(infos.arrangeDate + " "
										+ infos.arrangeTime);
							} else if ("02".equals(infos.cityCode)) {
								szTimeList.add(infos.arrangeDate + " "
										+ infos.arrangeTime);
							}
							setSpinnerAdapter(new SpinnerAdapter(
									getApplicationContext(),
									android.R.layout.simple_spinner_item,
									twTimeList));
						}
					}
					/*
					 * else { Toast.makeText(PhotoMeasureActivity.this,
					 * R.string.data_failure_hint, 0).show(); }
					 */
				}
				break;
			case Constants.REQUEST_TAG_A:
				if (null != msg.obj) {
					WebReturnData webInfo = (WebReturnData) msg.obj;
					if ("00".equals(webInfo.code)) {
						subscribe = true;
						Toast.makeText(PhotoMeasureActivity.this,
								R.string.commit_succeed, Toast.LENGTH_SHORT)
								.show();
					} else {
						subscribe = false;
						Toast.makeText(PhotoMeasureActivity.this,
								R.string.yuyuets, Toast.LENGTH_SHORT).show();
					}
				}
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(PhotoMeasureActivity.this,
						R.string.server_error, Toast.LENGTH_SHORT).show();
				break;
			case Constants.REQUEST_TAG_B:
				if (null != msg.obj) {
					MeasureHistoryInfo mMeasureHistoryInfo = (MeasureHistoryInfo) msg.obj;
					if ("00".equals(mMeasureHistoryInfo.code)) {
						UserData mUserData = mMeasureHistoryInfo.userData;
						BitmapUtils bitmapUtils = new BitmapUtils(
								PhotoMeasureActivity.this);
						bitmapUtils.display(photo_front,
								"http://121.42.29.14:8080"
										+ mUserData.frontPicPath,
								new BitmapLoadCallBack<View>() {

									@Override
									public void onLoadCompleted(View view,
											String arg1, Bitmap bitmap,
											BitmapDisplayConfig arg3,
											BitmapLoadFrom arg4) {
										frontBitmap = bitmap;
										view.setBackgroundDrawable(MyBitmapUtils
												.bitmapToDrawable(ThumbnailUtils
														.extractThumbnail(
																bitmap, 800,
																800)));
									}

									@Override
									public void onLoadFailed(View view,
											String arg1, Drawable arg2) {
										view.setBackgroundResource(R.drawable.default_photo);
									}
								});
						bitmapUtils.display(photo_side,
								"http://121.42.29.14:8080"
										+ mUserData.backPicPath,
								new BitmapLoadCallBack<View>() {

									@Override
									public void onLoadCompleted(View view,
											String arg1, Bitmap bitmap,
											BitmapDisplayConfig arg3,
											BitmapLoadFrom arg4) {
										sideBitmap = bitmap;
										view.setBackgroundDrawable(MyBitmapUtils
												.bitmapToDrawable(ThumbnailUtils
														.extractThumbnail(
																bitmap, 800,
																800)));
									}

									@Override
									public void onLoadFailed(View view,
											String arg1, Drawable arg2) {
										view.setBackgroundResource(R.drawable.default_photo);
									}
								});
						et_height.setText(mUserData.bodyHeight);
						et_weight.setText(mUserData.bodyWeight);
						isFrontUploading = true;
						isSideUploading = true;
					} else {
						Toast.makeText(PhotoMeasureActivity.this,
								R.string.data_failure_hint, Toast.LENGTH_SHORT)
								.show();
					}
				}
				break;
			case Constants.REQUEST_TAG_C:
				Toast.makeText(PhotoMeasureActivity.this, R.string.history_ts,
						Toast.LENGTH_SHORT).show();
				break;
			case Constants.CONFIGTIMEOUT:
				Toast.makeText(PhotoMeasureActivity.this,
						R.string.server_error, Toast.LENGTH_SHORT).show();
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_measures);
		ViewUtils.inject(this);

		initData();

		initClick();

		initAppointmentCity();

		initAppointmentTime();

	}

	/**
	 * 初始化数据
	 */
	private void initData() {
		clothingApplication = (ClothingApplication) getApplication();
		commitData = clothingApplication.getmMap();
		// commitData.put("measurModel", "0");
		List<BaseActivity> activityList = clothingApplication.getActivityList();
		activityList.add(PhotoMeasureActivity.this);
		clothingApplication.setActivityList(activityList);

		szTimeList = new ArrayList<String>();
		twTimeList = new ArrayList<String>();
		photoView = View.inflate(PhotoMeasureActivity.this,
				R.layout.pop_photo_view, null);
		ll = (LinearLayout) photoView.findViewById(R.id.ll_pop_photo);

	}

	/**
	 * 初始化控件
	 */
	private void initClick() {
		bt_front.setOnClickListener(this);
		bt_side.setOnClickListener(this);
		photo_front.setOnClickListener(this);
		photo_side.setOnClickListener(this);
		commit.setOnClickListener(this);
		history.setOnClickListener(this);

		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (SharedPreferencesUitls.getBoolean(
						PhotoMeasureActivity.this, "isonecustomize", false)) {
					startActivity(new Intent(PhotoMeasureActivity.this,
							ThemeActivity.class));
				} else {
					startActivity(new Intent(PhotoMeasureActivity.this,
							UnityPlayerNativeActivity.class));
				}
				PhotoMeasureActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);
			}
		});
		tbl_title.getNext().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				nextStep();
			}

		});
	}

	/**
	 * 下一步
	 */
	private void nextStep() {
		String heightText = et_height.getText().toString();
		String weightText = et_weight.getText().toString();
		if (subscribe || isMeasureHistory) {
			commitData.put("height", heightText);
			commitData.put("weight", weightText);
			startActivity(new Intent(PhotoMeasureActivity.this,
					PreviewActivity.class));
			overridePendingTransition(R.anim.push_right_in,
					R.anim.push_right_out);
			return;
		}

		if (!isFrontUploading || !isSideUploading) {
			Toast.makeText(getApplicationContext(), R.string.photo_ts,
					Toast.LENGTH_SHORT).show();
		} else if (TextUtils.isEmpty(heightText)) {
			Toast.makeText(getApplicationContext(), R.string.photo_hint_height,
					Toast.LENGTH_SHORT).show();
		} else if (TextUtils.isEmpty(weightText)) {
			Toast.makeText(getApplicationContext(), R.string.photo_hint_weight,
					Toast.LENGTH_SHORT).show();
		} else {
			saveBitmap();// 把测量图片保存到本地
			commitData.put("height", heightText);
			commitData.put("weight", weightText);
			clothingApplication.setmMap(commitData);
			startActivity(new Intent(PhotoMeasureActivity.this,
					PreviewActivity.class));
			overridePendingTransition(R.anim.push_right_in,
					R.anim.push_right_out);
		}
	}

	/**
	 * 初始化预约时间
	 */
	private void initAppointmentTime() {
		// TODO Auto-generated method stub
		ClothingHttpUtils.httpSend(HttpMethod.GET,
				ServerInterface.APPOINTMENT_MEASURE, null,
				AppointmentData.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);
	}

	/**
	 * 初始化预约城市
	 */
	private void initAppointmentCity() {
		city_list = new ArrayList<String>();
		// city_list.add("台湾-台北");
		// city_list.add("台湾-基隆市");
		// city_list.add("台湾-新北市");

		ArrayAdapter<String> cityAdapter = new SpinnerAdapter(this,
				android.R.layout.simple_spinner_item, city_list);
		cityAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinner.setAdapter(cityAdapter);
		mSpinner.setOnItemSelectedListener(this);
		mSpinner.setSelection(0);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_front:
			frontPhotoTag = true;
			isMeasureHistory = false;
			showAvatarPop();
			break;
		case R.id.bt_side:
			sidePhotoTag = true;
			isMeasureHistory = false;
			showAvatarPop();
			break;
		case R.id.iv_photo_front:

			ll.setBackgroundDrawable(MyBitmapUtils
					.bitmapToDrawable(frontBitmap));
			ll.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (popupWindow != null) {
						popupWindow.dismiss();
					}

				}
			});
			showPopupWindow(PhotoMeasureActivity.this.getWindow()
					.getDecorView(), photoView, layoutFather);
			break;
		case R.id.iv_photo_side:

			ll.setBackgroundDrawable(MyBitmapUtils.bitmapToDrawable(sideBitmap));
			ll.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (popupWindow != null) {
						popupWindow.dismiss();
					}

				}
			});
			showPopupWindow(PhotoMeasureActivity.this.getWindow()
					.getDecorView(), photoView, layoutFather);
			break;
		case R.id.bt_commit:
			Toast.makeText(PhotoMeasureActivity.this, R.string.no_open, Toast.LENGTH_SHORT).show();
//			subscribeCommit();暂未开放预约上门量身功能，开放后打开此处注释即可
			break;
		case R.id.iv_history_data:
			if (Member.isMember(PhotoMeasureActivity.this)) {
				mProgressBar.setVisibility(View.VISIBLE);
				isMeasureHistory = true;
				ClothingHttpUtils.httpSend(
						HttpMethod.GET,
						ServerInterface.HISTORY_MEASURE_DATA
								+ SharedPreferencesUitls
										.getString(PhotoMeasureActivity.this,
												"userId", ""), null,
						MeasureHistoryInfo.class, handler,
						Constants.REQUEST_TAG_B, Constants.REQUEST_FAILURE);
			}
			break;
		}

	}

	private void subscribeCommit() {
		String nameText = et_name.getText().toString();
		if (TextUtils.isEmpty(nameText)) {
			Toast.makeText(getApplicationContext(), R.string.hint_name,
					Toast.LENGTH_SHORT).show();
			return;
		}
		String phoneText = et_phone.getText().toString();
		if (TextUtils.isEmpty(phoneText)) {
			Toast.makeText(getApplicationContext(), R.string.hint_phone,
					Toast.LENGTH_SHORT).show();
			return;
		}
		String addressText = et_address.getText().toString();
		if (TextUtils.isEmpty(addressText)) {
			Toast.makeText(getApplicationContext(),
					R.string.hint_caifengshi, Toast.LENGTH_SHORT).show();
			return;
		}
		int selectedItemPosition = appointment_time
				.getSelectedItemPosition();
		RequestParams params = new RequestParams();
		params.addBodyParameter("id",
				appointmentDatas.get(selectedItemPosition).id);
		params.addBodyParameter("webUserTel", phoneText);
		params.addBodyParameter("webUserName", nameText);
		params.addBodyParameter("webUserAddress", addressText);
		mProgressBar.setVisibility(View.VISIBLE);
		ClothingHttpUtils.httpSend(HttpMethod.POST,
				ServerInterface.APPOINTMENT_COMMIT, params,
				WebReturnData.class, handler, Constants.REQUEST_TAG_A,
				Constants.REQUEST_FAILURE);
	}

	/**
	 * 显示图片选择
	 * 
	 * @param iv
	 */
	private void showAvatarPop() {
		View view = LayoutInflater.from(this).inflate(
				R.layout.photo_showavator, null);
		layout_choose = (RelativeLayout) view.findViewById(R.id.layout_choose);
		layout_photo = (RelativeLayout) view.findViewById(R.id.layout_photo);
		layout_photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// ShowLog("点击拍照");
				layout_choose.setBackgroundColor(getResources().getColor(
						R.color.base_color_text_white));
				layout_photo.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.pop_bg_press));
				File dir = new File(Constants.MyAvatarDir);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				// 原图
				File file = new File(dir, new SimpleDateFormat("yyMMddHHmmss")
						.format(new Date()));
				filePath = file.getAbsolutePath();// 获取相片的保存路径
				Uri imageUri = Uri.fromFile(file);

				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				startActivityForResult(intent,
						Constants.REQUESTCODE_UPLOADAVATAR_CAMERA);
				if (avatorPop != null) {
					avatorPop.dismiss();
				}
			}
		});
		layout_choose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				layout_photo.setBackgroundColor(getResources().getColor(
						R.color.base_color_text_white));
				layout_choose.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.pop_bg_press));
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent,
						Constants.REQUESTCODE_UPLOADAVATAR_LOCATION);
			}
		});

		avatorPop = new PopupWindow(view, mScreenWidth, 600);
		avatorPop.setTouchInterceptor(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					avatorPop.dismiss();
					return true;
				}
				return false;
			}
		});

		avatorPop.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		avatorPop.setHeight(550);
		avatorPop.setTouchable(true);
		avatorPop.setFocusable(true);
		avatorPop.setOutsideTouchable(true);
		avatorPop.setBackgroundDrawable(new BitmapDrawable());
		// 动画效果 从底部弹起
		avatorPop.setAnimationStyle(R.style.Animations_GrowFromBottom);
		avatorPop.showAtLocation(layoutFather, Gravity.BOTTOM, 0, 0);
	}

	/**
	 * 回调结果处理
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case Constants.REQUESTCODE_UPLOADAVATAR_CAMERA:// 拍照
			if (resultCode == RESULT_OK) {
				if (!Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					Toast.makeText(getApplicationContext(), R.string.no_sd,
							Toast.LENGTH_SHORT).show();
					return;
				}
				isFromCamera = true;
				File file = new File(filePath);
				degree = PhotoUtil.readPictureDegree(file.getAbsolutePath());
				cameraBitmap = MyBitmapUtils.getSmallBitmap(filePath);
				if (cameraBitmap == null) {
					Toast.makeText(getApplicationContext(),
							R.string.getiamge_fail, Toast.LENGTH_SHORT).show();
					return;
				}
			}

			break;
		case Constants.REQUESTCODE_UPLOADAVATAR_LOCATION:// 相册
			if (avatorPop != null) {
				avatorPop.dismiss();
			}
			Uri uri = null;
			if (data == null) {
				return;
			}
			if (resultCode == RESULT_OK) {
				if (!Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					Toast.makeText(getApplicationContext(), R.string.no_sd,
							Toast.LENGTH_SHORT).show();
					return;
				}
				isFromCamera = false;
				uri = data.getData();
				String[] proj = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(uri, proj, null, null, null);
				int column_index = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String path = cursor.getString(column_index);
				cameraBitmap = MyBitmapUtils.getSmallBitmap(path);
				if (cameraBitmap == null) {
					Toast.makeText(getApplicationContext(),
							R.string.getiamge_fail, Toast.LENGTH_SHORT).show();
					return;
				}
			} else {
				Toast.makeText(getApplicationContext(),
						R.string.photograph_fail, Toast.LENGTH_SHORT).show();
			}

			break;

		}
		// 初始化文件路径
		filePath = "";
		if (cameraBitmap != null) {
			processBitmap();
		}
	}

	/**
	 * 处理用户上传的照片
	 */
	private void processBitmap() {
		if (frontPhotoTag) {
			frontBitmap = cameraBitmap;
			photo_front.setBackgroundDrawable(MyBitmapUtils
					.bitmapToDrawable(ThumbnailUtils.extractThumbnail(
							cameraBitmap, 800, 800)));
			frontPhotoTag = !frontPhotoTag;
			isFrontUploading = true;
		}
		if (sidePhotoTag) {
			sideBitmap = cameraBitmap;
			photo_side.setBackgroundDrawable(MyBitmapUtils
					.bitmapToDrawable(ThumbnailUtils.extractThumbnail(
							cameraBitmap, 800, 800)));
			sidePhotoTag = !sidePhotoTag;
			isSideUploading = true;
		}
		cameraBitmap = null;
	}

	private void saveBitmap() {
		MyBitmapUtils.downloadBitmap(frontBitmap, "front.jpg");
		MyBitmapUtils.downloadBitmap(sideBitmap, "side.jpg");
	}

	/**
	 * 大图预览
	 * 
	 * @param v
	 * @param view
	 * @param iv
	 */
	private void showPopupWindow(View v, View view, View iv) {
		popupWindow = new PopupWindow(view,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		int[] location = new int[2];
		iv.getLocationOnScreen(location);

		popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, location[0],
				location[1]);
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub

		switch (position) {
		case 0:// 台湾
			setSpinnerAdapter(new SpinnerAdapter(getApplicationContext(),
					android.R.layout.simple_spinner_item, twTimeList));
			break;
		case 1:// 深圳
			setSpinnerAdapter(new SpinnerAdapter(getApplicationContext(),
					android.R.layout.simple_spinner_item, szTimeList));
			break;
		}

	}

	private void setSpinnerAdapter(ArrayAdapter<String> timeAdapter) {
		timeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		appointment_time.setAdapter(timeAdapter);
		appointment_time.setSelection(0);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	public class SpinnerAdapter extends ArrayAdapter<String> {

		Context context;
		List<String> items;

		public SpinnerAdapter(final Context context,
				final int textViewResourceId, final List<String> objects) {
			super(context, textViewResourceId, objects);
			this.items = objects;
			this.context = context;
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						android.R.layout.simple_spinner_item, parent, false);
			}

			TextView tv = (TextView) convertView
					.findViewById(android.R.id.text1);

			tv.setText(items.get(position));
			tv.setGravity(Gravity.CENTER);
			tv.setTextColor(Color.BLUE);
			tv.setTextSize(15);
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						android.R.layout.simple_spinner_item, parent, false);
			}

			TextView tv = (TextView) convertView
					.findViewById(android.R.id.text1);
			tv.setText(items.get(position));
			tv.setGravity(Gravity.CENTER);
			tv.setTextColor(Color.BLUE);
			tv.setTextSize(15);
			return convertView;
		}

	}

	public Bitmap decodeUriAsBitmap(Uri uri) {

		Bitmap bitmap = null;
		if (null == uri) {
			return null;
		}
		try {
			InputStream is = this.getContentResolver().openInputStream(uri);
			bitmap = BitmapFactory.decodeStream(is);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		return bitmap;
	}

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub
		this.finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (popupWindow != null) {
			popupWindow.dismiss();
		}
		if (frontBitmap != null)
			frontBitmap.recycle();
		if (sideBitmap != null)
			sideBitmap.recycle();
	}

}
