package com.ez_cloud.traditioncustom;

import java.io.File;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.base.BaseActivity;
import com.ez_cloud.bean.ReturnOrderInfo;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.personalcentre.AddressActivity;
import com.ez_cloud.personalcentre.MyOrderActivity;
import com.ez_cloud.theme.ThemeActivity;
import com.ez_cloud.utils.ClothingHttpUtils;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.Member;
import com.ez_cloud.utils.MyBitmapUtils;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PreviewActivity extends BaseActivity implements OnClickListener {
	@ViewInject(R.id.tv_preview_backSnap)
	TextView backSnap;
	@ViewInject(R.id.tv_preview_cuff)
	TextView cuff;
	@ViewInject(R.id.tv_preview_collar)
	TextView collar;
	@ViewInject(R.id.tv_preview_placket)
	TextView placket;
	@ViewInject(R.id.tv_preview_pocket)
	TextView pocket;
	@ViewInject(R.id.tv_height)
	TextView height;
	@ViewInject(R.id.tv_weight)
	TextView weight;
	@ViewInject(R.id.tv_preview_style)
	TextView style;
	@ViewInject(R.id.tv_preview_money)
	TextView price;
	@ViewInject(R.id.et_preview_number)
	TextView number;
	@ViewInject(R.id.tv_commodity_number)
	TextView commodityNumber;
	@ViewInject(R.id.tv_money_sum)
	TextView moneySum;
	@ViewInject(R.id.tv_preview_add)
	TextView add;
	@ViewInject(R.id.tv_preview_minus)
	TextView delete;
	@ViewInject(R.id.et_preview_beizhu)
	EditText beizhu;
	@ViewInject(R.id.et_embroidery)
	EditText xiuzi;
	@ViewInject(R.id.bon_save_commit)
	Button commitOrder;
	@ViewInject(R.id.iv_imagebackgroud)
	ImageView previewImage;
	@ViewInject(R.id.iv_preview_address)
	ImageView inputAddress;
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	@ViewInject(R.id.tv_username)
	TextView takeName;
	@ViewInject(R.id.tv_user_phone)
	TextView takePhone;
	@ViewInject(R.id.tv_preview_address)
	TextView takeAddress;
	@ViewInject(R.id.tv_preview_currency)
	TextView tv_currency;
	@ViewInject(R.id.sv_preview_scrollview)
	ScrollView mScrollView;
	@ViewInject(R.id.preview_progress_bar)
	ProgressBar mProgressBar;
	private com.ez_cloud.bean.OneCustomize.Result serializable;
	private String recipients;
	private String recipientsNumber;
	private String recipientsAddress;
	private String recipientsAreaCode;
	private int picNumber = 1;
	private List<BaseActivity> activityList;
	private boolean isOneCustomize;
	private Integer clothPrice;// 布料单价
	private int totalPrice;// 总价
	private Map<String, String> commitData;// 数据集合
	private String path = Environment.getExternalStorageDirectory()
			+ "/Android/data/com.ez_cloud.clothingcustom/files/front.JPG";

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			mProgressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				Toast.makeText(getApplicationContext(),
						R.string.order_commit_succeed, Toast.LENGTH_SHORT)
						.show();
				submitAchieve();
				break;
			case Constants.REQUEST_FAILURE:
				Toast.makeText(getApplicationContext(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
				break;
			}
		}

	};
	private ClothingApplication clothingApplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preview);
		ViewUtils.inject(this);

		init();

		initClick();

		initData();// 初始化数据

	}

	/**
	 * 初始化
	 */
	private void init() {
		clothingApplication = (ClothingApplication) getApplication();
		activityList = clothingApplication.getActivityList();
		activityList.add(PreviewActivity.this);
		clothingApplication.setActivityList(activityList);

		isOneCustomize = SharedPreferencesUitls.getBoolean(
				getApplicationContext(), "isonecustomize", false);
		commitData = clothingApplication.getmMap();
		if (isOneCustomize) {
			// 加载特款定制图片
			serializable = clothingApplication.getOneCustomizeInfo();
			MyBitmapUtils.display(getApplicationContext(), previewImage,
					serializable.imagePath);
		} else {
			// 加载3D模型截图
			loadUnityImage();
		}

	}

	private void initClick() {
		add.setOnClickListener(this);
		delete.setOnClickListener(this);
		commitOrder.setOnClickListener(this);
		inputAddress.setOnClickListener(this);

		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				PreviewActivity.this.finish();
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);
			}
		});

		tbl_title.getNext().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				nextStep();
			}

		});
	}
	/**
	 * 下一步
	 */
	private void nextStep() {
		commitData.clear();
		clothingApplication.setmMap(commitData);
		submitAchieve();
		startActivity(new Intent(getApplicationContext(),
				ThemeActivity.class));
		clothingApplication.destroyActivity();// 销毁已经记录的activity
		PreviewActivity.this.finish();
		overridePendingTransition(R.anim.push_left_in,
				R.anim.push_left_out);
	}

	/**
	 * 加载3D模型截图
	 */
	private void loadUnityImage() {
		BitmapUtils bitmapUtils = new BitmapUtils(this);
		bitmapUtils.configDefaultLoadFailedImage(R.drawable.placehold_pi);// 加载失败默认图片
		bitmapUtils.configDefaultBitmapConfig(Bitmap.Config.RGB_565);// 设置图片压缩类型
		bitmapUtils.clearMemoryCache();
		bitmapUtils.clearDiskCache();
		bitmapUtils.clearCache();
		bitmapUtils.closeCache();
		bitmapUtils.display(previewImage, path);
		bitmapUtils.clearCache();
		bitmapUtils.clearMemoryCache();
	}

	/**
	 * 初始化数据
	 */
	private void initData() {

		if (isOneCustomize) {
			backSnap.setText(serializable.backSnap);
			style.setText(serializable.bxStyle);
			cuff.setText(serializable.xkStyle);
			collar.setText(serializable.lkStyle);
			placket.setText(serializable.mjStyle);
			pocket.setText(serializable.kdStyle);

			clothPrice = Integer.parseInt(serializable.price);// 布料单价
			price.setText(clothPrice + "");
		} else {
			if ("1".equals(commitData.get("bx_Model"))) {
				style.setText(R.string.standard_edition);
			} else if ("2".equals(commitData.get("bx_Model"))) {
				style.setText(R.string.loose_edition);
			} else if ("3".equals(commitData.get("bx_Model"))) {
				style.setText(R.string.personal_edition);
			} else if ("4".equals(commitData.get("bx_Model"))) {
				style.setText(R.string.skintight_edition);
			}

			if ("0".equalsIgnoreCase(commitData.get("backSnap"))) {
				backSnap.setText(R.string.no_pleat);
			} else if ("1".equalsIgnoreCase(commitData.get("backSnap"))) {
				backSnap.setText(R.string.dorsal_part_pleat);
			} else if ("2".equalsIgnoreCase(commitData.get("backSnap"))) {
				backSnap.setText(R.string.waist_pleat);
			} else if ("3".equalsIgnoreCase(commitData.get("backSnap"))) {
				backSnap.setText(R.string.dorsal_line_pleat);

			}

			if ("X004".equals(commitData.get("sleeveStyle"))) {
				cuff.setText(R.string.purpose_cuff);
			} else if ("X007".equals(commitData.get("sleeveStyle"))) {
				cuff.setText(R.string.chamfering_cuff);
			} else if ("X005".equals(commitData.get("sleeveStyle"))) {
				cuff.setText(R.string.fillet_cuff);
			} else if ("X009".equals(commitData.get("sleeveStyle"))) {
				cuff.setText(R.string.french_cuff);
			} else if ("X002".equals(commitData.get("sleeveStyle"))) {
				cuff.setText(R.string.cotta);
			}

			if ("L001".equals(commitData.get("collarModel"))) {
				collar.setText(R.string.standard_collar);
			} else if ("L005".equals(commitData.get("collarModel"))) {
				collar.setText(R.string.fastener_collar);
			} else if ("L003".equals(commitData.get("collarModel"))) {
				collar.setText(R.string.windsor_collar);
			} else if ("L007".equals(commitData.get("collarModel"))) {
				collar.setText(R.string.stan_collar);
			}

			if ("1".equalsIgnoreCase(commitData.get("closing"))) {
				placket.setText(R.string.tradition_top_fly);
			} else if ("2".equalsIgnoreCase(commitData.get("closing"))) {
				placket.setText(R.string.ctamj);
			} else if ("3".equalsIgnoreCase(commitData.get("closing"))) {
				placket.setText(R.string.french_top_fly);
			} else if ("4".equalsIgnoreCase(commitData.get("closing"))) {
				placket.setText(R.string.fsamj);
			}

			if ("0".equalsIgnoreCase(commitData.get("pocket"))) {
				pocket.setText(R.string.no_pocket);
			} else if ("1".equalsIgnoreCase(commitData.get("pocket"))) {
				pocket.setText(R.string.fillet_pocket);
			} else if ("2".equalsIgnoreCase(commitData.get("pocket"))) {
				pocket.setText(R.string.pentagon_pocket);
			} else if ("3".equalsIgnoreCase(commitData.get("pocket"))) {
				pocket.setText(R.string.hexagonal_pocket);
			}
			clothPrice = Integer.parseInt(commitData.get("price"));// 布料的单价
			price.setText(clothPrice + "");

		}
		height.setText(commitData.get("height"));
		weight.setText(commitData.get("weight"));
		beizhu.setText(commitData.get("remark"));
		xiuzi.setText(commitData.get("embContent"));

		setRecipients();// 设置收件人信息

	}

	/**
	 * 设置收货人信息
	 */
	private void setRecipients() {
		recipients = SharedPreferencesUitls.getString(this, "takeName", "");
		recipientsNumber = SharedPreferencesUitls.getString(this, "takeNumber",
				"");
		recipientsAddress = SharedPreferencesUitls.getString(this, "address",
				"");
		recipientsAreaCode = SharedPreferencesUitls.getString(
				getApplicationContext(), "areaCode", "1");
		takePhone.setText(recipientsNumber);
		takeName.setText(recipients);
		takeAddress.setText(recipientsAddress);
		number.setText(picNumber + "");
		price.setText(clothPrice + "");
		totalPrice = clothPrice * picNumber;
		moneySum.setText(totalPrice + ".00");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bon_save_commit:// 保存订单，提交订单到服务器并提示用户付款
			if (takeAddress.getText().toString().length() <= 0) {
				Toast.makeText(getApplicationContext(),
						R.string.qsrdetailed_address, Toast.LENGTH_SHORT)
						.show();
				return;
			}

			String xiuziText = xiuzi.getText().toString();
			if (TextUtils.isEmpty(xiuziText)) {
				commitData.put("isEmb", "Y");
				commitData.put("embContent", xiuziText);
			}

			String commodityNumberText = commodityNumber.getText().toString();
			commitData.put("preNum", commodityNumberText);
			if (Member.isMember(PreviewActivity.this)) {
				mProgressBar.setVisibility(View.VISIBLE);
				requestWeb(ServerInterface.MODEL_COMMIT_URL, getParams());
			}
			break;
		case R.id.tv_preview_add:
			picNumber++;
			number.setText(picNumber + "");
			commodityNumber.setText(picNumber + "");
			totalPrice = clothPrice * picNumber;
			moneySum.setText(totalPrice + ".00");
			break;
		case R.id.tv_preview_minus:
			if (picNumber > 1) {
				picNumber--;
				number.setText(picNumber + "");
				commodityNumber.setText(picNumber + "");
				totalPrice = clothPrice * picNumber;
				moneySum.setText(totalPrice + ".00");
			} else {
				Toast.makeText(this, R.string.number_hint, Toast.LENGTH_SHORT)
						.show();
			}
			break;
		case R.id.iv_preview_address:
			if (Member.isMember(PreviewActivity.this)) {
				SharedPreferencesUitls.saveBoolean(this, "spikPreview", true);
				startActivity(new Intent(PreviewActivity.this,
						AddressActivity.class));
				this.finish();
			}
			break;
		}

	}

	/**
	 * 封装订单请求参数
	 * 
	 * @return
	 */
	private RequestParams getParams() {
		RequestParams requestParams = new RequestParams();
		if (isOneCustomize) {
			requestParams
					.addBodyParameter("clothNo", serializable.clothNo + "");
			requestParams
					.addBodyParameter("modelId", serializable.modelId + "");
			requestParams.addBodyParameter("price", clothPrice + "");
		} else {
			requestParams.addBodyParameter("orderId", "");
			requestParams
					.addBodyParameter("clothNo", commitData.get("clothNo"));
			requestParams.addBodyParameter("bxModel",
					commitData.get("bx_Model"));
			requestParams.addBodyParameter("sleeveLongShort",
					commitData.get("sleeveLongShort") + "");
			requestParams.addBodyParameter("sleeveStyle",
					commitData.get("sleeveStyle") + "");
			requestParams.addBodyParameter("collarModel",
					commitData.get("collarModel") + "");
			requestParams.addBodyParameter("closing", commitData.get("closing")
					+ "");
			requestParams.addBodyParameter("pocket", commitData.get("pocket")
					+ "");

			requestParams.addBodyParameter("chestLength",
					commitData.get("chestLength") + "");
			requestParams.addBodyParameter("waistLength",
					commitData.get("waistLength") + "");
			requestParams.addBodyParameter("collarLength",
					commitData.get("collarLength") + "");
			requestParams.addBodyParameter("shoulder",
					commitData.get("shoulder") + "");
			requestParams.addBodyParameter("sleeveLength",
					commitData.get("sleeveLength") + "");
			requestParams.addBodyParameter("clothLength",
					commitData.get("clothLength") + "");
			requestParams.addBodyParameter("wristLength",
					commitData.get("wristLength") + "");

			requestParams.addBodyParameter("fastener",
					commitData.get("fastener") + "");
			requestParams.addBodyParameter("buttonLine", commitData
					.get("buttonLine" + ""));
			requestParams.addBodyParameter("remark", commitData.get("remark")
					+ "");
			requestParams.addBodyParameter("backSnap",
					commitData.get("backSnap") + "");
			requestParams.addBodyParameter("price", clothPrice + "");
		}
		requestParams.addBodyParameter("userId", SharedPreferencesUitls
				.getString(getApplicationContext(), "userId", ""));
		requestParams.addBodyParameter("isEmb", commitData.get("isEmb") + "");
		requestParams.addBodyParameter("embContent",
				commitData.get("embContent") + "");
		requestParams.addBodyParameter("preNum", number.getText().toString()
				+ "");
		requestParams.addBodyParameter("measurModel", "0");
		requestParams.addBodyParameter("height", commitData.get("height") + "");
		requestParams.addBodyParameter("weight", commitData.get("weight") + "");
		requestParams
				.addBodyParameter("orderAreaCode", recipientsAreaCode + "");
		requestParams.addBodyParameter("receiverName", recipients + "");
		requestParams.addBodyParameter("receiverTel", recipientsNumber + "");
		requestParams.addBodyParameter("receiverAddress", recipientsAddress
				+ "");
		requestParams.addBodyParameter("totalPrice", totalPrice + "");
		File file = new File(Environment.getExternalStorageDirectory()
				+ "/front.jpg");
		if (file.exists()) {
			requestParams.addBodyParameter("front", file);
			Log.d("file=", file.getPath());
		}
		File side_file = new File(Environment.getExternalStorageDirectory()
				+ "/side.jpg");
		if (side_file.exists()) {
			requestParams.addBodyParameter("file1", side_file);
			Log.d("file1=", side_file.getPath());
		}
		return requestParams;
	}

	/**
	 * 请求服务器
	 * 
	 * @param url
	 * @param requestParams
	 */
	private void requestWeb(String url, RequestParams requestParams) {
		ClothingHttpUtils.httpSend(HttpMethod.POST, url, requestParams,
				ReturnOrderInfo.class, handler, Constants.REQUEST_SUCCEED,
				Constants.REQUEST_FAILURE);

	}

	private void submitAchieve() {
		MyBitmapUtils.deleteTempFile(Environment.getExternalStorageDirectory()
				+ "/front.jpg");
		MyBitmapUtils.deleteTempFile(Environment.getExternalStorageDirectory()
				+ "/side.jpg");
		commitData.clear();
		clothingApplication.setmMap(commitData);
		clothingApplication.destroyActivity();
		startActivity(new Intent(PreviewActivity.this, MyOrderActivity.class));
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
	};

	@Override
	public void mFinish() {
		// TODO Auto-generated method stub
		this.finish();
	}
}
