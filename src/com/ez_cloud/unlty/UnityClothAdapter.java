package com.ez_cloud.unlty;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ez_cloud.bean.ClothAllInfo.ClothInfos;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.BitmapUtils;
import com.unity3d.player.UnityPlayer;

public class UnityClothAdapter extends BaseAdapter {

	ArrayList<ClothInfos> bms;
	private Context context;
	private LayoutInflater inflater;
	private ImageView cloth;


	public UnityClothAdapter(
			Context context,
			List<ClothInfos> cloths, ImageView cloth) {
		this.context=context;
		this.bms=(ArrayList<ClothInfos>) cloths;
		inflater = LayoutInflater.from(context);
		this.cloth=cloth;
	}

	class ViewHolder{
		ImageView iv;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return bms.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return bms.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		final ClothInfos info=bms.get(position);
		if(convertView==null){
			holder=new ViewHolder();
			convertView=inflater.inflate(R.layout.unity_list_item, null);
			//			convertView=View.inflate(context, R.layout.unity_list_item, null);
			holder.iv = (ImageView) convertView.findViewById(R.id.gridview_item_cloth);
			convertView.setTag(holder);
		}

		holder = (ViewHolder) convertView.getTag();

//		MyBitmapUtils.downImage(info.path, holder.iv,true);
		BitmapUtils bitmapUtils = new BitmapUtils(context);
		bitmapUtils.display(holder.iv, info.path);
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				MyBitmapUtils.downImage(info.path, cloth,true);
				BitmapUtils bitmapUtils = new BitmapUtils(context);
				bitmapUtils.display(cloth, info.path);
				SharedPreferencesUitls.saveString(context, "unity_clothNo",info.clothNo);
				SharedPreferencesUitls.saveString(context, "unity_clothName", info.clothName);
				SharedPreferencesUitls.saveString(context, "unity_price", info.price);
				SharedPreferencesUitls.saveString(context, "unity_clothPath", info.path);
				int lastIndexOf = info.path.lastIndexOf('/');
				String substring = info.path.substring(lastIndexOf+1,info.path.length()-4);
				UnityPlayer.UnitySendMessage("Main Camera", "DP",substring);
				//				popupWindow.dismiss();
			}
		});
		return convertView;
	}

}
