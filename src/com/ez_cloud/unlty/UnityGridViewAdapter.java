package com.ez_cloud.unlty;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.ez_cloud.bean.ClothAllInfo.ClothInfos;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.lidroid.xutils.BitmapUtils;
import com.unity3d.player.UnityPlayer;

public class UnityGridViewAdapter extends BaseAdapter {
	private List<ClothInfos> bms;
	private Context context;

	

	public UnityGridViewAdapter(
			Context context,
			List<ClothInfos> cloths, PopupWindow popupWindow) {
		this.context=context;
		this.bms=cloths;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return bms.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return bms.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder holder=null;
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			holder=new ViewHolder();
			convertView=View.inflate(context, R.layout.unity_list_item, null);
			holder.iv = (ImageView) convertView.findViewById(R.id.gridview_item_cloth);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
			
//		MyBitmapUtils.downImage(bms.get(position).path, holder.iv,true);
		BitmapUtils bitmapUtils = new BitmapUtils(context);
		bitmapUtils.display(holder.iv,bms.get(position).path);
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String path=bms.get(position).path;
				SharedPreferencesUitls.saveString(context, "unity_clothNo", bms.get(position).clothNo);
				SharedPreferencesUitls.saveString(context, "unity_clothName", bms.get(position).clothName);
				SharedPreferencesUitls.saveString(context, "unity_price", bms.get(position).price);
				int lastIndexOf = path.lastIndexOf('/');
				String substring = path.substring(lastIndexOf+1,path.length()-4);
				UnityPlayer.UnitySendMessage("Main Camera", "DP",substring);
//				popupWindow.dismiss();
			}
		});
		return convertView;
	}
	class ViewHolder{
		 ImageView iv;
	}

}
