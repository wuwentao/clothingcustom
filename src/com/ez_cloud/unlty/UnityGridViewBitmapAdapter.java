package com.ez_cloud.unlty;

import java.util.List;

import com.ez_cloud.clothingcustom.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class UnityGridViewBitmapAdapter extends BaseAdapter {
	private Context context;
	private List<Integer> bms;

	public UnityGridViewBitmapAdapter(
			Context context,
			List<Integer> bms) {
		this.context=context;
		this.bms=bms;
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return bms.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return bms.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			convertView=View.inflate(context, R.layout.unity_list_item, null);
		}
		ImageView iv=(ImageView) convertView.findViewById(R.id.gridview_item_cloth);
		iv.setImageResource(bms.get(position));
		return convertView;
	}

}
