package com.ez_cloud.unlty;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.NativeActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import com.ez_cloud.application.ClothingApplication;
import com.ez_cloud.bean.ClothAllInfo;
import com.ez_cloud.bean.ClothAllInfo.ClothInfos;
import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.home.HomeActivity;
import com.ez_cloud.theme.ThemeActivity;
import com.ez_cloud.traditioncustom.CustomizationActivity;
import com.ez_cloud.traditioncustom.ModelActivity;
import com.ez_cloud.traditioncustom.PhotoMeasureActivity;
import com.ez_cloud.utils.Constants;
import com.ez_cloud.utils.GsonUtils;
import com.ez_cloud.utils.ServerInterface;
import com.ez_cloud.utils.SharedPreferencesUitls;
import com.ez_cloud.utils.Utility;
import com.google.android.gms.analytics.HitBuilders;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.unity3d.player.UnityPlayer;

public class UnityPlayerNativeActivity extends NativeActivity implements

OnClickListener {
	@ViewInject(R.id.tbl_title)
	com.ez_cloud.view.TitleBarLayout tbl_title;
	@ViewInject(R.id.unity_scroll)
	ScrollView unityMenu;
	@ViewInject(R.id.tv_skip_cloth)
	ImageView skipCloth;
	@ViewInject(R.id.tv_unity_cloth)
	ImageView cloth;// 布料
	@ViewInject(R.id.tv_unity_pants)
	ImageView pants;
	@ViewInject(R.id.tv_unity_longsleeve)
	ImageView longSleeve;
	@ViewInject(R.id.tv_unity_shortsleeve)
	ImageView shortsleeve;
	@ViewInject(R.id.tv_unity_shoe)
	ImageView shoe;
	@ViewInject(R.id.unity_cloth_list)
	ListView cloth_list;
	@ViewInject(R.id.iv_unity_heiku)
	ImageView heiku;
	@ViewInject(R.id.iv_unity_kaqiku)
	ImageView kaqiku;
	@ViewInject(R.id.iv_unity_lanku)
	ImageView lanku;
	@ViewInject(R.id.iv_unity_xiku)
	ImageView xiku;
	@ViewInject(R.id.iv_unity_xiuxianku)
	ImageView xiuxianku;
	@ViewInject(R.id.ll_unity_pants_list)
	LinearLayout ll_pants;
	@ViewInject(R.id.tv_unity_shorts)
	ImageView shorts;// 短裤
	@ViewInject(R.id.ll_unity_shorts_list)
	LinearLayout ll_unity_shorts;// 短裤集合
	@ViewInject(R.id.iv_unity_chouxiang)
	ImageView chouxiang;
	@ViewInject(R.id.iv_unity_fangge)
	ImageView fangge;
	@ViewInject(R.id.iv_unity_huawen)
	ImageView huawen;
	@ViewInject(R.id.iv_unity_shiwen)
	ImageView shiwen;
	@ViewInject(R.id.iv_unity_tiaowen)
	ImageView tiaowen;
	@ViewInject(R.id.ll_unity_shoe_list)
	LinearLayout ll_unity_shoe;// 鞋子集合
	@ViewInject(R.id.iv_unity_shoes_gym)
	ImageView shoes_gym;
	@ViewInject(R.id.iv_unity_shoes_leather)
	ImageView shoes_leather;
	@ViewInject(R.id.iv_unity_shoes_no)
	ImageView shoes_no;
	@ViewInject(R.id.unity_scroll)
	ScrollView mScrollView;

	protected UnityPlayer mUnityPlayer; // don't change the name of this
	private boolean isPants = true;// 长短裤标记
	private List<ClothInfos> clothsInfos = new ArrayList<ClothAllInfo.ClothInfos>();
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case Constants.REQUEST_SUCCEED:
				if (null != msg.obj) {
					ClothAllInfo allInfo = (ClothAllInfo) msg.obj;
					clothsInfos = allInfo.result;

				}
				break;
			/*
			 * case Constants.TODAY_RECOMMEND: if(null!=msg.obj){ TodayDataInfo
			 * todayInfo=(TodayDataInfo) msg.obj; todayResult=todayInfo.result;
			 * RequestData(); } break;
			 */
			}
		}
	};
	private Map<String, String> mData;
	private String shortsString = "shortsCX";
	private String pantsString = "PantsBlack";// 记录裤子的变量
	private String fastenerString = "whiteblack";// 记录纽扣的变量，默认白色
	private String collarString = "BZ";// 记录领型的变量，默认标准领
	private String sleeveString = "longT";// 记录长短袖的变量，默认长袖
	private ClothingApplication clothingApplication;

	// Setup activity layout
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		getWindow().takeSurface(null);
		setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
		getWindow().setFormat(PixelFormat.RGB_565);
		mUnityPlayer = new UnityPlayer(this);
		if (mUnityPlayer.getSettings().getBoolean("hide_status_bar", true))
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);

		mUnityPlayer.requestFocus();

		mUnityPlayer = new UnityPlayer(this);
		int glesMode = mUnityPlayer.getSettings().getInt("gles_mode", 1);
		boolean trueColor8888 = false;
		mUnityPlayer.init(glesMode, trueColor8888);
		View playerView = mUnityPlayer.getView();
		setContentView(R.layout.unity_layout);
		ViewUtils.inject(this);

		clothingApplication = (ClothingApplication) getApplication();
		clothingApplication.setStart(true);
		initClick();

		LinearLayout layout = (LinearLayout) findViewById(R.id.ll_unity);
		layout.addView(playerView);
		// 初始化渲染
		initData();
		String language = getResources().getConfiguration().locale
				.getLanguage();
		if ("en".equals(language)) {
			language = "EN";
		} else if ("zh".equalsIgnoreCase(language)) {
			language = "CH";
		} else if ("tw".equalsIgnoreCase(language)) {
			language = "TW";
		}
		// 请求服务器获取数据
		requestData(ServerInterface.CLOTH_All_URL + language,
				Constants.REQUEST_CLOTH);

	}

	private void initClick() {
		cloth.setOnClickListener(this);
		longSleeve.setOnClickListener(this);
		pants.setOnClickListener(this);
		shortsleeve.setOnClickListener(this);
		shorts.setOnClickListener(this);
		shoe.setOnClickListener(this);
		skipCloth.setOnClickListener(this);
		heiku.setOnClickListener(this);
		kaqiku.setOnClickListener(this);
		lanku.setOnClickListener(this);
		xiku.setOnClickListener(this);
		xiuxianku.setOnClickListener(this);
		chouxiang.setOnClickListener(this);
		fangge.setOnClickListener(this);
		huawen.setOnClickListener(this);
		shiwen.setOnClickListener(this);
		tiaowen.setOnClickListener(this);

		shoes_gym.setOnClickListener(this);
		shoes_leather.setOnClickListener(this);
		shoes_no.setOnClickListener(this);

		tbl_title.getBack().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if ("model".equals(SharedPreferencesUitls.getString(
						getApplicationContext(), "ActivityTag", ""))) {
					startActivity(new Intent(UnityPlayerNativeActivity.this,
							ModelActivity.class));
				} else if ("play".equals(SharedPreferencesUitls.getString(
						getApplicationContext(), "ActivityTag", ""))) {
					startActivity(new Intent(UnityPlayerNativeActivity.this,
							ThemeActivity.class));
				} else {
					startActivity(new Intent(UnityPlayerNativeActivity.this,
							HomeActivity.class));
				}
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);
			}
		});

		tbl_title.getNext().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 截图
				UnityPlayer.UnitySendMessage("Main Camera", "MeDetail", "");
				UnityPlayer.UnitySendMessage("Main Camera", "Front", "");
				UnityPlayer.UnitySendMessage("Main Camera", "CaptureFront", "");
				clothingApplication.setmMap(mData);
				startActivity(new Intent(UnityPlayerNativeActivity.this,
						PhotoMeasureActivity.class));
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_skip_cloth:
			startActivity(new Intent(UnityPlayerNativeActivity.this,
					CustomizationActivity.class));
			break;
		case R.id.tv_unity_cloth:
			if (0 == cloth_list.getVisibility()) {
				cloth_list.setVisibility(View.GONE);
				return;
			}
			cloth_list.setVisibility(View.VISIBLE);
			if (clothsInfos != null) {
				cloth_list.setAdapter(new UnityClothAdapter(
						UnityPlayerNativeActivity.this, clothsInfos, cloth));
				Utility.setListViewHeightBasedOnChildren(cloth_list);
			}

			break;
		case R.id.tv_unity_longsleeve:
			sleeveString = "longT";
			if (isPants) {
				UnityPlayer.UnitySendMessage("Main Camera", "Shirt",
						fastenerString + "|longT|" + collarString);
				UnityPlayer.UnitySendMessage("Main Camera", "Pants",
						pantsString + "|longT");
			} else {
				UnityPlayer.UnitySendMessage("Main Camera", "Shirt",
						fastenerString + "|longT|" + collarString);// 细节中
				UnityPlayer.UnitySendMessage("Main Camera", "Pants",
						shortsString + "|longT");
			}
			break;
		case R.id.tv_unity_shortsleeve:
			sleeveString = "short";
			if (isPants) {
				UnityPlayer.UnitySendMessage("Main Camera", "Pants",
						pantsString + "|short");
				UnityPlayer.UnitySendMessage("Main Camera", "Shirt",
						fastenerString + "|short|" + collarString);// 细节中

			} else {
				UnityPlayer.UnitySendMessage("Main Camera", "Pants",
						shortsString + "|short");
				UnityPlayer.UnitySendMessage("Main Camera", "Shirt",
						fastenerString + "|short|" + collarString);
			}
			break;

		case R.id.tv_unity_pants:
			if (0 == ll_pants.getVisibility()) {
				ll_pants.setVisibility(View.GONE);
				return;
			}
			ll_pants.setVisibility(View.VISIBLE);
			break;

		case R.id.iv_unity_biaozhun:
			collarString = "BZ";
			UnityPlayer.UnitySendMessage("Main Camera", "Shirt", "whiteblack|"
					+ sleeveString + "|BZ");

			break;
		case R.id.iv_unity_niukou:
			collarString = "DK";
			UnityPlayer.UnitySendMessage("Main Camera", "Shirt", "whiteblack|"
					+ sleeveString + "|DK");
			break;
		case R.id.iv_unity_winsha:
			collarString = "WS";
			UnityPlayer.UnitySendMessage("Main Camera", "Shirt", "whiteblack|"
					+ sleeveString + "|WS");
			break;
		case R.id.iv_unity_liling:
			collarString = "Li";
			UnityPlayer.UnitySendMessage("Main Camera", "Shirt", "whiteblack|"
					+ sleeveString + "|Li");
			break;
		case R.id.iv_unity_french:
			UnityPlayer
					.UnitySendMessage("Main Camera", "MenJin", "Traditional");
			break;
		case R.id.iv_unity_aditional:
			UnityPlayer
					.UnitySendMessage("Main Camera", "MenJin", "Traditional");
			break;
		case R.id.iv_unity_fashi_anmen:
			UnityPlayer.UnitySendMessage("Main Camera", "MenJin", "French");
			break;
		case R.id.iv_unity_chuangtong_anmen:
			UnityPlayer.UnitySendMessage("Main Camera", "MenJin", "French");
			break;
		case R.id.iv_unity_black:
			fastenerString = "blackwhite";
			UnityPlayer.UnitySendMessage("Main Camera", "MeDetail", "");
			UnityPlayer.UnitySendMessage("Main Camera", "Shirt", "blackwhite|"
					+ sleeveString + "|" + collarString);

			break;
		case R.id.iv_unity_white:
			fastenerString = "whiteblack";
			UnityPlayer.UnitySendMessage("Main Camera", "MeDetail", "");
			UnityPlayer.UnitySendMessage("Main Camera", "Shirt", "whiteblack|"
					+ sleeveString + "|" + collarString);
			break;
		case R.id.iv_unity_brown:
			fastenerString = "yellowwhite";
			UnityPlayer.UnitySendMessage("Main Camera", "MeDetail", "");
			UnityPlayer.UnitySendMessage("Main Camera", "Shirt", "yellowwhite|"
					+ sleeveString + "|" + collarString);
			break;
		case R.id.iv_unity_no:
			UnityPlayer.UnitySendMessage("Main Camera", "Pocket", "NO");
			break;
		case R.id.iv_unity_corner:
			UnityPlayer.UnitySendMessage("Main Camera", "Pocket", "YJ");
			break;
		case R.id.iv_unity_wujiao:
			UnityPlayer.UnitySendMessage("Main Camera", "Pocket", "WJ");
			break;
		case R.id.iv_unity_heiku:
			isPants = true;
			pantsString = "JeanGrey";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "JeanGrey|"
					+ sleeveString);
			break;
		case R.id.iv_unity_kaqiku:
			isPants = true;
			pantsString = "PantsKhaki";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "PantsKhaki|"
					+ sleeveString);
			break;
		case R.id.iv_unity_lanku:
			isPants = true;
			pantsString = "JeanBlue";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "JeanBlue|"
					+ sleeveString);
			break;
		case R.id.iv_unity_xiku:
			isPants = true;
			pantsString = "PantsBlack";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "PantsBlack|"
					+ sleeveString);
			break;
		case R.id.iv_unity_xiuxianku:
			isPants = true;
			pantsString = "PantsBlue";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "PantsBlue|"
					+ sleeveString);
			break;
		case R.id.tv_unity_shorts:
			if (0 == ll_unity_shorts.getVisibility()) {
				ll_unity_shorts.setVisibility(View.GONE);
				return;
			}
			ll_unity_shorts.setVisibility(View.VISIBLE);
			break;
		case R.id.iv_unity_chouxiang:
			isPants = false;
			shortsString = "shortsCX";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "shortsCX|"
					+ sleeveString);
			break;
		case R.id.iv_unity_fangge:
			isPants = false;
			shortsString = "shortsGWZ";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "shortsGWZ|"
					+ sleeveString);
			break;
		case R.id.iv_unity_huawen:
			isPants = false;
			shortsString = "shortsHW";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "shortsHW|"
					+ sleeveString);
			break;
		case R.id.iv_unity_shiwen:
			isPants = false;
			shortsString = "shortsSW";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "shortsSW|"
					+ sleeveString);
			break;
		case R.id.iv_unity_tiaowen:
			isPants = false;
			shortsString = "shortsGWH";
			UnityPlayer.UnitySendMessage("Main Camera", "Pants", "shortsGWH|"
					+ sleeveString);
			break;
		case R.id.tv_unity_shoe:
			if (0 == ll_unity_shoe.getVisibility()) {
				ll_unity_shoe.setVisibility(View.GONE);
				return;
			}
			ll_unity_shoe.setVisibility(View.VISIBLE);
			break;

		case R.id.iv_unity_shoes_gym:
			UnityPlayer.UnitySendMessage("Main Camera", "Shoes", "Sshoes");
			break;
		case R.id.iv_unity_shoes_leather:
			UnityPlayer.UnitySendMessage("Main Camera", "Shoes", "Lshoes");
			break;
		case R.id.iv_unity_shoes_no:
			UnityPlayer.UnitySendMessage("Main Camera", "Shoes", "NO");
			break;

		}

	}

	/**
	 * 默认数据渲染
	 */
	private void defaultData() {
		String string = "RN-1649-NO.7";
		try {
			string = new String(string.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		UnityPlayer.UnitySendMessage("Main Camera", "DP", string);
		StringBuffer sb = new StringBuffer();

		sb.append("blackwhite" + "|");
		sb.append("longT" + "|");
		sb.append("BZ");
		UnityPlayer.UnitySendMessage("Main Camera", "Me", "");

		UnityPlayer.UnitySendMessage("Main Camera", "Shirt", sb.toString());

		UnityPlayer.UnitySendMessage("Main Camera", "MenJin", "Traditional");

		UnityPlayer.UnitySendMessage("Main Camera", "Pocket", "YJ");

		UnityPlayer
				.UnitySendMessage("Main Camera", "Pants", "PantsBlack|longT");
	}

	private void requestData(String url, final int tag) {
		Log.d(">>>>>>>>>>>", "url=" + url);
		HttpUtils httpUtils = new HttpUtils();
		httpUtils.send(HttpMethod.GET, url, new RequestCallBack<String>() {
			@Override
			public void onFailure(HttpException arg0, String arg1) {
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				Message msg = Message.obtain();
				switch (tag) {
				case Constants.REQUEST_CLOTH:
					ClothAllInfo allInfo = GsonUtils.json2Bean(arg0.result,
							ClothAllInfo.class);
					if (allInfo != null && "00".equals(allInfo.code)) {
						msg.obj = allInfo;
						msg.what = Constants.REQUEST_SUCCEED;
					}
					break;
				}

				handler.sendMessage(msg);
			}
		});
	}

	/**
	 * 从Model、Measur跳转到3D数据初始化方法
	 */
	private void initModel() {
		mData = clothingApplication.getmMap();
		System.out.println("unity---mData===" + mData);
		String clothPath = mData.get("path");
		if (null!=clothPath&&!"".equals(clothPath)) {
			int lastIndexOf = clothPath.lastIndexOf('/');
			String substring = clothPath.substring(lastIndexOf + 1,
					clothPath.length() - 4);
			UnityPlayer.UnitySendMessage("Main Camera", "DP", substring);
		} else {
			String string = "RN-1649-NO.7";
			try {
				string = new String(string.getBytes(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			UnityPlayer.UnitySendMessage("Main Camera", "DP", string);
			StringBuffer sb = new StringBuffer();

			sb.append("blackwhite" + "|");
			sb.append("longT" + "|");
			sb.append("BZ");
			UnityPlayer.UnitySendMessage("Main Camera", "Shirt", sb.toString());
		}
		UnityPlayer.UnitySendMessage("Main Camera", "MeDetail", "");
		StringBuffer sb = new StringBuffer();
		String fastener = mData.get("fastener");
		if ("1".equals(fastener)) {
			fastener = "whiteblack";
		} else if ("2".equals(fastener)) {
			fastener = "blackwhite";
		} else if ("3".equals(fastener)) {
			fastener = "yellowwhite";
		}

		String xz = mData.get("sleeveLongShort");
		if ("L".equals(xz)) {
			xz = "longT";
			sleeveString = "longT";
		} else if ("S".equals(xz)) {
			xz = "short";
			sleeveString = "short";
		}

		String lz = mData.get("collarModel");
		if ("L001".equals(lz)) {
			lz = "BZ";
		} else if ("L005".equals(lz)) {
			lz = "DK";
		} else if ("L003".equals(lz)) {
			lz = "WS";
		} else if ("L007".equals(lz)) {
			lz = "Li";
		}
		sb.append(fastener + "|");
		sb.append(xz + "|");
		sb.append(lz);
		UnityPlayer.UnitySendMessage("Main Camera", "Shirt", sb.toString());

		String mj = mData.get("closing");
		if ("1".equals(mj) || "2".equals(mj)) {
			mj = "Traditional";
		} else if ("3".equals(mj) || "4".equals(mj)) {
			mj = "French";
		}
		UnityPlayer.UnitySendMessage("Main Camera", "MenJin", mj);

		String kd = mData.get("pocket");
		if ("0".equals(kd)) {
			kd = "NO";
		} else if ("1".equals(kd)) {
			kd = "YJ";
		} else if ("2".equals(kd)) {
			kd = "WJ";
		}

		UnityPlayer.UnitySendMessage("Main Camera", "Pocket", kd);
		UnityPlayer.UnitySendMessage("Main Camera", "MeDetail", "");
		UnityPlayer.UnitySendMessage("Main Camera", "Pants", "PantsBlack|"
				+ sleeveString);
	}

	@Override
	protected void onRestart() {
		// initModel();
		// TODO Auto-generated method stub
		super.onRestart();
		initData();

	}

	/**
	 * 初始化渲染
	 */
	private void initData() {
		boolean is_detail = SharedPreferencesUitls.getBoolean(
				getApplicationContext(), "is_detail", false);
		if (is_detail) {
			// 延迟3秒显示菜单
			new Handler().postDelayed(new Runnable() {
				public void run() {
					unityMenu.setVisibility(View.VISIBLE);
				}
			}, 3000);
			tbl_title.setTitle(getResources().getString(R.string.dapei));
			tbl_title.getNext().setVisibility(View.GONE);
			skipCloth.setVisibility(View.GONE);
			if (0 == cloth_list.getVisibility())
				cloth_list.setVisibility(View.GONE);
			if (0 == ll_pants.getVisibility())
				ll_pants.setVisibility(View.GONE);
			if (0 == ll_unity_shorts.getVisibility())
				ll_unity_shorts.setVisibility(View.GONE);
			if (0 == ll_unity_shoe.getVisibility())
				ll_unity_shoe.setVisibility(View.GONE);
			defaultData();

		} else {
			// requestData(ServerInterface.TODAY_RECOMMEND,2);
			tbl_title.setTitle(getResources().getString(R.string.preview3d));
			unityMenu.setVisibility(View.GONE);
			skipCloth.setVisibility(View.VISIBLE);
			initModel();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if (id == R.id.menu_settings) {
			// Log setting open event with category="ui", action="open", and
			// label="settings"
			ClothingApplication.tracker().send(
					new HitBuilders.EventBuilder("ui", "open").setLabel(
							"settings").build());
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// Quit Unity
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mUnityPlayer.quit();
	}

	// Pause Unity
	@Override
	protected void onPause() {
		super.onPause();
		mUnityPlayer.pause();
	}

	// Resume Unity
	@Override
	protected void onResume() {
		super.onResume();
		mUnityPlayer.resume();
	}

	// This ensures the layout will be correct.
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mUnityPlayer.configurationChanged(newConfig);
	}

	// Notify Unity of the focus change.
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		mUnityPlayer.windowFocusChanged(hasFocus);
	}

	// For some reason the multiple keyevent type is not supported by the ndk.
	// Force event injection by overriding dispatchKeyEvent().
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
			return mUnityPlayer.injectEvent(event);
		return super.dispatchKeyEvent(event);
	}

	// Pass any events not handled by (unfocused) views straight to UnityPlayer
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		return mUnityPlayer.injectEvent(event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return mUnityPlayer.injectEvent(event);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return mUnityPlayer.injectEvent(event);
	}

	/* API12 */public boolean onGenericMotionEvent(MotionEvent event) {
		return mUnityPlayer.injectEvent(event);
	}

}
