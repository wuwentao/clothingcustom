package com.ez_cloud.utils;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpCache;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class ClothingHttpUtils {

	private static final HttpUtils httpUtils = new HttpUtils();

	private ClothingHttpUtils() {

	}

	public static <T> void httpSend(HttpMethod method, String url,
			RequestParams params, final Class<T> clazz, final Handler handler,
			final int successTag,
			final int failureTag) {
		HttpCache.setDefaultExpiryTime(0);
		httpUtils.configTimeout(5000);
		httpUtils.configSoTimeout(5000);
		httpUtils.configRequestRetryCount(0);

		httpUtils.send(method, url, params, new RequestCallBack<String>() {
			Message msg = Message.obtain();

			@Override
			public void onFailure(HttpException exctption, String arg1) {
				// TODO Auto-generated method stub
				msg.what=failureTag;
				handler.sendMessage(msg);
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				// TODO Auto-generated method stub
				Log.d("json", arg0.result);
				T json2Bean = GsonUtils.json2Bean(arg0.result, clazz);
				msg.obj=json2Bean;
				msg.what =successTag;
				handler.sendMessage(msg);
			}
		});

	}

}
