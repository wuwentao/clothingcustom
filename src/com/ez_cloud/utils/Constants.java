package com.ez_cloud.utils;

import android.os.Environment;

public class Constants {

	/** 邮箱正则表达式 */
	public static final String EMAIL = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

	/** 正则表达式。验证用户输入的手机号码格式是否正确 */
	public static final String telRegex = "(09)+[0-9]{8}";

	/** 正则表达式。验证用户名格式是否正确 */
	public static final String usernameRegex = "^[A-Za-z0-9]{5,17}$";
	/** 正则表达式。验证密码是否输入正确 */
	public static final String pswRegex = "/[a-zA-Z0-9]+/{6,17}$";// 长度在6~18之间，只能包含字符、数字

	/**
	 * 我的头像保存目录
	 */
	public static String MyAvatarDir = Environment
			.getExternalStorageDirectory() + "/avatar/";
	/**
	 * 拍照回调
	 */
	public static final int REQUESTCODE_UPLOADAVATAR_CAMERA = 1;// 拍照修改头像
	public static final int REQUESTCODE_UPLOADAVATAR_LOCATION = 2;// 本地相册修改头像
	public static final int REQUESTCODE_UPLOADAVATAR_CROP = 3;// 系统裁剪头像

	public static final int REQUESTCODE_TAKE_CAMERA = 0x000001;// 拍照
	public static final int REQUESTCODE_TAKE_LOCAL = 0x000002;// 本地图片
	public static final int REQUESTCODE_TAKE_LOCATION = 0x000003;// 位置
	public static final String EXTRA_STRING = "extra_string";


	public static final int HANDLER_WHAT = 4;

	public static final int REQUEST_TAG_A = 101;

	public static final int REQUEST_TAG_B = 102;

	public static final int REQUEST_TAG_C = 103;

	public static final int MANUAL_UPDATE = 111;

	public static final int SELF_UPDATE = 112;

	public static final int REQUEST_SUCCEED = 113;

	public static final int REQUEST_FAILURE = 114;

	public static final int REQUEST_TIMEOUT = 200;

	public static final int REQUEST_DATA = 300;

	public static final int COMMENT_APPROVE = 301;

	public static final int DOWN_IMAGE_SUCCEED = 400;

	public static final int DOWN_IMAGE_FAILURE = 401;

	public static final int REQUEST_CLOTH = 410;

	public static final int CONFIGTIMEOUT = 5000;// 请求网络超时

	public static final String REGION = "region";// 地区

}
