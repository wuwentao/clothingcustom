package com.ez_cloud.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.ez_cloud.account.LoginActivity;
import com.ez_cloud.clothingcustom.R;

public class Member {
	private static boolean value=true;
	public static boolean isMember(final Context context){
		if(SharedPreferencesUitls.getString(context, "userId", null)==null){
			new AlertDialog.Builder(context)
			.setMessage(R.string.login_phone_number)
			.setPositiveButton(R.string.login_yes, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					Intent intent = new Intent(context, LoginActivity.class);
//					intent.putExtra("loginTag", loginTag);
					context.startActivity(intent);
					if (dialog != null && ((Dialog) dialog).isShowing())
					{
						dialog.dismiss();
					}
				}
			})
			.setNegativeButton(R.string.login_no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					//取消按钮事件
				}
			})
			.show();
			return false;
	}
		return value;
	}
	
	/*private void showExitGameAlert() {
		 final AlertDialog dlg = new AlertDialog.Builder(context).create();
		 dlg.show();
		 Window window = dlg.getWindow();
		        // *** 主要就是在这里实现这种效果的.
		        // 设置窗口的内容页面,shrew_exit_dialog.xml文件中定义view内容
		 window.setContentView(R.layout.shrew_exit_dialog);
		        // 为确认按钮添加事件,执行退出应用操作
		 ImageButton ok = (ImageButton) window.findViewById(R.id.btn_ok);
		 ok.setOnClickListener(new View.OnClickListener() {
		  public void onClick(View v) {
		   exitApp(); // 退出应用...
		  }
		 });
		 
		        // 关闭alert对话框架
		        ImageButton cancel = (ImageButton) window.findViewById(R.id.btn_cancel);
		        cancel.setOnClickListener(new View.OnClickListener() {
		   public void onClick(View v) {
		    dlg.cancel();
		  }
		   });
		}*/
}
