package com.ez_cloud.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import com.ez_cloud.clothingcustom.R;
import com.lidroid.xutils.BitmapUtils;

public class MyBitmapUtils {
	static BitmapUtils bitmapUtils;

	public static final int LEFT = 0x01;
	public static final int RIGHT = 0x02;
	public static final int TOP = 0x03;
	public static final int BOTTOM = 0x04;

	/**
	 * 获取sd卡里的图片
	 * 
	 * @return
	 */
	public static Bitmap decodeBitmap(String path) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(path, options);
		if (bitmap == null) {
			Log.d("bitmap", "为空");
		}
		float realWidth = options.outWidth;
		float realHeight = options.outHeight;
		int scale = (int) ((realHeight > realWidth ? realHeight : realWidth) / 100);
		if (scale <= 0) {
			scale = 1;
		}
		options.inSampleSize = scale;
		options.inJustDecodeBounds = false;
		return bitmap;
	}

	// 获取指定Activity的截屏，保存到png文件
	public static Bitmap takeScreenShot(Activity activity) {
		// View是你需要截图的View
		View view = activity.getWindow().getDecorView();
		view.setDrawingCacheEnabled(true);
		view.buildDrawingCache();
		Bitmap b1 = view.getDrawingCache();

		// 获取状态栏高度
		Rect frame = new Rect();
		activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
		int statusBarHeight = frame.top;
		Log.i("TAG", "" + statusBarHeight);

		// 获取屏幕长和高
		int width = activity.getWindowManager().getDefaultDisplay().getWidth();
		int height = activity.getWindowManager().getDefaultDisplay()
				.getHeight();
		// 去掉标题栏
		// Bitmap b = Bitmap.createBitmap(b1, 0, 25, 320, 455);
		System.out.println("----" + b1);
		Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height
				+ -statusBarHeight);
		view.destroyDrawingCache();
		return b;
	}

	/**
	 * 保存bitmap到本地
	 * 
	 * @param bm
	 * @param fileName
	 */
	public static void downloadBitmap(Bitmap bm, String fileName) {

		File file = new File(Environment.getExternalStorageDirectory(),
				fileName);
		// if(!file.exists()){
		// file.mkdirs();
		// }
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(
					file.getPath());
			bm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保存bitmap到本地
	 * 
	 * @param bm
	 * @param fileName
	 */
	public static String downloadBitmap(Bitmap bm) {
		
		File file = new File(Environment.getExternalStorageDirectory(),
				"theme");
		// if(!file.exists()){
		// file.mkdirs();
		// }
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(
					file.getPath());
			bm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file.getPath();
	}

	/**
	 * 将Drawable转换为Bitmap
	 * 
	 * @param drawable
	 * @return
	 */
	public static Bitmap drawableToBitmap(Drawable drawable) {
		Bitmap bitmap = Bitmap
				.createBitmap(
						drawable.getIntrinsicWidth(),
						drawable.getIntrinsicHeight(),
						drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
								: Bitmap.Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight());
		drawable.draw(canvas);
		return bitmap;
	}

	/**
	 * 将Bitmap转换为 byte[]
	 * 
	 * @param bm
	 * @return
	 */
	public static byte[] Bitmap2Bytes(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}

	/**
	 * 将 byte[]转换为bitmap
	 * 
	 * @param b
	 * @return
	 */
	public static Bitmap Bytes2Bimap(byte[] b) {
		if (b.length != 0) {
			return BitmapFactory.decodeByteArray(b, 0, b.length);
		} else {
			return null;
		}
	}

	/**
	 * 将bitmap转化为Drawable
	 * 
	 * @param bitmap
	 * @return
	 */
	public static Drawable bitmapToDrawable(Bitmap bitmap) {
		BitmapDrawable drawable = new BitmapDrawable(bitmap);
		return (Drawable) drawable;
	}

	/**
	 * 根据图片的宽度比列来缩放图片
	 * 
	 * @param resourse
	 * @param desWith
	 * @return
	 */
	public static Bitmap scaleBitMapByWidth(Bitmap source, int desWith) {
		int rWidth = source.getWidth();
		int rHeight = source.getHeight();
		float rat = (float) rWidth / (float) desWith;
		Matrix matrix = new Matrix();
		matrix.postScale(rat, rat);
		Bitmap bm = Bitmap.createBitmap(source, 0, 0, rWidth, rHeight, matrix,
				true);
		return bm;
	}

	/**
	 * 按屏幕宽度比例拉伸图片
	 * 
	 * @param b
	 * @param x
	 * @param y
	 * @return
	 */
	public static Bitmap stretchScreenWidth(Bitmap b, float screenWidth) {
		int bitmapWidth = b.getWidth();
		int bitmapHeight = b.getHeight();
		float sx;
		if (screenWidth > bitmapWidth) {
			sx = (float) screenWidth / bitmapWidth;// 要强制转换，不转换我的在这总是死掉。
		} else if (screenWidth < bitmapWidth) {
			sx = (float) bitmapWidth / screenWidth;
		} else {
			sx = 1;
		}
		// float sy=(float)bitmapWidth/bitmapHeight;
		Matrix matrix = new Matrix();
		matrix.postScale(sx, sx); // 长和宽放大缩小的比例
		Bitmap resizeBmp = Bitmap.createBitmap(b, 0, 0, bitmapWidth,
				bitmapHeight, matrix, true);
		return resizeBmp;
	}

	/**
	 * 	使用BitmapFactory.Options的inSampleSize参数来缩放
	 * @param path
	 * @param width
	 * @param height
	 * @return
	 */

	public static Drawable stretchBitmap(String path, int width, int height) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;// 不加载bitmap到内存中
		BitmapFactory.decodeFile(path, options);
		int outWidth = options.outWidth;
		int outHeight = options.outHeight;
		options.inDither = false;
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		options.inSampleSize = 1;

		if (outWidth != 0 && outHeight != 0 && width != 0 && height != 0) {
			int sampleSize = (outWidth / width + outHeight / height) / 2;
//			Log.d(tag, "sampleSize = " + sampleSize);
			options.inSampleSize = sampleSize;
		}

		options.inJustDecodeBounds = false;
		return new BitmapDrawable(BitmapFactory.decodeFile(path, options));
	}

	/**
	 * 根据图片的高度比列来缩放图片
	 * 
	 * @param resourse
	 * @param desWith
	 * @return
	 */
	public static Bitmap scaleBitMapByHeight(Bitmap source, int desHeight) {
		int rWidth = source.getWidth();
		int rHeight = source.getHeight();
		float rat = (float) rHeight / (float) desHeight;
		Matrix matrix = new Matrix();
		matrix.postScale(rat, rat);
		Bitmap bm = Bitmap.createBitmap(source, 0, 0, rWidth, rHeight, matrix,
				true);
		return bm;
	}

	/**
	 * 将图片缩放到规定宽度和高度大小的尺寸中
	 * 
	 * @param source
	 * @param desWidth
	 * @param desHeight
	 * @return
	 */
	public static Bitmap scaleBitmap(Bitmap source, int desWidth, int desHeight) {
		int rWidth = source.getWidth();
		int rHeight = source.getHeight();
		float sx = (float) rWidth / (float) desWidth;
		float sy = (float) rHeight / (float) desHeight;
		Matrix matrix = new Matrix();
		matrix.postScale(sx, sy);
		Bitmap bm = Bitmap.createBitmap(source, 0, 0, rWidth, rHeight, matrix,
				true);
		return bm;
	}

	/**
	 * 图片的合成方法
	 * 
	 * @param bitmapList
	 *            要合的图片集合 所有图片宽高一致
	 * @return
	 */
	public static Bitmap compoundBitmap(List<Bitmap> bitmapList) {
		if (bitmapList == null || bitmapList.size() < 1) {
			return null;
		}

		if (bitmapList.size() == 1) {
			return bitmapList.get(0);
		}
		int firstWidth = bitmapList.get(0).getWidth();
		int firstHeight = bitmapList.get(0).getHeight();

		Bitmap newBitmap = null;
		Canvas canvas = null;

		newBitmap = Bitmap.createBitmap(firstWidth,
				firstHeight * bitmapList.size(), Config.RGB_565);
		canvas = new Canvas(newBitmap);
		for (int i = 0; i < bitmapList.size(); i++) {
			canvas.drawBitmap(bitmapList.get(i), 0, firstHeight * i, null);
		}
		canvas.save();
		canvas.restore();
		return compressImage(newBitmap);
	}

	/**
	 * 图片的合成方法
	 * 
	 * @param first
	 *            第一张图片
	 * @param second
	 *            第二张图片
	 * @param direction
	 *            第二张图片在第一张图片的上面位置 LEFT,RIGHT,TOP,BOTTOM
	 * @return
	 */
	public static Bitmap compoundBitmap(Bitmap first, Bitmap second,
			int direction) {
		if (first == null && second == null) {
			return null;
		}
		if (first == null && second != null) {
			return second;
		}
		if (first != null && second == null) {
			return first;
		}
		int firstWidth = first.getWidth();
		int firstHeight = first.getHeight();

		int secondWidth = second.getWidth();
		int secondHeight = second.getHeight();
		Bitmap newBitmap = null;
		Canvas canvas = null;
		switch (direction) {
		case LEFT:
			newBitmap = Bitmap.createBitmap(firstWidth + secondWidth,
					firstHeight > secondHeight ? firstHeight : secondHeight,
					Config.ARGB_8888);
			canvas = new Canvas(newBitmap);
			canvas.drawBitmap(second, 0, 0, null);
			canvas.drawBitmap(first, secondWidth, 0, null);
			// canvas.restore();
			canvas.save();
			break;
		case RIGHT:
			newBitmap = Bitmap.createBitmap(firstWidth + secondWidth,
					firstHeight > secondHeight ? firstHeight : secondHeight,
					Config.ARGB_8888);
			canvas = new Canvas(newBitmap);
			canvas.drawBitmap(first, 0, 0, null);
			canvas.drawBitmap(second, firstWidth, 0, null);
			// canvas.restore();
			canvas.save();
			break;
		case TOP:
			// 项目需要，宽度一致，故图片纵向合并
			newBitmap = Bitmap.createBitmap(
					firstWidth > secondWidth ? secondWidth : firstWidth,
					firstHeight + secondHeight, Config.ARGB_4444);
			canvas = new Canvas(newBitmap);
			canvas.drawBitmap(second, 0, 0, null);
			canvas.drawBitmap(first, 0, secondHeight, null);
			canvas.save();
			canvas.restore();
			break;
		case BOTTOM:
			newBitmap = Bitmap.createBitmap(
					firstWidth > secondWidth ? firstWidth : secondWidth,
					firstHeight + secondHeight, Config.ARGB_8888);
			canvas = new Canvas(newBitmap);
			canvas.drawBitmap(first, 0, 0, null);
			canvas.drawBitmap(second, 0, firstHeight, null);
			// canvas.restore();
			canvas.save();
			break;
		default:
			break;
		}

		return compressImage(newBitmap);
		// return newBitmap;
	}

	/**
	 * 将图片转换为圆角图片
	 * 
	 * @param bitmap
	 * @param pixels
	 *            圆角的弧度
	 * @return
	 */
	public static Bitmap roundCornerBitmap(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		int color = 0xff424242;
		Paint paint = new Paint();
		Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		RectF rectf = new RectF(rect);
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectf, pixels, pixels, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	/**
	 * 把bitmap转换成字符串
	 * 
	 * @param bitmap
	 * @return
	 */
	public static String bitmaptoString(Bitmap bitmap) {
		// 将Bitmap转换成字符串
		String string = null;
		ByteArrayOutputStream bStream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 100, bStream);
		byte[] bytes = bStream.toByteArray();
		string = Base64.encodeToString(bytes, Base64.DEFAULT);
		byte[] bytes2 = string.getBytes();
		try {
			string = new String(bytes2, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return string;
	}

	/**
	 * 给bitmap添加阴影效果
	 * 
	 * @param mbitmap
	 * @return
	 */
	@SuppressLint("SdCardPath")
	public static Bitmap drawPicture(Bitmap mbitmap) {
		Bitmap bitmap = mbitmap;
		// BitmapFactory.decodeResource(getResources(), R.drawable.mymoive_01);
		Bitmap mBitmap = Bitmap.createBitmap(bitmap.getWidth() + 4,
				bitmap.getHeight() + 4, Bitmap.Config.ARGB_8888);
		Canvas mCanvas = new Canvas(mBitmap);
		Paint mPaint = new Paint();

		mPaint.setColor(Color.WHITE);
		mCanvas.drawRect(0, 0, bitmap.getWidth() + 4, bitmap.getHeight() + 4,
				mPaint);
		mCanvas.drawBitmap(bitmap, 2, 2, new Paint());
		mCanvas.save(Canvas.ALL_SAVE_FLAG);
		mCanvas.restore();

		Matrix matrix = new Matrix();
		matrix.setRotate(8);
		Bitmap newBitmap = Bitmap.createBitmap(mBitmap, 0, 0,
				mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
		matrix.setRotate(5);
		Bitmap xBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(),
				mBitmap.getHeight(), matrix, true);

		Canvas canvas = new Canvas(newBitmap);
		canvas.drawBitmap(xBitmap, 0, 0, new Paint());
		canvas.drawBitmap(mBitmap, 0, 4, new Paint());
		canvas.drawBitmap(mBitmap, 0, 10, new Paint());
		canvas.save(Canvas.ALL_SAVE_FLAG);
		canvas.restore();
		String filename = "/sdcard/12.png";
		File file = new File(filename);

		FileOutputStream out;
		try {
			file.createNewFile();
			out = new FileOutputStream(file);
			newBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			Log.e("System.out", "Save Ok");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("System.out", e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("System.out", e.toString());
			e.printStackTrace();
		}

		return newBitmap;
	}

	/**
	 * 按照图片宽高压缩图片
	 * 
	 * @param srcPath
	 * @param screenWidth
	 * @param screenHeight
	 * @return
	 */
	public static Bitmap getimage(String srcPath, int screenWidth,
			int screenHeight) {
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空
		newOpts.inPreferredConfig = Bitmap.Config.RGB_565;
		newOpts.inPurgeable = true;
		newOpts.inInputShareable = true;
		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		float hh = screenHeight;// 这里设置高度原图75%
		float ww = screenWidth;// 这里设置宽度原图75%
		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / ww);
		} else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / hh);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置缩放比例
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
		return compressBitmap(bitmap);// 压缩好比例大小后再进行质量压缩
	}

	/**
	 * 质量压缩图片
	 * 
	 * @param image
	 * @return
	 */
	public static Bitmap compressImage(Bitmap image) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 25, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
		int options = 100;
		while (baos.toByteArray().length / 1024 > 200) { // 循环判断如果压缩后图片是否大于500kb,大于继续压缩
			baos.reset();// 重置baos即清空baos
			options = options - 10;// 每次都减少10
			image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中

		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
		// Bitmap bitmap =BitmapFactory.decodeByteArray(baos.toByteArray(), 0,
		// baos.toByteArray().length);
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);
		return bitmap;
	}

	/**
	 * 
	 * @param image
	 * @param screenWidth
	 * @param screenHeight
	 * @return
	 */
	public static Bitmap comp(Bitmap image, int screenWidth, int screenHeight) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 100, baos);

		if (baos.toByteArray().length / 1024 > 1024) {// 判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
			baos.reset();// 重置baos即清空baos
			image.compress(Bitmap.CompressFormat.JPEG, 20, baos);// 这里压缩20%，把压缩后的数据存放到baos中
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
		newOpts.inJustDecodeBounds = false;
		newOpts.inPreferredConfig = Bitmap.Config.RGB_565;
		newOpts.inPurgeable = true;
		newOpts.inInputShareable = true;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		float hh = screenHeight;// 这里设置高度原图75%
		float ww = screenWidth;// 这里设置宽度原图75%
		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / ww);
		} else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / hh);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置缩放比例
		newOpts.inPreferredConfig = Config.RGB_565;// 降低图片从ARGB888到RGB565
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		isBm = new ByteArrayInputStream(baos.toByteArray());
		bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
		return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
	}

	/**
	 * 计算图片的缩放值
	 * 
	 * @param options
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	/**
	 * 根据路径获得图片并压缩返回bitmap用于显示
	 * 
	 * @param imagesrc
	 * @return
	 */
	public static Bitmap getCompressBitmap(String filePath) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPreferredConfig = Bitmap.Config.ARGB_4444;
		BitmapFactory.decodeFile(filePath, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, 480, 720);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		// return BitmapFactory.decodeFile(filePath, options);
		return BitmapFactory.decodeFile(filePath, options);
	}

	/**
	 * 根据路径获得图片并压缩返回bitmap用于显示
	 * 
	 * @param imagesrc
	 * @return
	 */
	public static Bitmap getSmallBitmap(String filePath) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		BitmapFactory.decodeFile(filePath, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, 320, 540);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		// return BitmapFactory.decodeFile(filePath, options);
		return compressBitmap(BitmapFactory.decodeFile(filePath, options));
	}

	public static Bitmap compressBitmap(Bitmap image) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
		int options = 100;
		while (baos.toByteArray().length / 1024 > 50) { // 循环判断如果压缩后图片是否大于50kb,大于继续压缩
			baos.reset();// 重置baos即清空baos
			options -= 10;// 每次都减少10
			image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);
		return bitmap;
	}

	/**
	 * 根据路径删除图片
	 * 
	 * @param path
	 */
	public static void deleteTempFile(String path) {
		File file = new File(path);
		Log.d("path", path);
		if (file.exists()) {
			file.delete();
		}
	}

	/**
	 * 添加到图库
	 */
	public static void galleryAddPic(Context context, String path) {
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(path);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		context.sendBroadcast(mediaScanIntent);
	}

	public static void display(Context context, View view, String path) {
		BitmapUtils bitUtils = new BitmapUtils(context);
		bitUtils.configDefaultLoadFailedImage(R.drawable.placehold_pi);// 加载失败默认图片
		bitUtils.configDefaultBitmapConfig(Bitmap.Config.RGB_565);// 设置图片压缩类型
		bitUtils.display(view, path);
	}
}
