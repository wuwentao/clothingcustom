package com.ez_cloud.utils;

import android.content.Context;
import android.util.DisplayMetrics;

public class ScreenUtils {
	
	public static int screenWidthPx = 0;
	
	public static int screenHeightPx = 0;
	
	public static float densityPx = 0;
	
	private static ScreenUtils screenUtils;
	
	public ScreenUtils()
	{
	}
	
	public static ScreenUtils getInstance()
	{
		if(screenUtils == null)
		{
			synchronized (ScreenUtils.class)
			{
				if(screenUtils == null)
				{
					screenUtils = new ScreenUtils();
				}
				
			}
		}
		
		return screenUtils;
	}
	
	
	public static void initScreenSize(Context mContext)
	{
		DisplayMetrics dm = mContext.getResources().getDisplayMetrics();

		screenHeightPx = dm.heightPixels;
		
		screenWidthPx = dm.widthPixels;
		
		densityPx = dm.density;
	}
	
	public static int dipConvertPx(int size)
	{
		return (int)(size*densityPx);
	}


}
