package com.ez_cloud.utils;

public class ServerInterface {
	/**登录接口*/
	public static final String LOGINURL="http://121.42.29.14:8080/fashion/mobileuser/mobileWebUserLogin.do?";

	/**密码重置接口接口*/
	public static final String findbackUrl="http://121.42.29.14:8080/fashion/mobileuser/resetWebUserPassword.do?";
	/**从服务器下载的首页相关数据*/
	public static final String homeUrl="http://121.42.29.14:8080/fashion/mobilerecommend/getMobileRecommend.do";
	/**点赞接口*/
	public static final String approveUrl="http://121.42.29.14:8080/fashion/mobilerecommend/addApproveToRecommend.do?";
	/**短信接口*/
	public static final String messageUrl="http://www.ucpaas.com/";
	/**修改密码接口*/
	public static final String modifyPswUrl="http://121.42.29.14:8080/fashion/mobileuser/updateWebUserPassword.do?";
	/**查询已经发货的订单接口，查询物流的时候用到的接口*/
	public static final String shippedOrderUrl="http://121.42.29.14:8080/fashion/mobileuser/getWebUserHaveSendOrderMessage.do?";
	/**根据userId查询收货地址的接口*/
	public static final String addressUrl="http://121.42.29.14:8080/fashion/mobileuser/getAllWebUserReceiveAddress.do?";
	/**根据id来删除某一个收货地址的接口*/
	public static final String deleteAddressUrl="http://121.42.29.14:8080/fashion/mobileuser/deleteWebUserReceiveAddress.do?id=";
	/**根据快递公司的名称和运单号码来查询物流*/
	public static final String logisticsInfoUrl="http://www.kuaidi100.com/query?";
	/**添加收货地址*/
	public static final String addAddressUrl="http://121.42.29.14:8080/fashion/mobileuser/androidAddWebUserReceiveAddress.do?";
	/**根据id进行修改地址*/
	public static final String modifyAddressUrl="http://121.42.29.14:8080/fashion/mobileuser/androidUpdateWebUserReceiveAddress.do?";
	/**根据userId获取用户默认地址*/
	public static final String USRE_DEFAULTADDRESSURL="http://121.42.29.14:8080/fashion/mobileuser/getWebUserDefaultAddress.do?";
	/**根据userId和订单状态码来查看各种状态下的订单信息*/
	public static final String orderStatusUrl="http://121.42.29.14:8080/fashion/mobileuser/getWebUserOrderMessageByCondition.do?";

	/** 根据telNumber和model来获取验证码，model有RESET和REGIST两种类型,RESET是找回密码，REGIST是注册 */
	public static final String GET_AUTH_CODE_URL = "http://121.42.29.14:8080/fashion/mobileuser/sendCheckCodeToMobile.do?";


	/**校验用户名是否注册*/
	public static final String CHECK_USERNAME_URL="http://121.42.29.14:8080/fashion/mobileuser/checkUserNameRegisted.do";

	/**根据telNumber校验手机号是否注册*/
	public static final String CHECK_NUMBER_URL="http://121.42.29.14:8080/fashion/mobileuser/checkTelNumberRegisted.do";

	/**根据telNumber和checkCode来校验验证码*/
	public static final String CHECK_CODE_URL="http://121.42.29.14:8080/fashion/mobileuser/checkMobileCheckCode.do?";

	/**根据邮箱获取验证码*/
	public static final String EMAIL_CHECK_CODE="http://121.42.29.14:8080/fashion/mobileuser/sendCheckCodeToUserEmail.do?";
	/**校验邮箱是否被注册*/
	public static final String CHECK_EMAIL="http://121.42.29.14:8080/fashion/mobileuser/checkUserEmailRegisted.do?";

	/**注册接口*/
	public static final String registerUrl="http://121.42.29.14:8080/fashion/mobileuser/mobileWebUserRegist.do?";
	/**
	 * 获取布料信息
     	需要提交参数：价格（199 － 899），类别（全部，素色，条纹，格纹,Y,M,C）
     	需返回数据：布料信息（包括：图片，名称，编号，成分，价格，介绍 。。。）
	 */
	public static final String CLOTH_All_URL="http://121.42.29.14:8080/fashion/mobilecloth/getMobileClothMessage.do?sysLan=";

	public static final String CLOTH_S_URL="http://121.42.29.14:8080/fashion/mobilecloth/getMobileClothMessage.do?style=S&sysLan=";

	public static final String CLOTH_T_URL="http://121.42.29.14:8080/fashion/mobilecloth/getMobileClothMessage.do?style=T&sysLan=";

	public static final String CLOTH_G_URL="http://121.42.29.14:8080/fashion/mobilecloth/getMobileClothMessage.do?style=G&sysLan=";
	
	public static final String CLOTH_Y_URL="http://121.42.29.14:8080/fashion/mobilecloth/getMobileClothMessage.do?style=Y&sysLan=";
	
	public static final String CLOTH_M_URL="http://121.42.29.14:8080/fashion/mobilecloth/getMobileClothMessage.do?style=M&sysLan=";
	
	public static final String CLOTH_C_URL="http://121.42.29.14:8080/fashion/mobilecloth/getMobileClothMessage.do?style=C&sysLan=";




	/** 
	 * 提交数据到服务器
	 */
	public static final String MODEL_COMMIT_URL="http://121.42.29.14:8080/fashion/mobileorder/androidUserSubmitOrderMessage.do";

	/**
	 * 标准 尺寸参数size  (37 —- 46)
	 */
	public static final String MODEL_SIZE_URL="http://121.42.29.14:8080/fashion/mobileorder/getStandardSizeMessage.do?";

	/**
	 * 买家分享
	 */
	public static final String SHARE_ORDER_URL="http://121.42.29.14:8080/fashion/buyershare/getBuyersShareMessage.do?order=N&style=&pageNo=";

	/**
	 * 分享点赞
	 */
	public static final String SHARE_APPROVE_URL="http://121.42.29.14:8080/fashion/buyershare/addApproveToBuyersShare.do?shareId=";

	/**
	 * 获取订单
	 */
	public static final String USER_INDENT_URL="http://121.42.29.14:8080/fashion/buyershare/getOrderMessageByUserId.do?userId=";
	/**
	 * 评论列表
	 */
	public static final String USER_COMMENT_URL="http://121.42.29.14:8080/fashion/buyershare/getWebUserShareCommentList.do?";
	/**
	 * 分享别人订单
	 */
	public static final String SHARE_COMMITOTHERS_URL="http://121.42.29.14:8080/fashion/buyershare/updateWebUserShareMessage.do?";
	/**
	 * 分享上传提交
	 */
	public static final String SHARE_COMMIT_URL="http://121.42.29.14:8080/fashion/buyershare/webUserUploadShareImage.do?";
	/**
	 * 分享上传提交数据（安卓）
	 */
	public static final String MYSHARE_COMMIT_URL="http://121.42.29.14:8080/fashion/buyershare/androidUserUploadShareImage.do";
	/**
	 * 删除订单
	 */
	public static final String DEIETE_ORDER="http://121.42.29.14:8080/fashion/mobileorder/cancelOrderByOrderId.do?orderId=";

	/**根据userId获取该用户的所有信息，这些信息主要用在个人中心页面*/
	public static final String getWebUserMessageUrl="http://121.42.29.14:8080/fashion/mobileuser/getWebUserMessage.do?";
	/**根据userId来上传头像*/
	public static final String uploadUserImageUrl="http://121.42.29.14:8080/fashion/mobileuser/androidUserUploadUserImage.do?";


	public static final  String UPDATE_VERSIONS="http://121.42.29.14:8080/fashion/sysmsg/getAppVersionMessage.do?appSys=android";
	//今日推荐
	public static final  String TODAY_RECOMMEND="http://121.42.29.14:8080/fashion/mobilerecommend/getEveryDayRecommend.do";
	//获取主题馆轮播图
	public static final String THEME_SHOW_IMAGE="http://121.42.29.14:8080/fashion/topicalorder/getTopicalModelMessage.do?sysLan=";
	//主题馆一键定制提交订单
	/**
	 * String orderId = StringUtil.sNull(request.getParameter("orderId"));
			tOrder.setUserId(StringUtil.sNull(request.getParameter("userId")));
			tOrder.setUserName(StringUtil.sNull(request.getParameter("userName")));
			tOrder.setModelId(StringUtil.sNull(request.getParameter("modelId")));
			
			tOrder.setBodyHeight(StringUtil.sNull(request.getParameter("bodyHeight")));
			tOrder.setBodyWeight(StringUtil.sNull(request.getParameter("bodyWeight")));
			
			tOrder.setXwValue(StringUtil.sNull(request.getParameter("xwValue")));
			tOrder.setYwValue(StringUtil.sNull(request.getParameter("ywValue")));
			tOrder.setTwValue(StringUtil.sNull(request.getParameter("twValue")));
			tOrder.setLwValue(StringUtil.sNull(request.getParameter("lwValue")));
			tOrder.setJkValue(StringUtil.sNull(request.getParameter("jkValue")));
			
			tOrder.setXcValue(StringUtil.sNull(request.getParameter("xcValue")));
			tOrder.setYcValue(StringUtil.sNull(request.getParameter("ycValue")));
			tOrder.setXsValue(StringUtil.sNull(request.getParameter("xsValue")));
			tOrder.setXkValue(StringUtil.sNull(request.getParameter("xkValue")));
	 */
	public static final String THEME_ONEKEY_ORDER="http://121.42.29.14:8080/fashion/topicalorder/submitTopicalOrder.do";
	
	//预约量身时间
	public static final String APPOINTMENT_MEASURE="http://121.42.29.14:8080/fashion/marrange/getArrangeMessage.do";
	//提交预约数据
	public static final String APPOINTMENT_COMMIT="http://121.42.29.14:8080/fashion/marrange/submitArrangeMessage.do?";
	//测量历史数据
	public static final String HISTORY_MEASURE_DATA="http://121.42.29.14:8080/fashion/mobileorder/getWebUserNewlyData.do?userId=";
} 
