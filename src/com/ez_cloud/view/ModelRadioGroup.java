package com.ez_cloud.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.HorizontalScrollView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ez_cloud.clothingcustom.R;

public class ModelRadioGroup extends HorizontalScrollView {

	private RadioGroup mRadioGroup;
	private RadioButton rb_one;
	private RadioButton rb_two;
	private RadioButton rb_three;
	private RadioButton rb_four;
	private RadioButton rb_five;
	private List<RadioButton> rButtonList;

	public ModelRadioGroup(Context context) {
		super(context);
	}

	public ModelRadioGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.model_radio_group, this);
		initData(context, attrs);
	}

	// 初始化数据
	private void initData(Context context, AttributeSet attrs) {
		rButtonList = new ArrayList<RadioButton>();

		mRadioGroup = (RadioGroup) findViewById(R.id.radio_group);
		rb_one = (RadioButton) findViewById(R.id.rg_but_one);
		rb_two = (RadioButton) findViewById(R.id.rg_but_two);
		rb_three = (RadioButton) findViewById(R.id.rg_but_three);
		rb_four = (RadioButton) findViewById(R.id.rg_but_four);
		rb_five = (RadioButton) findViewById(R.id.rg_but_five);

		rButtonList.add(rb_one);
		rButtonList.add(rb_two);
		rButtonList.add(rb_three);
		rButtonList.add(rb_four);
		rButtonList.add(rb_five);
		getAttrsData(context, attrs);

	}

	/**
	 * 解析XML文件数据
	 * 
	 * @param context
	 * @param attrs
	 */
	private void getAttrsData(Context context, AttributeSet attrs) {
		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.ModelRadioGroup);

		int oneId = ta.getResourceId(
				R.styleable.ModelRadioGroup_modelRadionButtonOneDrawable, 0);
		int twoId = ta.getResourceId(
				R.styleable.ModelRadioGroup_modelRadionButtonTwoDrawable, 0);
		int threeId = ta.getResourceId(
				R.styleable.ModelRadioGroup_modelRadionButtonThreeDrawable, 0);
		int fourId = ta.getResourceId(
				R.styleable.ModelRadioGroup_modelRadionButtonFourDrawable, 0);
		int FiveId = ta.getResourceId(
				R.styleable.ModelRadioGroup_modelRadionButtonFiveDrawable, 0);

		setButtonDrawables(oneId, twoId, threeId, fourId, FiveId);
	}

	/**
	 * 设置RadioButton绑定的图片资源
	 * 
	 * @param oneId
	 * @param twoId
	 * @param threeId
	 * @param fourId
	 * @param FiveId
	 */
	private void setButtonDrawables(int oneId, int twoId, int threeId,
			int fourId, int FiveId) {
		// 设置第一个为默认选中
		rb_one.setChecked(true);

		rb_one.setCompoundDrawablesWithIntrinsicBounds(0, oneId, 0, 0);
		rb_two.setCompoundDrawablesWithIntrinsicBounds(0, twoId, 0, 0);
		rb_three.setCompoundDrawablesWithIntrinsicBounds(0, threeId, 0, 0);
		rb_four.setCompoundDrawablesWithIntrinsicBounds(0, fourId, 0, 0);
		rb_five.setCompoundDrawablesWithIntrinsicBounds(0, FiveId, 0, 0);
	}

	/**
	 * 返回RadioGroup对象
	 * 
	 * @return RadioGroup
	 */
	public RadioGroup getRadioGroup() {

		return mRadioGroup;
	}

	/**
	 * 返回RadioGroup里子button集合
	 * @return rButtonList
	 */
	public List<RadioButton> getRadioButtonList() {
		return rButtonList;
	}

}
