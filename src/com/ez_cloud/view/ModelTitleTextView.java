package com.ez_cloud.view;

import com.ez_cloud.clothingcustom.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ModelTitleTextView extends RelativeLayout {

	public ModelTitleTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.title_text_view, this);
		
		initData(context,attrs);
	}

	private void initData(Context context, AttributeSet attrs) {
		TextView tv_model_title=(TextView) findViewById(R.id.tv_model_title);
		
		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TitleTextView);
		String strText = ta.getString(R.styleable.TitleTextView_modelTitleText);
		
		tv_model_title.setText(strText);
		
		
	}

	public ModelTitleTextView(Context context) {
		super(context);
	}

}
