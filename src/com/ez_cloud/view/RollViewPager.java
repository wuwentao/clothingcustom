package com.ez_cloud.view;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.utils.MyBitmapUtils;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.callback.BitmapLoadCallBack;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;

/**
 * 
 * viewpager需要传的数据： 1 需要传点的集合 2 需要把图片的url地址传进来 3 把文本描述信息传进来和textView传进来
 * 
 */
public class RollViewPager extends ViewPager {
	// 点的集合
	private List<View> dotLists;
	private Context mContext;
	private FrameLayout viewpager;
	private int screenWidth;
	private MyOnPagerClickListener myOnPagerClickListener;

	public RollViewPager(Context context, List<View> dotLists,
			FrameLayout top_news_viewpager, int screenWidth,
			MyOnPagerClickListener myOnPagerClickListener) {
		super(context);
		this.mContext = context;
		this.dotLists = dotLists;
		this.viewpager = top_news_viewpager;
		this.screenWidth = screenWidth;
		bitmapUtils = new BitmapUtils(mContext);

		// 设置图片的色彩模式
		// bitmapUtils.configDefaultBitmapConfig(Config.ARGB_4444);

		taskPager = new TaskPager();

		myOnTouchListener = new MyOnTouchListener();
		// RollViewPager.this.setOnTouchListener(myOnTouchListener);
		this.myOnPagerClickListener = myOnPagerClickListener;
	}

	public interface MyOnPagerClickListener {
		public void OnPagerClickListener(int position);
	}

	private class MyOnTouchListener implements OnTouchListener {

		private long downTimeMillis;
		private float onTouchDownX;

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:

				handler.removeCallbacksAndMessages(null);
				downTimeMillis = System.currentTimeMillis();
				onTouchDownX = event.getX();
				break;

			case MotionEvent.ACTION_UP:
				long duration = System.currentTimeMillis() - downTimeMillis;
				float onTouchUpX = event.getX();
				if (duration < 500 && onTouchDownX == onTouchUpX) {
					myOnPagerClickListener
							.OnPagerClickListener(mCurrentPostion);
				}
				startRoll();

				break;
			case MotionEvent.ACTION_MOVE:
				handler.removeCallbacks(taskPager);
				break;
			case MotionEvent.ACTION_CANCEL:
				startRoll();
				break;
			}
			return true;
		}

	}

	// 如果长按屏幕的时候描述信息和点还会动，调用如下方法
	@Override
	protected void onDetachedFromWindow() {
		handler.removeCallbacksAndMessages(null);
		super.onDetachedFromWindow();
	}

	/**
	 * dispatchTouchEvent 这个是用来分发事件用的 onInterceptTouchEvent
	 * 这个是用来做事件拦截用的(viewgroup里面才有这个方法) onTouchEvent 这个是用来做事件处理用的
	 */

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			downX = ev.getX();
			downY = ev.getY();

			getParent().requestDisallowInterceptTouchEvent(true);
			// long currentTimeMillis = System.currentTimeMillis();
			break;

		case MotionEvent.ACTION_MOVE:
			float mCurrentX = ev.getX();
			float mCurrentY = ev.getY();
			if (Math.abs(mCurrentX - downX) > Math.abs(mCurrentY - downY)) {
				getParent().requestDisallowInterceptTouchEvent(true);
			} else {
				getParent().requestDisallowInterceptTouchEvent(false);
			}

			break;
		case MotionEvent.ACTION_UP:

			break;
		}
		return super.dispatchTouchEvent(ev);
	}

	private class TaskPager implements Runnable {

		@Override
		public void run() {
			mCurrentPostion = (mCurrentPostion + 1) % mImageUrlLists.size();
			handler.obtainMessage().sendToTarget();
		}

	}

	public RollViewPager(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	private List<String> mImageUrlLists;

	// 图片的url地址
	public void setImageUrlLists(List<String> mImageUrlLists) {
		// TODO Auto-generated method stub
		this.mImageUrlLists = mImageUrlLists;
	}

	private TextView top_news_title;
	private List<String> mTitleLists;
	private BitmapUtils bitmapUtils;

	public void setTitleLists(TextView top_news_title, List<String> mTitleLists) {
		// TODO Auto-generated method stub
		this.top_news_title = top_news_title;
		this.mTitleLists = mTitleLists;
		// 初始化描述信息
		if (top_news_title != null && mTitleLists != null
				&& mTitleLists.size() > 0) {
			top_news_title.setText(mTitleLists.get(0));
		}
	}

	private boolean has_adapter = false;

	// 设置viewpager可以跳动的方法
	public void startRoll() {

		if (!has_adapter) {
			has_adapter = true;
			ViewPagerAdapter pagerAdapter = new ViewPagerAdapter();
			RollViewPager.this.setAdapter(pagerAdapter);
			RollViewPager.this
					.setOnPageChangeListener(new MyPageChangeListener());
		}

		handler.postDelayed(taskPager, 4000);

	}

	private int oldPosition = 0;

	private class MyPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageSelected(int position) {
			mCurrentPostion = position;

			if (top_news_title != null && mTitleLists != null
					&& mTitleLists.size() > 0) {
				top_news_title.setText(mTitleLists.get(position));
			}

			if (dotLists != null && dotLists.size() > 0) {
				dotLists.get(position).setBackgroundResource(
						R.drawable.dot_focus);
				dotLists.get(oldPosition).setBackgroundResource(
						R.drawable.dot_normal);
			}

			oldPosition = position;

		}

	}

	private int mCurrentPostion = 0;
	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			// 把图片设置到当前的界面
			RollViewPager.this.setCurrentItem(mCurrentPostion);
			startRoll();
		}

	};
	private TaskPager taskPager;
	private float downX;
	private float downY;
	private MyOnTouchListener myOnTouchListener;

	private class ViewPagerAdapter extends PagerAdapter {

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			View view = View.inflate(mContext, R.layout.viewpager_item, null);
			ImageView image = (ImageView) view.findViewById(R.id.image);
			bitmapUtils.configDefaultShowOriginal(true);
			bitmapUtils.display(image, mImageUrlLists.get(position),
					new BitmapLoadCallBack<ImageView>() {

						@Override
						public void onLoadCompleted(ImageView view,
								String arg1, Bitmap bitmap,
								BitmapDisplayConfig config, BitmapLoadFrom arg4) {
//							// TODO Auto-generated method stub
							Bitmap stretchBitmap = MyBitmapUtils
									.stretchScreenWidth(bitmap, screenWidth);
//							// 设置viewpager宽高
							android.view.ViewGroup.LayoutParams layoutParams = viewpager
									.getLayoutParams();
							layoutParams.width = stretchBitmap.getWidth();
							layoutParams.height = stretchBitmap.getHeight();
							viewpager.setLayoutParams(layoutParams);
							view.setImageBitmap(stretchBitmap);
						}

						@Override
						public void onLoadFailed(ImageView arg0, String arg1,
								Drawable arg2) {
							// TODO Auto-generated method stub

						}

					});

			view.setOnTouchListener(myOnTouchListener);
			container.addView(image);
			// view.setOnClickListener(new OnClickListener() {
			//
			// @Override
			// public void onClick(View arg0) {
			// // TODO Auto-generated method stub
			// Intent intent = new Intent(mContext,PhotoMeasureActivity.class);
			// intent.putExtra("shirtUrl", mImageUrlLists.get(position));
			// mContext.startActivity(intent);
			// }
			// });
			return view;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mImageUrlLists.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

	}
}
