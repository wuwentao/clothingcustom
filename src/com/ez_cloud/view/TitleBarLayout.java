package com.ez_cloud.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ez_cloud.clothingcustom.R;

public class TitleBarLayout extends RelativeLayout {

	private ImageView iv_back;
	private ImageView iv_next;
	private TextView tv_title;

	public TitleBarLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater  layoutInflater  = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.title_bar_layout, this);
		//初始化数据
		initData(context, attrs);
	}

	private void initData(Context context, AttributeSet attrs) {
		iv_back = (ImageView) findViewById(R.id.btn_return);
		iv_next = (ImageView) findViewById(R.id.iv_title_next);
		tv_title = (TextView)findViewById(R.id.tv_title);
		//解析attrs文件
		TypedArray ta=context.obtainStyledAttributes(attrs, R.styleable.TitleText);
		String titleText = ta.getString(R.styleable.TitleText_titleText);
		boolean nextVisibility=ta.getBoolean(R.styleable.TitleText_nextVisibility, true);
		int resourceId = ta.getResourceId(R.styleable.TitleText_nextSrc, R.drawable.next);
		iv_next.setImageResource(resourceId);
		tv_title.setText(titleText);
		if(nextVisibility){
			iv_next.setVisibility(View.VISIBLE);
		}else{
			iv_next.setVisibility(View.GONE);
		}
	}

	public TitleBarLayout(Context context) {
		super(context);
	}
	
	/**
	 * 设置标题
	 * @param str
	 */
	public void setTitle(String str){
		tv_title.setText(str);
	}
	/**
	 * 获取后退键
	 * @return
	 */
	public ImageView getBack(){
		return iv_back;
	}
	
	/**
	 * 获取下一步
	 * @return
	 */
	public ImageView getNext(){
		return iv_next;
	}
	
}
