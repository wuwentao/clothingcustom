package com.ez_cloud.welcome;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;

import com.ez_cloud.clothingcustom.R;
import com.ez_cloud.home.HomeActivity;
import com.ez_cloud.utils.SharedPreferencesUitls;
public class MainActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//隐藏华为魅族等手机底部的虚拟按键
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion >= 14) {
			getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		}
		setContentView(R.layout.strat);
		
		//初始化
		init();

	}



	private void init() {
		NetWorkStatus();// 判断网络连接状态
	}


	/**
	 * 判断网络
	 */
	private void NetWorkStatus() {

		ConnectivityManager cwjManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		cwjManager.getActiveNetworkInfo();
		if (cwjManager.getActiveNetworkInfo() != null) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					skipHome();
				}
			}, 2000);
		} else {
			Builder b = new AlertDialog.Builder(this).setTitle(R.string.network)
					.setMessage(R.string.set_network);
			b.setPositiveButton(R.string.is, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					/*
					 * Intent mIntent = new Intent("/"); ComponentName comp =
					 * new ComponentName( "com.android.settings",
					 * "com.android.settings.WirelessSettings");
					 * mIntent.setComponent(comp);
					 * mIntent.setAction("android.intent.action.VIEW");
					 * startActivityForResult(mIntent, 0); //
					 * 如果在设置完成后需要再次进行操作，可以重写操作代码，在这里不再重写
					 */
					// 4.0以上把原来的设置方式舍弃了,不能用上面的打开网络的方式，应该用下面的打开方式
					if (android.os.Build.VERSION.SDK_INT > 13) { // 3.2以上打开设置界面，也可以直接用ACTION_WIRELESS_SETTINGS打开到wifi界面
						startActivity(new Intent(
								android.provider.Settings.ACTION_SETTINGS));
					} else {
						startActivity(new Intent(
								android.provider.Settings.ACTION_WIRELESS_SETTINGS));
					}
					System.exit(0);
				}
			}).setNeutralButton(R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					System.exit(0);
				}
			}).show();
		}

	}


	private void skipHome() {
		// TODO Auto-generated method stub
		//延迟两秒后执行run方法中的页面跳转ת
		new Handler().postDelayed(new Runnable() {


			@Override
			public void run() {
				//用户第一次打开程序进入引导界面
				if("".equals(SharedPreferencesUitls.getString(MainActivity.this, "first", ""))){
					SharedPreferencesUitls.saveString(MainActivity.this, "first", "NO");
					Intent intent = new Intent(MainActivity.this, WhatsnewPagesA.class);
					MainActivity.this.startActivity(intent);
				}else {
					Intent intent = new Intent(MainActivity.this,HomeActivity.class);
					MainActivity.this.startActivity(intent);
				}
				finish();
			}
		}, 10);
	}
	


}
